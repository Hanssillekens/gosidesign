$(window).resize(function(){	
	initialiseren_website();
});

$(document).ready(function(){		
	/**********************************************
    * Footermenu aanpassen	                 	  *
    **********************************************/	
	
	if($("#footer .menu-columns>ul").length>0){
		//Eerste padding weghalen
		$("#footer .menu-columns>ul:first").css("padding-left","0px");
		if($("#footer>.content>.kolom").length>0){
			$("#footer>.content>.kolom:last").css("padding-right","0px");
			$("#footer>.content>.kolom:last").css("border","0px");
		} else {
			$("#footer .menu-columns>ul:last").css("padding-right","0px");
			$("#footer .menu-columns>ul:last").css("border","0px");
		}
	
		//Opsplitsen van submenuitems in meerdere kolommen
		var aantal_items_per_pagina=5;		
		$("#footer .menu-columns>ul>li>ul").each(function(){
			var items_zelf=$(this).children("li");
			var aantal_items=items_zelf.length;
			if(aantal_items>aantal_items_per_pagina){
				for(var i=0;i<aantal_items;i+=aantal_items_per_pagina){
					var opdeling=items_zelf.slice(i,i+aantal_items_per_pagina);  
					opdeling.first().addClass('eerste');
					opdeling.last().addClass('laatste');
					opdeling.wrapAll("<div class=\"kolom\"></div>");
				}
			}
		});
	}
	
	
	/**********************************************
    * Submenu van hoofdmenu	                 	  *
    **********************************************/	
	$("#header .menu>ul>li>ul").append("<div class=\"driehoek\"></div>");
	$("#header .menu>ul>li").hover(function() {			
		$(this).find("ul").css("opacity","0");
		$(this).find("ul").css("display","block");
		$(this).find("ul").stop(true,true).animate({top:"56px",opacity:1},250,"easeOutQuart");		
	},function(){
		$(this).find("ul").delay(250).stop(true,true).animate({top:"160px",opacity:0},150,"easeOutQuart",function(){
			$(this).css("display","none");
			$(this).css("top","35px");
		});			
	});
	
	
	/**********************************************
    * Mobiel menu			                 	  *
    **********************************************/	
	//Inschuiven en Uitschuiven van het menu op mobiele weergave
	$("#header .mobielmenu_activator").click(function(klik){	
		klik.preventDefault();		
		if($(".menu_mobiel").hasClass("actief")){
			//Inschuiven
			$(".menu_mobiel").removeClass("actief");
			$("body").stop(true,true).animate({paddingLeft:"0px"},250,"easeOutQuart");
			$(".menu_mobiel").stop(true,true).animate({left:"-250px"},250,"easeOutQuart");
		} else {
			//Uitschuiven
			$(".menu_mobiel").addClass("actief");
			$("body").stop(true,true).animate({paddingLeft:"250px"},250,"easeOutQuart");
			$(".menu_mobiel").stop(true,true).animate({left:"0px"},250,"easeOutQuart");
		}
	});
	
	
	/**********************************************
    * Portfolio 			                  	  *
    **********************************************/	
	if($("#portfoliooverzicht .items").length>0){		
		//Overlay naar onder halen als er over een item wordt gehoverd
		$("#portfoliooverzicht .items .item").hover(function(){
			$(".afbeelding .overlay",this).stop(true,true).animate({top:"0px"},300,"easeOutQuart");
		},function(){
			$(".afbeelding .overlay",this).stop(true,true).animate({top:"-"+$(".afbeelding",this).height()+"px"},300,"easeOutQuart");
		});	
	}
	
	/**********************************************
    * Vers werk widget		                   	  *
    **********************************************/	
	if($("#vers_werk").length>0){		
		//Overlay naar onder halen als er over een item wordt gehoverd
		$("#vers_werk .items .item").hover(function(){
			$(".afbeelding .overlay",this).stop(true,true).animate({top:"0px"},200,"easeOutQuart");
		},function(){
			$(".afbeelding .overlay",this).stop(true,true).animate({top:"-"+$(".afbeelding",this).height()+"px"},200,"easeOutQuart");
		});	
		
		
		//Slider van het vers werk
		//initialiseren
		if($(".rechts #vers_werk").length>0){	
			var huidige_slide_vers_werk=1;
			var aantal_slides_vers_werk=$("#vers_werk .slider .item").length;

			//automatisch starten van de slider
			var timer_vers_werk=$.timer(function(){
				slide_naar_slide_vers_werk(huidige_slide_vers_werk+1);
			});
			timer_vers_werk.set({time:5000,autostart:true});
			timer_vers_werk.play();
			
			//functie voor het starten en stoppen van automatisch sliden
			function start_automatisch_sliden_vers_werk(){
				timer_vers_werk.play();
			}
			function stop_automatisch_sliden_vers_werk(){
				timer_vers_werk.stop();
			}

			//functie voor het sliden naar een slide
			function slide_naar_slide_vers_werk(slide){
				breedte_item=$("#vers_werk .slider .item:eq(0)").width();
				//Domein bepalen van het aantal slides
				huidige_slide_vers_werk=slide;
				if(huidige_slide_vers_werk<0){ huidige_slide_vers_werk=aantal_slides_vers_werk; }
				if(huidige_slide_vers_werk>aantal_slides_vers_werk){ huidige_slide_vers_werk=1; }
				//Sliden naar de nieuwe slide
				$(".rechts #vers_werk .slider").stop(true).animate({marginLeft:"-"+((huidige_slide_vers_werk-1)*breedte_item)+"px"},500,"easeOutQuart");
			}

			//stoppen met automatisch sliden bij een hover
			$("#vers_werk .slider").hover(function(){
				stop_automatisch_sliden_vers_werk();
			},function(){
				start_automatisch_sliden_vers_werk();
			});
		}
	}
	
    /**********************************************
    * Headerafbeeldingen slider                   *
    **********************************************/
    if($("#headerafbeeldingen .slider .slide").length>1){
        //initialiseren
        var huidige_slide_headerafbeeldingen=1;
        var aantal_slides_headerafbeeldingen=$("#headerafbeeldingen .slider .slide").length;
        $("#headerafbeeldingen .navigatie div:eq(0)").addClass("actief");

        //automatisch starten van de slider
        var timer_headerafbeeldingen=$.timer(function(){
            slide_naar_slide_headerafbeeldingen(huidige_slide_headerafbeeldingen+1);
        });
        timer_headerafbeeldingen.set({time:5000,autostart:true});
        timer_headerafbeeldingen.play();

        //opvangen van de navigatie klikken
        $("#headerafbeeldingen .navigatie div").click(function(klik){
            klik.preventDefault();
            if(!$(this).hasClass("actief")){
                slide_naar_slide_headerafbeeldingen(($(this).index("#headerafbeeldingen .navigatie div")+1));
            }
        });

        //functie voor het starten en stoppen van automatisch sliden
        function start_automatisch_sliden_headerafbeeldingen(){
            timer_headerafbeeldingen.play();
        }
        function stop_automatisch_sliden_headerafbeeldingen(){
            timer_headerafbeeldingen.stop();
        }

        //functie voor het sliden naar een slide
        function slide_naar_slide_headerafbeeldingen(slide){
            hoogte_slides=$("#headerafbeeldingen .slider .slide:eq(0)").height();
            //Domein bepalen van het aantal slides
            huidige_slide_headerafbeeldingen=slide;
            if(huidige_slide_headerafbeeldingen<0){ huidige_slide_headerafbeeldingen=aantal_slides_headerafbeeldingen; }
            if(huidige_slide_headerafbeeldingen>aantal_slides_headerafbeeldingen){ huidige_slide_headerafbeeldingen=1; }
            //Nieuwe navigatieknop actief maken en oude inactief maken
            $("#headerafbeeldingen .navigatie div").removeClass("actief");
            $("#headerafbeeldingen .navigatie div:eq("+(huidige_slide_headerafbeeldingen-1)+")").addClass("actief");
            //Sliden naar de nieuwe slide
            $("#headerafbeeldingen .slider").stop(true).animate({marginTop:"-"+((huidige_slide_headerafbeeldingen-1)*hoogte_slides)+"px"},300,"easeOutQuart");
        }

        //stoppen met automatisch sliden bij een hover
        $("#headerafbeeldingen .slider").hover(function(){
            stop_automatisch_sliden_headerafbeeldingen();
        },function(){
            start_automatisch_sliden_headerafbeeldingen();
        });
    }	
	
	/**************************************************************
	* AUTOMATISCH INGEVULDE INPUTBOXEN WERKING					  *
	**************************************************************/
	//Alle input text boxen afgaan of er een default waarde moet worden ingevuld als deze leeg is en data-default is ingevuld
	$("input[type=text],textarea").each(function(){
		if(typeof  $(this).attr("data-default")!="undefined"){			
			if($(this).attr("value")==""){ 
				$(this).attr("value", $(this).attr("data-default"));
				$(this).css("color", "#787878");
			}	
		}			
	});	
	
	//Werking van de inputboxen bij een klik
	$("input[type=text],textarea").focus(function(){
		if(typeof $(this).attr("data-default")!="undefined"){
			if($(this).attr("value")==$(this).attr("data-default")){
				$(this).attr("value", "");
				$(this).css({"color":"#000000"});
			}
		}
	}).focusout(function(){
		if(typeof  $(this).attr("data-default")!="undefined"){
			if($(this).attr("value")==""){
				$(this).attr("value", $(this).attr("data-default"));
				$(this).css({"color":"#787878"});
			}
		}
	});

	//On submit van een formulier alle tekstvelden met nog de default waarde in leeg maken
	$("form").submit(function(){
		$("input[type=text]").each(function(){
			if($(this).attr("value")==$(this).attr("data-default")){ $(this).attr("value", ""); }		
		});
		$("textarea").each(function(){
			if($(this).attr("value")==$(this).attr("data-default")){ $(this).attr("value", ""); }		
		});
	});		
	
	initialiseren_website();
	
});




/**************************************************************
* Initiele functie, deze wordt ook aangeroepen bij een resize *
**************************************************************/
function initialiseren_website(){
	//PORTFOLIOPAGINA EN BLOGPAGINA
	if($(".filterbalk").length>0){
		//op de portfolio pagina, de filterbalk gelijke paddings geven
		breedte_gehele_balk=$(".filterbalk ul").width();
		breedte_ingevulde_balk=0;
		$(".filterbalk ul li").each(function(){
			breedte_ingevulde_balk+=$(this).width();
		});
		ruimte_over=breedte_gehele_balk-breedte_ingevulde_balk;
		aantal_filters=$(".filterbalk ul li").length;
		if(ruimte_over>4){
			padding_beide_kanten=Math.floor((ruimte_over/aantal_filters)/2);
			$(".filterbalk ul li").css("padding","0px "+padding_beide_kanten+"px 0px "+padding_beide_kanten+"px");
		} else {
			$(".filterbalk ul li").css("padding","0px 2px 0px 2px");
		}		
	}
	
	//FOOTERMENUHOOGTES GELIJKTREKKEN
	if(($("#footer .content>.kolom").length>0)||($("#footer .menu-columns>ul").length>0)){
		$("#footer .content>.kolom, #footer .menu-columns>ul").css("padding-bottom","25px");
		var hoogste=0;
		//hoogte vaststellen
		$("#footer .content>.kolom, #footer .menu-columns>ul").each(function(){
			if($(this).outerHeight()>hoogste){ hoogste=$(this).outerHeight(); }
		});
		//hoogte instellen
		$("#footer .content>.kolom, #footer .menu-columns>ul").each(function(){			
			if($(this).outerHeight()<hoogste){
				verschil=(hoogste-$(this).outerHeight());
				$(this).css("padding-bottom",(verschil-85)+"px");
			}
		});		
	}	
	
	//Hoogte mobielmenu instellen
	hoogte_scherm=$(window).height();
	hoogte_header=$("#header").height();
	$(".menu_mobiel").css("height",((hoogte_scherm-hoogte_header)+50)+"px");
	
	//Footer aan de onderkant van de website plakken
	//reset margin-top van footer
	$("#footer").css("margin-top","70px");
	//Opmeten en opnieuw instellen
	hoogte_inhoud=$("#website_container").height();
	hoogte_venster=$(window).height();
	if(hoogte_inhoud<hoogte_venster){
		$("#footer").css("margin-top",((hoogte_venster-hoogte_inhoud)+70)+"px");
	}
	
	//ALS DE MOBIELE WEBSITE WORDT GETOOND, DAN EEN AANTAL DINGEN AANPASSEN.
	breedte=$(window).innerWidth();
	console.log(breedte);
	if((breedte+17)<1000){
		//vers werk op de homepagina na het eerste teksblok zetten
		if($(".homepagina .tekstblok.eerste").length>0){			
			$(".homepagina #vers_werk").insertAfter(".homepagina .tekstblok.eerste");
		}
	} else {
		if($(".homepagina .tekstblok.eerste").length>0){			
			$(".homepagina #vers_werk").insertBefore(".homepagina .tekstblok.eerste");
		}
	}	
}