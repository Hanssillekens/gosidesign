<?php

return array(
    'phpthumb'  => array( 'auto' => true ),
    'phpmailer' => array( 'auto' => true ),
    'frontend'  => array(
        'auto'     => true,
        'location' => 'frontend',
        'handles'  => '/',
        'autoloads' => array(
            'map' => array('Frontend_Base_Controller' => '(:bundle)/controllers/base.php'),
        )
    ),
);