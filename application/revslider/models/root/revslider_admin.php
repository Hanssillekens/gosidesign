<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

	class RevSliderAdmin extends UniteBaseAdminClassRev{

		const VIEW_SLIDER = "slider";
		const VIEW_SLIDERS = "sliders";
		
		const VIEW_SLIDES = "slides";
		const VIEW_SLIDE = "slide";
		
		
		/**
		 * 
		 * the constructor
		 */
		public function __construct(){ //$mainFilepath
			
			parent::__construct(); //$mainFilepath,$this
			
			//set table names
			GlobalsRevSlider::$filepath_captions = self::$path_plugin."rs-plugin/css/captions.css";
			GlobalsRevSlider::$filepath_captions_original = self::$path_plugin."rs-plugin/css/captions-original.css";
			GlobalsRevSlider::$urlCaptionsCSS = self::$url_plugin."rs-plugin/css/captions.css";
			
			$this->init();
		}
		
		
		/**
		 * 
		 * init all actions
		 */
		private function init(){
			
			$this->checkCopyCaptionsCSS();
			
			
			self::setDebugMode();
			
		}
		
		/**
		 * if caption file don't exists - copy it from the original file.
		 */
		public static function checkCopyCaptionsCSS(){
			if(file_exists(GlobalsRevSlider::$filepath_captions) == false)
				copy(GlobalsRevSlider::$filepath_captions_original,GlobalsRevSlider::$filepath_captions);
				
			if(!file_exists(GlobalsRevSlider::$filepath_captions) == true){
				self::setStartupError("Can't copy <b>captions-original.css </b> to <b>captions.css</b> in <b> packages/revslider/rs-plugin/css </b> folder. Please try to copy the file by hand or turn to support.");
			}
			
		}
		
		
		/**
		 * 
		 * a must function. adds scripts on the page
		 * add all page scripts and styles here.
		 * pelase don't remove this function
		 * common scripts even if the plugin not load, use this function only if no choise.
		 */
		public static function onAddScripts(){
//			$html = Loader::helper('html');
//			$v = View::getInstance();
//
//			$v->addHeaderItem($html->css('edit_layers.css', 'revslider'));
//
//			$v->addHeaderItem($html->javascript('edit_layers.js', 'revslider'));
//
//			//add rs css:
//			$v->addHeaderItem($html->css('../rs-plugin/css/settings.css', 'revslider'));
//			$v->addHeaderItem($html->css('../rs-plugin/css/captions.css', 'revslider'));
			
		}
		
		/**
		 * 
		 * import slideer handle (not ajax response)
		 */
		public static function importSliderHandle(){
			
			dmp("importing slider setings and data...");
			
			$slider = new RevSlider();
			$response = $slider->importSliderFromPost();
			$sliderID = $response["sliderID"];
			
			$viewBack = View::url('/dashboard/revolution_slider/create/', $sliderID);
			if(empty($sliderID))
				$viewBack = View::url('/dashboard/revolution_slider/overview/');
			
			//handle error
			if($response["success"] == false){
				$message = $response["error"];
				dmp("<b>Error: ".$message."</b>");
				echo UniteFunctionsRev::getHtmlLink($viewBack, "Go Back");
			}
			else{	//handle success, js redirect.
				dmp("Slider Import Success, redirecting...");
				echo "<script>location.href='$viewBack'</script>"; 
			}
			exit();
		}
		
		
		
		/**
		 * 
		 * onAjax action handler
		 */
		public static function onAjaxAction(){
			$slider = new RevSlider();
			$slide = new RevSlide();
			$operations = new RevOperations();
			
			$action = self::getPostGetVar("client_action");
			$data = self::getPostGetVar("data");
			
			
			try{
			
				switch($action){
					case "export_slider":
						$sliderID = self::getGetVar("sliderid");
						$slider->initByID($sliderID);
						$slider->exportSlider();
					break;
					case "import_slider":
						self::importSliderHandle();
					break;
					case "create_slider":
						$newSliderID = $slider->createSliderFromOptions($data);
						
						self::ajaxResponseSuccessRedirect(
						            "The slider successfully created", 
									View::url('/dashboard/revolution_slider/overview'));
						
					break;
					case "update_slider":
						$slider->updateSliderFromOptions($data);
						self::ajaxResponseSuccess("Slider updated");
					break;
					
					case "delete_slider":
						$slider->deleteSliderFromData($data);
						
						self::ajaxResponseSuccessRedirect(
						            "The slider deleted", 
									View::url('/dashboard/revolution_slider/overview/slider_deleted'));
					break;
					case "duplicate_slider":
						
						$slider->duplicateSliderFromData($data);
						
						self::ajaxResponseSuccessRedirect(
						            "The duplicate successfully, refreshing page...", 
									View::url('/dashboard/revolution_slider/overview'));
					break;
					
					case "add_slide":
						$slider->createSlideFromData($data);
						$sliderID = $data["sliderid"];
						
						self::ajaxResponseSuccessRedirect(
						            "Slide Created", 
									'admin/sliders/slides/'.$sliderID );
					break;
					case "update_slide":
						$slide->updateSlideFromData($data);
						self::ajaxResponseSuccess("Slide updated");
					break;
					case "delete_slide":
						$slide->deleteSlideFromData($data);
						$sliderID = UniteFunctionsRev::getVal($data, "sliderID");
						self::ajaxResponseSuccessRedirect(
						            "Slide Deleted Successfully", 
									View::url('/dashboard/revolution_slider/slides/', $sliderID));					
					break;
					case "duplicate_slide":
						$sliderID = $slider->duplicateSlideFromData($data);
						self::ajaxResponseSuccessRedirect(
						            "Slide Duplicated Successfully", 
									View::url('/dashboard/revolution_slider/slides/', $sliderID));
					break;
					case "get_captions_css":
						$contentCSS = $operations->getCaptionsContent();
						self::ajaxResponseData($contentCSS);
					break;
					case "update_captions_css":
						$arrCaptions = $operations->updateCaptionsContentData($data);
						self::ajaxResponseSuccess("CSS file saved succesfully!",array("arrCaptions"=>$arrCaptions));
					break;
					case "restore_captions_css":
						$operations->restoreCaptionsCss();
						$contentCSS = $operations->getCaptionsContent();
						self::ajaxResponseData($contentCSS);
					break;
					case "update_slides_order":
						$slider->updateSlidesOrderFromData($data);
						self::ajaxResponseSuccess("Order updated successfully");
					break;
					case "change_slide_image":
						$slide->updateSlideImageFromData($data);
						$sliderID = UniteFunctionsRev::getVal($data, "slider_id");						
						self::ajaxResponseSuccessRedirect(
							"Slide Changed Successfully",
                            'admin/sliders/slides/'.$sliderID );
					break;	
					case "preview_slide":
						$operations->putSlidePreviewByData($data);
					break;
					case "preview_slider":
						$sliderID = UniteFunctionsRev::getPostVariable("sliderid");
						$operations->previewOutput($sliderID);
					break;
					case "toggle_slide_state":
						$currentState = $slide->toggleSlideStatFromData($data);
						self::ajaxResponseData(array("state"=>$currentState));
					break;
					default:
						self::ajaxResponseError("wrong ajax action: $action ");
					break;
				}
				
			}
			catch(Exception $e){
				$message = $e->getMessage();
				
				self::ajaxResponseError($message);
			}
			
			//it's an ajax action, so exit
			self::ajaxResponseError("No response output on <b> $action </b> action. please check with the developer.");
			exit();
		}
		
	}
	
	
?>