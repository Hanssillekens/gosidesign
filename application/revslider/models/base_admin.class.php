<?php
defined('C5_EXECUTE') or die(_("Access Denied.")); 
 
 class UniteBaseAdminClassRev extends UniteBaseClassRev{
 	
		const ACTION_ADMIN_MENU = "admin_menu";
		const ACTION_ADMIN_INIT = "admin_init";	
		const ACTION_ADD_SCRIPTS = "admin_enqueue_scripts";
		
		protected static $master_view;
		protected static $view;
		
		private static $arrSettings = array();
		private static $arrMenuPages = array();
		private static $tempVars = array();
		private static $startupError = "";
		
		/**
		 * 
		 * main constructor		 
		 */
		public function __construct(){ //$mainFile,$t
		
			parent::__construct(); //$mainFile,$t
			
		}		
		
		/**
		 * 
		 * set startup error to be shown in master view
		 */
		public static function setStartupError($errorMessage){
			self::$startupError = $errorMessage;
		}
		
		/**
		 * 
		 * add common used scripts
		 */
		public static function addCommonScripts(){


			//include jquery ui
//			$html = Loader::helper('html');
//			$v = View::getInstance();
//
//			$v->addHeaderItem($html->css('jui/smoothness/jquery-ui-1.9.2.custom.min.css', 'revslider'));
//
//			$v->addHeaderItem($html->javascript('settings.js', 'revslider'));
//			$v->addHeaderItem($html->javascript('admin.js', 'revslider'));
//			$v->addHeaderItem($html->javascript('jquery.tipsy.js', 'revslider'));
//			//--- add styles
//			$v->addHeaderItem($html->css('admin.css', 'revslider'));
//
//			//add tipsy
//			$v->addHeaderItem($html->css('tipsy.css', 'revslider'));
//
//			//include farbtastic
//			$v->addHeaderItem($html->javascript('farbtastic/farbtastic.js', 'revslider'));
//			$v->addHeaderItem($html->css('../js/farbtastic/farbtastic.css', 'revslider'));
//
//			//include codemirror
//			$v->addHeaderItem($html->javascript('codemirror/codemirror.js', 'revslider'));
//			$v->addHeaderItem($html->javascript('codemirror/css.js', 'revslider'));
//			$v->addHeaderItem($html->css('../js/codemirror/codemirror.css', 'revslider'));
			
		}
		
		/**
		 * 
		 * add admin used scripts
		 */
		public static function addAdminScripts(){
			
//			$html = Loader::helper('html');
//			$v = View::getInstance();
//
//			$v->addHeaderItem($html->javascript('rev_admin.js', 'revslider'));
//			$v->addHeaderItem($html->css('colors-fresh.css', 'revslider'));
		}
		
		/**
		 * 
		 * store settings in the object
		 */
		public static function storeSettings($key,$settings){
			self::$arrSettings[$key] = $settings;
		}
		
		/**
		 * 
		 * get settings object
		 */
		public static function getSettings($key){
			if(!isset(self::$arrSettings[$key]))
				UniteFunctionsRev::throwError("Settings $key not found");
			$settings = self::$arrSettings[$key];
			return($settings);
		}
		
		/**
		 * 
		 * echo json ajax response
		 */
		private static function ajaxResponse($success,$message,$arrData = null){
			
			$response = array();			
			$response["success"] = $success;				
			$response["message"] = $message;
			
			if(!empty($arrData)){
				
				if(gettype($arrData) == "string")
					$arrData = array("data"=>$arrData);				
				
				$response = array_merge($response,$arrData);
			}
				
			$json = json_encode($response);
			
			echo $json;
			exit();
		}

		/**
		 * 
		 * echo json ajax response, without message, only data
		 */
		protected static function ajaxResponseData($arrData){
			if(gettype($arrData) == "string")
				$arrData = array("data"=>$arrData);
			
			self::ajaxResponse(true,"",$arrData);
		}
		
		/**
		 * 
		 * echo json ajax response
		 */
		protected static function ajaxResponseError($message,$arrData = null){
			
			self::ajaxResponse(false,$message,$arrData,true);
		}
		
		/**
		 * echo ajax success response
		 */
		protected static function ajaxResponseSuccess($message,$arrData = null){
			
			self::ajaxResponse(true,$message,$arrData,true);
			
		}
		
		/**
		 * echo ajax success response
		 */
		protected static function ajaxResponseSuccessRedirect($message,$url){
			$arrData = array("is_redirect"=>true,"redirect_url"=>$url);
			
			self::ajaxResponse(true,$message,$arrData,true);
		}
 }