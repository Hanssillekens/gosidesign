<?php

class Loader{
    static function models( $filename ){

    }

    static function db(){
        return new LoaderDb();
    }
}


class LoaderDb{

    function execute( $sql, $params = array() ){

        $ret = DB::query($sql, array_values($params));

        try{
            if( 0 < $ret && is_array($ret) )
            {
                foreach($ret AS $key => $row )
                {
                    $ret[$key] = (array) $row;
                }

                return $ret;
            }
        }
        catch( Exception $e ){
            var_dump($ret); die;
        }


        return false;
    }


    function getRow( $sql, $params = array() ){
        return $this->execute($sql, array_values($params));
    }

    function getALL( $sql, $params = array() ){
        return $this->execute($sql, array_values($params));
    }
}


function recurse_copy($src,$dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if ( is_dir($src . '/' . $file) ) {
                recurse_copy($src . '/' . $file,$dst . '/' . $file);
            }
            else {
                copy($src . '/' . $file,$dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}