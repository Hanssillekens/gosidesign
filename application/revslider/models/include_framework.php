<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

if(!function_exists("dmp"))
    require dirname(__FILE__) . DS . 'functions.php';

if(!class_exists("UniteFunctionsRev"))
    require dirname(__FILE__) . DS . 'functions.class.php';
	
if(!class_exists("UniteFunctionsWPRev"))
    require dirname(__FILE__) . DS . 'functions_wordpress.class.php';

if(!class_exists("UniteSettingsRev"))
    require dirname(__FILE__) . DS . 'settings.class.php';

if(!class_exists("UniteCssParserRev"))
    require dirname(__FILE__) . DS . 'cssparser.class.php';
	
if(!class_exists("UniteSettingsAdvancedRev"))
    require dirname(__FILE__) . DS . 'settings_advances.class.php';

if(!class_exists("UniteSettingsOutputRev"))
    require dirname(__FILE__) . DS . 'settings_output.class.php';

if(!class_exists("UniteSettingsRevProductRev"))
    require dirname(__FILE__) . DS . 'settings_product.class.php';

if(!class_exists("UniteSettingsProductSidebarRev"))
    require dirname(__FILE__) . DS . 'settings_product_sidebar.class.php';

if(!class_exists("UniteImageViewRev"))
    require dirname(__FILE__) . DS . 'image_view.class.php';

if(!class_exists("UniteZipRev"))
    require dirname(__FILE__) . DS . 'zip.class.php';