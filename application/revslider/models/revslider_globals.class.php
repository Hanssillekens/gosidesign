<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

	class GlobalsRevSlider{
		
		const TABLE_SLIDERS_NAME = "sliders";
		const TABLE_SLIDES_NAME = "sliders_slides";
		const FIELDS_SLIDE = "slider_id,slide_order,params,layers";
		const FIELDS_SLIDER = "title,alias,params";
		
		const PACKAGE_HANDLE = 'revslider';
							
		public static $filepath_captions;
		public static $filepath_captions_original;
		public static $urlCaptionsCSS;
		
	}

?>