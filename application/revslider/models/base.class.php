<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

	class UniteBaseClassRev{
		
		protected static $mainFile;
		protected static $t;
		
		public static $dir_plugin;
		public static $url_plugin;
		protected static $url_ajax;
		public static $url_ajax_actions;
		public static $url_ajax_showimage;
		public static $path_settings;
		protected static $path_plugin;
		protected static $path_views;
		protected static $path_templates;
		protected static $path_cache;
		
		protected static $debugMode = false;
		
		/**
		 * 
		 * the constructor
		 */
		public function __construct(){ //$mainFile,$t

			//set plugin handle (as the main filename)
			self::$dir_plugin = GlobalsRevSlider::PACKAGE_HANDLE; 			

			
			self::$url_plugin = '/';
			
			self::$mainFile = Bundle::path('revslider').'lib';

			self::$url_ajax_actions = 'admin/sliders/ajax';

			self::$path_plugin = self::$mainFile."/";
			self::$path_settings = self::$path_plugin."settings/";
			
			self::$path_views = self::$path_plugin."views/";
			self::$path_templates = self::$path_views."/templates/";
			
		}
		
		/**
		 * 
		 * set debug mode.
		 */
		public static function setDebugMode(){
			self::$debugMode = true;
		}
		
		/**
		 * 
		 * get image url to be shown via thumb making script.
		 */
		public static function getImageUrl($fileID, $width=null,$height=null,$exact=false,$effect=null,$effect_param=null){
			
			$urlImage = UniteImageViewRev::getUrlThumb(self::$url_ajax_showimage, $fileID,$width ,$height ,$exact ,$effect ,$effect_param);
			
			return($urlImage);
		}
		
		/**
		 * 
		 * get POST var
		 */
		public static function getPostVar($key,$defaultValue = ""){
			$val = self::getVar($_POST, $key, $defaultValue);
			return($val);			
		}
				
		/**
		 * 
		 * get GET var
		 */
		public static function getGetVar($key,$defaultValue = ""){
			$val = self::getVar($_GET, $key, $defaultValue);
			return($val);
		}
		
		
		/**
		 * 
		 * get post or get variable
		 */
		public static function getPostGetVar($key,$defaultValue = ""){
			
			if(array_key_exists($key, $_POST))
				$val = self::getVar($_POST, $key, $defaultValue);
			else
				$val = self::getVar($_GET, $key, $defaultValue);				
			
			return($val);							
		}
		
		
		/**
		 * 
		 * get some var from array
		 */
		public static function getVar($arr,$key,$defaultValue = ""){
			$val = $defaultValue;
			if(isset($arr[$key])) $val = $arr[$key];
			return($val);
		}
		
		
	}

?>