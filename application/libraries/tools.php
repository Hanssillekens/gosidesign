<?php

class Tools extends Frontend_Tools_Library{

	public static function geef_nederlands_datum($datum){
		//Maanden uitgeschreven
		$array_maanden[1]=tl("maanden-januari");
		$array_maanden[2]=tl("maanden-februari");
		$array_maanden[3]=tl("maanden-maart");
		$array_maanden[4]=tl("maanden-april");
		$array_maanden[5]=tl("maanden-mei");
		$array_maanden[6]=tl("maanden-juni");
		$array_maanden[7]=tl("maanden-juli");
		$array_maanden[8]=tl("maanden-augustus");
		$array_maanden[9]=tl("maanden-september");
		$array_maanden[10]=tl("maanden-oktober");
		$array_maanden[11]=tl("maanden-november");
		$array_maanden[12]=tl("maanden-decemeber");
		
		$tijdstip_nu=strtotime($datum);
		
		return date("j",$tijdstip_nu)." ".$array_maanden[date("n",$tijdstip_nu)]." ".date("Y",$tijdstip_nu);
	}
	
	public static function get_page_url_by_template($template,$language = '' ){
        $language = ('' == $language ? Scotty::get_language() : $language );

        $page = \DB::table('pages_meta')
            ->where('pages_meta.language_id', '=', $language)
            ->join('pages', 'pages_meta.page_id', '=', 'pages.id')
            ->where('pages.template_id', '=', $template)
            ->where('pages.deleted', '=', '0' )
            ->first(array('*', 'pages.id as main_page_id', 'pages.page_id as parent_page_id', 'pages_meta.id as sub_page_id'));

        $path = '';

        if( null != $page )
        {
            $template_slug = $page->slug;

            if(0 != $page->parent_page_id )
            {
                $path = self::get_path_to_page($page->parent_page_id, '', $language ) .$template_slug .'/';
            }
            else
            {
                $path = $page->slug .'/';
            }
        }

        return Scotty::url().'/'.$path;
    }
}