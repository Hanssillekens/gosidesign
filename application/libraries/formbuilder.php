<?php

class Formbuilder extends Frontend_Formbuilder_Library{
	
	
    public function generate_html($form_action = false, $errors = null){
        $html = '';
        $form_action = $form_action ? $form_action : $_SERVER['PHP_SELF'];
        $id    = ( $this->_form_css['form_id'] != '') ? $this->_form_css['form_id'] : '' ;
        $class = ( $this->_form_css['form_class'] != '') ? $this->_form_css['form_class'] : 'form form-horizontal';
        if(is_array($this->_form_array['form_structure'])){    
            $html .= '<form class="' .$class .'" id="' .$id .'" method="post"  action="'.$form_action.'" enctype="multipart/form-data">' . "\n";
            $html .= '<div class="test">';			
			$counter=0;
            foreach($this->_form_array['form_structure'] as $field)
            {					
				if(($field["cssClass"]=="input_text")or($field["cssClass"]=="checkbox")){					
					if(($counter%2==0)and($counter!=0)){ 
						$html.="</div></div>"; 
					}
					if($counter%2==0){ 
						$html.="<div class=\"dummy\"><div class=\"links\">";
					}
					if($counter%2==1){ 
						$html.="</div><div class=\"rechts\">";
					}
					$counter++;	
				} else {
					if($counter!=0){ $html.="</div></div>"; }
					$counter=0;
				}
				$html .= $this->loadField((array)$field, $errors);                
            }                
            $submit = end($this->_form_array['form_structure']);
            $sumbit_text = ($submit['cssClass'] == 'submit') ? $submit['values'] : 'Verzenden';                                             
            $sumbit_name = ($submit['cssClass'] == 'submit' && $submit['name_tag'] != '') ? $submit['name_tag'] : 'submit'; 
            $html .= '<div style="margin:12px 0 0;"><input type="submit" name="' .$sumbit_name .'" class="btn" id="submitBtn" value="' .$sumbit_text .'" /></div>';
            $html .= '</div>';
            $html .=  '</form>' . "\n";            
        }
        return $html;
    }
	

    protected function loadCheckboxGroup($field){
        $field['required'] = $field['required'] == 'checked' ? ' required' : false;
        $html = '';
        if(isset($field['title']) && !empty($field['title'])){
           // $html .= sprintf('<label>%s</label>' . "\n", $field['title']);
        } else {
			$field['title']="";
		}
        $field['values'] = (array)$field['values'];
        if(isset($field['values']) && is_array($field['values'])){
            foreach($field['values'] as $item){                
                $item = (array)$item;
                // set the default checked value
                $checked = ($item['baseline'] == 'true') ? true : false;
                // load post value
                $val = $this->getPostValue($this->elemId($item['value']));
                // $checked = !empty($val);
                // if checked, set html
                $checked=$checked?' checked="checked"':'';
                $checkbox='<div class="checkbox"><input type="checkbox" id="%s-%s" name="%s-%s" value="%s"%s /><label for="%s-%s">'.$field['title'].'<br/><span class="onderschrift">%s</span></label></div>'."\n";
                $html.=sprintf($checkbox, $this->elemId($field['title']), $this->elemId($item['value']), $this->elemId($field['name_tag']), $this->elemId($item['value']), $item['value'], $checked, $this->elemId($field['title']), $this->elemId($item['value']), $item['value']);
            }
        }
        return $html;
    }

}