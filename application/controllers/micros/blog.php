<?php
 
class Micros_Blog extends Micros{
 
	public static function random_blogberichten($aantal_items=5){	
		$items=Microitem::getAllItems(2,array(),'random',$aantal_items);
		$filtermeta=Filtermeta::where_language_id( Scotty::get('language') )->order_by('title','asc')->lists('title', 'filter_id');	
		return Beam::make('widgets/blog')->with('items', $items)->with('filtermeta', $filtermeta);
	}
	
	public static function random_blogberichten_mini($aantal_items=5){		
		$items=Microitem::getAllItems(2,array(),'random',$aantal_items);
		$filtermeta=Filtermeta::where_language_id( Scotty::get('language') )->order_by('title','asc')->lists('title', 'filter_id');	
		return Beam::make('widgets/blog_mini')->with('items', $items)->with('filtermeta', $filtermeta);
	}
 
	public function get_detail($slug){
		if(is_array($slug)){ $slug=$slug["slug"]; }
		$filtermeta = Filtermeta::where_language_id( Scotty::get('language') )->order_by('title','asc')->lists('title', 'filter_id');
        $item       = Microitem::getItem(Scotty::$module_id,array("slug"=>"=|".$slug));	
		
		//Vorige en volgende eruit halen
		$previous   = Microitem::getPreviousItem(Scotty::$module_id,$item,array('title' => '<|'));
		$next       = Microitem::getNextItem(Scotty::$module_id,$item);
	
		//Alle h1 en h2 in de tekst naar een h3 veranderen		
		$item->meta_data->content=str_replace("h1>","h3>",$item->meta_data->content);
		$item->meta_data->content=str_replace("h2>","h3>",$item->meta_data->content);
		
		return Beam::make('micros/blog/detail')
					->with('item', $item)
					->with('previous', $previous)				
					->with('next', $next)
					->with('filtermeta', $filtermeta);     
    }
 
	public function get_index(){				
        $items      = Microitem::getAllItems(Scotty::$module_id,array(),array('date_from'=>'desc'));
        $filtermeta = Filtermeta::where_language_id(Scotty::get('language'))->order_by('title','asc')->lists('title', 'filter_id'); 
        return Beam::make('micros/blog/overview')->with('items', $items)->with('filtermeta', $filtermeta);
    } 
 
    public function __call($method, $parameters){
		
		//RSS feed ophalen als daarom wordt gevraagd
		if($parameters[0]=="rssfeed"){
			$nieuwsitems=Microitem::getAllItems(Scotty::$module_id,array(),array('date_from'=>'desc'));
			$inhoud=Beam::make("micros/blog/rss",array("nieuwsitems"=>$nieuwsitems,"module_url"=>Scotty::$module_url));
			echo \Response::make($inhoud,200,array("Content-Type"=>"text/xml"))->render();
			exit;
		}
		
		//Alle andere gevallen, proberen op te halen van een blogitem		
		return $this->get_detail($parameters[sizeof($parameters)-1]);
    }
}