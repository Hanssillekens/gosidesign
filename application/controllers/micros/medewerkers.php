<?php
 
class Micros_Medewerkers extends Micros{ 

	public function get_index(){				
        return geef_medewerkers_overzicht();
    } 
	
	public static function geef_medewerkers_overzicht(){				
        $items = Microitem::getAllItems(3);
        return Beam::make('micros/medewerkers/overview')->with('items', $items);
    } 
 
    public function __call($method, $parameters){
		//Deze wordt aangeroepen als er niet gevonden wordt op de slug (of geen andere functie te vinden is die de call afhandeld)
        return 'whoops';
    }
}
