<?php
 
class Micros_Portfolio extends Micros{
 
	public static function random_vers_werk_items($aantal_items=5)
	{
		$items = Microitem::where_micro_id(1)->where_deleted(0)->where_archived(0)->order_by(DB::raw("RAND()"))->take($aantal_items)->get();	
		$filtermeta = Filtermeta::where_language_id( Scotty::get('language') )->lists('title', 'filter_id');		
		return Beam::make('widgets/vers_werk')->with('items', $items)->with('filtermeta', $filtermeta);
	}
 
	public static function geef_alle_gebruikte_filters()
	{
		$items = Microitem::where_micro_id(1)->where_deleted(0)->get();
		$filtermeta = Filtermeta::where_language_id(Scotty::get('language'))->lists('title', 'filter_id');
		$filters_beschikbaar=array();
		foreach($items as $item){		
			$filters=Filterpair::where_module_id($item->id)->where_module("microitems")->get();
			foreach($filters as $filter){		
				$filters_beschikbaar[$filtermeta[$filter->filter_id]]=1;
			}
		}
		return $filters_beschikbaar;
	}
 
    public function get_index(){
        $items      = Microitem::getAllItems(1);
        $filtermeta = Filtermeta::where_language_id( Scotty::get('language') )->lists('title', 'filter_id'); 
        return Beam::make('micros/portfolio/overview_detail')->with('items', $items)->with('filtermeta', $filtermeta);
    }
 
	//Detail pagina gaat ook naar de overview view, deze bevat ook de detailview
	public function get_detail($slug){
        $items      = Microitem::getAllItems(1);
        $filtermeta = Filtermeta::where_language_id( Scotty::get('language') )->lists('title', 'filter_id');
		$detailitem	= Microitem::getItem(Scotty::$module_id, array("slug"=>"=|".$slug));
        $vorige=Microitem::getAllItems(Scotty::$module_id,array("item"=>array("conditions"=>array("id"=>"<|".$detailitem->id))),array("id"=>"desc"));
		$vorige=(isset($vorige[0]))?$vorige[0]:array();
		$volgende=Microitem::getAllItems(Scotty::$module_id,array("item"=>array("conditions"=>array("id"=>">|".$detailitem->id))),array("id"=>"asc"));
		$volgende=(isset($volgende[0]))?$volgende[0]:array();
        return Beam::make('micros/portfolio/overview_detail')->with('items',$items)->with('filtermeta',$filtermeta)->with('slug',$slug)->with('detailitem',$detailitem)->with('volgende',$volgende)->with('vorige',$vorige);		
    }
	
 
    public function __call($method, $parameters){
       //Alle andere gevallen, proberen op te halen van een blogitem		
		return $this->get_detail($parameters[sizeof($parameters)-1]);
    }
}