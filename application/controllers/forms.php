<?php

class Forms extends Frontend_Forms_Controller{


    public static function before_render()
    {
        parent::before_render();

    }


    /**
     * this function is used to send an email after processing the form.
     * if you don't want an email to be send add "return false" in the start of the function
     * if you wan't to be able to have more control over the email sending replace parent::sendEmail( ... )
     * with your own code
     *
     * @author Sebastiano Bellinzis
     *
     * @param object $form
     * @param object $data
     * @param string $view
     *
     * return void
     */
    protected static function sendEmail( $form, $data, $view = '' )
    {
        if( '' == $view )
        {
            // Please set your default view template here.
            $view = Beam::make('mail/forms/default')->with( 'data', $data );
        }

        parent::sendEmail( $form, $data, $view );
    }
}