<?php

class Base extends Frontend_Base_Controller{

    /**
     * Within this method you can execute code before output will be rendered
     * For instance set the OG data.
     *
     * self::set_og( array('og:title' => 'Title website') ); or dynamically
     * self::set_og( array('og:title' => Beam::page_title() ) );
     *
     * For further reference for OG see http://ogp.me
     *
     */
    public static function before_render()
    {
        // To prevent this method from being called multiple times.
        // We let Scotty now it has been called
        Scotty::$base_rendered = true;




    }
}