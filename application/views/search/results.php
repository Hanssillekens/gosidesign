<?php foreach($results AS $module => $result): ?>
    <h2><?php echo tl('search.'.$module); ?></h2>

    <?php if(empty($result)): ?>
        <div><?php echo tl('search.no.results'); ?></div>
    <?php else: ?>
        <ul>
        <?php foreach($result AS $row): ?>
            <li>
                <h3><a href="<?php echo $row['url']; ?>" title="<?php echo $row['title']; ?>"><?php echo $row['title']; ?></a></h3>
                <div><?php echo Tools::shorten( $row['text'], 200 ); ?></div>
            </li>
        <?php endforeach; ?>
        </ul>
    <?php endif; ?>
<?php endforeach; ?>