<article class="post">
    <span class="print-button" ><a class="noprint" href="javascript:if(window.print)window.print()">Print <img src="<?php echo URL::to_asset('bundles/frontend/img/print.png'); ?>"/></a></span>

    <h1 class="post-title"><?php echo $article->title; ?></h1>
    <div class="entry post-meta">

    </div>
    <div class="entry post-content">
        <?php echo (false != $image && ''  != $image ? '<div class="news-image"><img src="'.$image.'"/></div>' : '' ); ?>
        <?php echo $article->introduction; ?>
        <?php echo $article->content; ?>
    </div>
</article>