<div class="shortcode-news-actueel">
    <span class="rss-button" ><a href="<?php echo Scotty::$module_url.'rss'; ?>" target="_blank"><img src="<?php echo URL::to_asset('bundles/frontend/img/rss.png'); ?>"/></a></span>
    <h2>actueel</h2>
<?php if( !empty($articles) ): ?>
    <ul>
    <?php foreach($articles AS $article ): ?>
        <?php $meta = $article->relationships['meta'][0]; ?>
        <li><a href="<?php echo $module_url.Newsitem::path( $meta->news_id ); ?>"><?php echo $meta->title; ?></a></li>
    <?php endforeach; ?>
    </ul>
    <h2 class="archive"><a href="<?php echo Scotty::$module_url.$translations['archive'][$language]; ?>">Archief</a></h2>
<?php else: ?>
    <ul>
        <li>Geen items gevonden!</li>
    </ul>
<?php endif; ?>
</div>