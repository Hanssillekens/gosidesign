<div id="map_canvas" style="width: 100%; height: 495px;"></div>


<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>


<script>
    function gmaps_initialize() {
        var myOptions = {
            zoom        : 15,
            mapTypeId   : google.maps.MapTypeId.ROADMAP,
            minZoom     : 8,
            styles      : [{
                    featureType : "poi",
                    elementType : "labels",
                    stylers     : [{ visibility: "off" }]
            }],

            panControl          : false,
            streetViewControl   : false,
            zoomControlOptions  : {
                style : 'SMALL'
            }
        }

        map = new google.maps.Map( document.getElementById("map_canvas"), myOptions );

        var json            = <?php echo $markers; ?>;
        var markers         = [];
        var latlngbounds    = new google.maps.LatLngBounds();
        var OpenInfoWindow  = null;

        jQuery.each(json, function(i)
        {
            point = this;
            console.log(point);
            MarkerLatLng = new google.maps.LatLng(point.lat, point.lng);

            latlngbounds.extend(MarkerLatLng);

            var marker = new google.maps.Marker({
                position    : MarkerLatLng,
                title       : point.title,
                map         : map,
                infowindow  : new google.maps.InfoWindow({
                    content : point.content
                })
            });

            google.maps.event.addListener(marker, 'click', function()
            {
                if( null != OpenInfoWindow ){
                    OpenInfoWindow.close();
                }

                OpenInfoWindow = this.infowindow;
                OpenInfoWindow.open(map, marker);
            });

            markers.push(marker);
        });

        map.setCenter( latlngbounds.getCenter() );
        map.fitBounds( latlngbounds );
    }

    gmaps_initialize();
</script>