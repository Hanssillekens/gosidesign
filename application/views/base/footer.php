	<div id="footer">
		<div class="content">
			<?php
			echo Beam::pages_by_menu_columns(2);
			
			//TODO Social Media -> Nieuwsbrief aanmelden
			?>		
			<div class="kolom socialmedia">
				<h5><?php echo tl("footer-socialmedia-titel"); ?></h5>
				<?php
					//nagaan welke links er naar de social media pagina's zijn ingevuld en deze tonen      
					if(trim(Scotty::$settings["linkedin_url"])!=""){ echo "<a href=\"".Scotty::$settings["linkedin_url"]."\" target=\"_blank\"><img src=\"/images/icoon.linkedin.footer.png\" alt=\"".tl("footer-slogan-linkedin")."\" title=\"".tl("footer-slogan-linkedin")."\"/> LinkedIn</a>"; }
					if(trim(Scotty::$settings["facebook_url"])!=""){ echo "<a href=\"".Scotty::$settings["facebook_url"]."\" target=\"_blank\"><img src=\"/images/icoon.facebook.footer.png\" alt=\"".tl("footer-slogan-facebook")."\" title=\"".tl("footer-slogan-facebook")."\"/> Facebook</a>"; }
					if(trim(Scotty::$settings["twitter_url"])!=""){ echo "<a href=\"".Scotty::$settings["twitter_url"]."\" target=\"_blank\"><img src=\"/images/icoon.twitter.footer.png\" alt=\"".tl("footer-slogan-twitter")."\" title=\"".tl("footer-slogan-twitter")."\"/> Twitter</a>"; }
					if(trim(Scotty::$settings["google_plus_url"])!=""){ echo "<a href=\"".Scotty::$settings["google_plus_url"]."\" target=\"_blank\"><img src=\"/images/icoon.googleplus.footer.png\" alt=\"".tl("footer-slogan-googleplus")."\" title=\"".tl("footer-slogan-googleplus")."\"/> Google+</a>"; }				
				?>
			</div>
			<div class="kolom">
				<h5><?php echo tl("footer-nieuwsbrief-titel"); ?></h5>
				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="/js/scripts.js"></script>
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-1027446-1']);
	_gaq.push(['_trackPageview']);
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>