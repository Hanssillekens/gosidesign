<div id="website_container">
	<div id="header">
		<div class="content">
			<div class="mobielmenu_activator"></div>
			<div class="logo"><a href="/"><img src="/images/logo.gosidesign.png" alt="<?=Scotty::$settings["site_title"]?>" title="<?=Scotty::$settings["site_title"]?>"/></a></div>      
			<div class="menu">
			<?php echo Beam::menu("navigation", "mainNav"); ?>
			</div>        
		</div>
	</div>
	<div class="menu_mobiel">
		<?php echo Beam::menu("nav-bar", "nav"); ?>
	</div>