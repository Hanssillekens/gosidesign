<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xml:lang="<?php echo Config::get('frontend::runtime.language.default'); ?>" prefix="og: http://ogp.me/ns#">
<head>
	<title><?php echo Beam::page_title(); ?></title>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/jquery.timer.js"></script>
	<script type="text/javascript" src="/js/jquery.icheck.min.js"></script>	 
    <script type="text/javascript" src="/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="/rs-plugin/js/jquery.themepunch.revolution.js"></script>
	<?php foreach(Beam::$og AS $og_row): ?>
	    <?php echo $og_row; ?>
	<?php endforeach; ?>
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Arimo:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/rs-plugin/css/settings.css" type="text/css" media="all" />
    <link rel="stylesheet" href="/rs-plugin/css/captions.css" type="text/css" media="all" />
	<link rel="stylesheet" media="screen" href="/css/jquery.icheck.css" />
	<link rel="stylesheet" media="screen" href="/css/styling_algemeen.css" />
	<link rel="stylesheet" media="screen and (max-width:999px)" href="/css/styling_mobiel.css" />
	<link rel="stylesheet" media="screen and (min-width:1000px)" href="/css/styling_desktop.css" />	
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="/images/favicon.ico" />
</head>

