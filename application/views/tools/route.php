<div class="routewidget">
    <?php if('true' == $use_google_maps): ?>
        <div id="googlemap"></div>
    <?php endif; ?>
    <div class="adres">
		<h2><?php echo tl("routeplanner-bereken-je-route"); ?></h2>
        <input type="text" id="maps-adres" data-default="<?=tl("routeplanner-veld-adres")?>" />
        <input type="text" id="maps-plaats" data-default="<?=tl("routeplanner-veld-plaats")?>" />
        <a href="#getdirections" class="knop"><?php echo "plan route"; ?></a>
		<div id="directions"></div>
    </div>   		
</div>
<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
<script>
    $('a[href="#getdirections"]').click(function(e) {
        e.preventDefault();
        var adres = $('#maps-adres').val();
        var plaats = $('#maps-plaats').val();
        if(adres != '' && plaats != ''){
            getDirections(adres+', '+ plaats);
        } else {
            alert("<?=tl("routeplanner-fout-alle-velden-invullen")?>");
        }
    });
    $('#maps-plaats').keyup(function(e){
        if(e.keyCode == 13) {
            $('a[href="#getdirections"]').click();
        }
    });
	

    var directionDisplay;
    var map;
    function doGoogleMaps() {
        directionsDisplay = new google.maps.DirectionsRenderer({draggable:true, suppressMarkers:true});
        var geocoder = new google.maps.Geocoder();
        if(geocoder) {
            geocoder.geocode({'address':'<?php echo $street.', '.$city; ?>'}, function(results, status) {
                if(status == google.maps.GeocoderStatus.OK) {
                    var gLat = results[0].geometry.location.lat();
                    var gLng = results[0].geometry.location.lng();
                }
                var latlng = new google.maps.LatLng(gLat, gLng);
                var myOptions = {zoom:14, center:latlng, mapTypeId:google.maps.MapTypeId.ROADMAP};
                map = new google.maps.Map(document.getElementById('googlemap'), myOptions);
                new google.maps.Marker({position:latlng, map:map, icon:'/images/marker.googlemaps.png'});
                directionsDisplay.setMap(map);
                directionsDisplay.setPanel(document.getElementById("directions"));				
            });
        }
		//Na vertraging de zoom veranderen omdat er een stuk over de kaart staat van de beschrijving
		google.maps.event.addListener(directionsDisplay,'directions_changed',function(){
			var timer=$.timer(function() {
				var newZoom=map.getZoom()-1; 
				map.setZoom(newZoom);
			});
			timer.once(300);
		});
    }
	

    function getDirections(address){
        var directionsService = new google.maps.DirectionsService();
        directionsService.route({origin:address, destination:'<?php echo $street.', '.$city; ?>', provideRouteAlternatives:false, travelMode:google.maps.DirectionsTravelMode.DRIVING, unitSystem:google.maps.DirectionsUnitSystem.METRIC}, function(result, status){
            if(status == google.maps.DirectionsStatus.OK){
                directionsDisplay.setDirections(result);
                var geocoder = new google.maps.Geocoder();
                if(geocoder){
                    geocoder.geocode({'address':address}, function(results, status){
                        if(status == google.maps.GeocoderStatus.OK){
                            var gLat = results[0].geometry.location.lat();
                            var gLng = results[0].geometry.location.lng();
                        }
                        var latlng2 = new google.maps.LatLng(gLat,gLng);
                        new google.maps.Marker({position:latlng2, map:map});
                    });					
                }
				//Verwijderen voorgaande links en toevoegen link naar routebeschrijving
				$(".contactpagina #directions>a").remove();
				$(".contactpagina #directions").prepend("<a href=\"https://maps.google.com/maps?saddr="+address+"&daddr=<?php echo str_replace(" ","+",$street.', '.$city); ?>&hl=nl&mra=ls&t=m&z=12\" target=\"_blank\"><?php echo tl("route-link-routebeschrijving"); ?></a>");				 				
				
            } else {
                alert('<?=tl("routeplanner-fout-adres-niet-gevonden")?>');
            }
        });
    }
	
	
	

    <?php if('true' == $use_google_maps): ?>
        doGoogleMaps();
    <?php endif; ?>
</script>