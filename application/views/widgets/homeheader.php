<div id="homeheader">
    <div class="slider">
        <?php
        //Random foto ophalen van de pagina zelf
		$homeblokken=Micros_Homeblokken::geef_alle_homeblokken();
        if(count($homeblokken)>0){    
			foreach($homeblokken as $homeblok){	
				//Afbeelding ophalen al die er is
				$afbeelding=array();
				$afbeelding=Media::get_first_image("microitems",$homeblok->id);
				//Als er een afbeelding is, dan ook de slide plaatsen
				if(count($afbeelding)>0){
					$type_container="div";
					//Kijken of er een relatie is met een project	
					$relatie=$homeblok->relation_with_module_item;
					if(count($relatie)>0){
						switch($relatie["relation_module_micro_name"]){							
							case "Portfolio":
								//Portfolio item instellingen
								$type_container="a";
								$link="href=\"".Tools::get_page_url_by_module($relatie["micro_items_id"]).$relatie["slug"]."/\"";								
								//Ophalen van alle unieke filters die worden gebruikt bij dit portfolioitem
								$filters_beschikbaar=array();
								$filtermeta=Filtermeta::where_language_id(Scotty::get('language'))->order_by('title','asc')->lists('title', 'filter_id');									
								$filters=Filterpair::where_module_id($relatie["id"])->where_module("microitems")->get();
								foreach($filters as $filter){		
									$filters_beschikbaar[$filtermeta[$filter->filter_id]]=1;
								}
								//Kijken of er buttons aan gekoppeld kunnen worden
								var_dump($filters_beschikbaar);
								
							break;						
						}
					}					
					echo "<".$type_container." ".$link." class=\"slide\"><img src=\"".$afbeelding["cropped"]."\" alt=\"".$afbeelding["title"]."\" title=\"".$afbeelding["title"]."\" /></".$type_container.">";
				}
				
			}          
        }
		?>
    </div>
</div>