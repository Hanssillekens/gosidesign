<?php

//Blog widget
if(count($items)>0){
	echo "<div id=\"blogwidget\">";
		$item=$items[0];
		//Auteur ophalen
		if(isset($item->relation_with_module_item)){ $auteur=$item->relation_with_module_item; } else { $auteur=""; }
		//Afbeeldingen van die auteur ophalen
		$afbeelding=array();
		if($auteur!=""){ $afbeelding=Media::get_all_images("microitems",$auteur["micro_items_id"]); }	
		if($auteur!=""){ $auteurnaam=$auteur["title"]; } else { $auteurnaam=tl("blog-anoniem"); }
		
		$filters=Filterpair::where_module_id($item->id)->where_module("microitems")->get();
		$data_filters="";
		$datum_url=date("Y/n/j/",strtotime($item->date_from));
		echo "<a href=\"".Tools::get_page_url_by_module("micros|2").$datum_url.$item->meta_data->slug."/\" class=\"item\"\">\n";
			echo "<h3 class=\"desktop_only\">".$item->meta_data->title."</h3>";
			echo "<div class=\"links\">";
				
				if(count($afbeelding)>0){
					$afbeeldingspad=$afbeelding[0]["cropped"];
				} else {
					//dummy afbeelding
					$afbeeldingspad="/images/dummies/blog.persoon.png";
				}
				echo "<div class=\"afbeelding\"><img src=\"".$afbeeldingspad."\" alt=\"".$auteurnaam."\" title=\"".$auteurnaam."\"/></div>";
				echo "<h3 class=\"mobiel_only\">".$item->meta_data->title."</h3>";
				echo "<div class=\"datum\">".Tools::geef_nederlands_datum($item->date_from)."</div>";
			echo "</div>";
			echo "<div class=\"rechts\">";
				echo strip_tags($item->meta_data->introduction,"<p>");
			echo "</div>";
		echo "</a>";
	echo "</div>";		
}