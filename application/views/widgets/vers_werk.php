<?php

//Vers werk widget
if(count($items)>0){
	echo "<div id=\"vers_werk\">";
		echo "<div class=\"items\">";
			echo "<div class=\"slider\">";
				foreach($items as $item){
					$meta=$item->meta()->first();
					$filters=Filterpair::where_module_id($item->id)->where_module("microitems")->get();
					$data_filters="";
					foreach($filters as $filter){
						$data_filters.=$filtermeta[$filter->filter_id]." ";
					}	
					echo "<a href=\"".Scotty::$module_url.$meta->slug."/\" class=\"item\" data-filters=\"".$data_filters." ".tl("portfolio-filters-alles")."\">\n";
						//Random afbeelding hierin zetten
						$afbeelding=array();
						$afbeeldingoverzicht="";
						$afbeelding=Media::get_random_image("microitems",$item->id,"Overzicht");
						if(count($afbeelding)>0){								
							$afbeeldingoverzicht="<img src=\"".$afbeelding["cropped"]."\" alt=\"".$afbeelding["title"]."\" title=\"".$afbeelding["title"]."\" />";
						}
						echo "<div class=\"afbeelding\"><div class=\"overlay\"></div>".$afbeeldingoverzicht."</div>";
						echo "<div class=\"info\">";
							echo "<h3>".$meta->title."</h3>";
							echo "<span class=\"onderschrift\">".tl("portfolio-klant")." ".$meta->copyright_title."</span><br/>";
							echo "<span class=\"filters\">".str_replace(", /,"," /",str_replace(" ",", ",trim($data_filters)))."</span>";
						echo "</div>";
					echo "</a>";
				}
			echo "</div>";
		echo "</div>";
	echo "</div>";
}