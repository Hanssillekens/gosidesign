<?php

//Blog widget
if(count($items)>0){
	echo "<div id=\"blogminiwidget\">";
		echo "<div class=\"items\">";
			echo "<div class=\"slider\">";
				foreach($items as $item){
					//Auteur ophalen
					if(isset($item->relation_with_module_item)){ $auteur=$item->relation_with_module_item; } else { $auteur=""; }
					//Afbeeldingen van die auteur ophalen
					$afbeelding=array();
					if($auteur!=""){ $afbeelding=Media::get_all_images("microitems",$auteur["micro_items_id"]); }	
					if($auteur!=""){ $auteurnaam=$auteur["title"]; } else { $auteurnaam=tl("blog-anoniem"); }
					
					$filters=Filterpair::where_module_id($item->id)->where_module("microitems")->get();
					$data_filters="";$array_filters=array();
					foreach($filters as $filter){
						$array_filters[]=$filtermeta[$filter->filter_id];
					}	
					$datum_url=date("Y/n/j/",strtotime($item->date_from));
					echo "<a href=\"".Tools::get_page_url_by_module("micros|2").$datum_url.$item->meta_data->slug."/\" class=\"item\"\">\n";
						echo "<h3>".$item->meta_data->title."</h3>";						
						if(count($afbeelding)>0){
							$afbeeldingspad=$afbeelding[0]["cropped"];
						} else {
							//dummy afbeelding
							$afbeeldingspad="/images/dummies/blog.persoon.png";
						}
						if(count($array_filters)>0){
							echo "<div class=\"filters\">";
								echo implode(" - ",$array_filters);
							echo "</div>";
						}
						echo "<div class=\"dummy\">";
							echo "<div class=\"links\">";
								echo "<div class=\"afbeelding\"><img src=\"".$afbeeldingspad."\" alt=\"".$auteurnaam."\" title=\"".$auteurnaam."\"/></div>";
							echo "</div>";
							echo "<div class=\"rechts\">";
								echo "<div class=\"datum\">".Tools::geef_nederlands_datum($item->date_from)."</div>";
							echo "</div>";	
						echo "</div>";
					echo "</a>";
				}
			echo "</div>";
		echo "</div>";
	echo "</div>";
}