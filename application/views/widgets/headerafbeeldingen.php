<?php
//Kijken of de pagina zelf headerafbeeldingen heeft of de hoofdpagina...
$hoofdpagina_headers=Media::get_all_images("pages",Scotty::$page_array[0],"page_header");
if((Beam::$headers!=null)or(count($hoofdpagina_headers)>0)){
    echo "<div id=\"headerafbeeldingen\">";
        echo "<div class=\"slider\">";
            if(Beam::$headers!=null){
                $afbeeldingen=Beam::$headers;
            } else {
                $afbeeldingen=$hoofdpagina_headers;
            }
            $aantal_afbeeldingen=count($afbeeldingen);
            foreach($afbeeldingen as $afbeelding){
                echo "<div class=\"slide\"><img src=\"".$afbeelding["cropped"]."\" alt=\"".$afbeelding["title"]."\" title=\"".$afbeelding["title"]."\"/></div>";
            }
        echo "</div>";
        if($aantal_afbeeldingen>1){
            echo "<div class=\"navigatie\">";
                for($teller=1;$teller<=$aantal_afbeeldingen;$teller++){
                    echo "<div data-nummer=\"".$teller."\"></div>";
                }
            echo "</div>";
        }
    echo "</div>";
}
?>
