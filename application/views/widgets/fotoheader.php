<div id="fotoheader">
    <?php
    //Random foto ophalen van de pagina zelf
    $afbeeldingen_array_huidige_pagina=Beam::$backgrounds;
    if(count($afbeeldingen_array_huidige_pagina)>0){
        shuffle($afbeeldingen_array_huidige_pagina);
        echo "<img src=\"".$afbeeldingen_array_huidige_pagina[0]["cropped"]."\" alt=\"".$afbeeldingen_array_huidige_pagina[0]["title"]."\" title=\"".$afbeeldingen_array_huidige_pagina[0]["title"]."\" />";
    } else {
        //of, als deze er geen heeft, van de parent
        $afbeeldingen_array_hoofdpagina=Media::get_random_image("pages",Scotty::$page_array[0],"page_background");
        if(count($afbeeldingen_array_hoofdpagina)>0){ echo "<img src=\"".$afbeeldingen_array_hoofdpagina["cropped"]."\" alt=\"".$afbeeldingen_array_hoofdpagina["title"]."\" title=\"".$afbeeldingen_array_hoofdpagina["title"]."\" />"; }
    }
    //Titel van de (hoofd)pagina ophalen en tonen in de fotoheaderbalk
    $hoofdpagina_info=Tools::page_info_by_id(Scotty::$page_array[0]); ?>
	<div class="content">
		<h1><?=$hoofdpagina_info["page_title"]?></h1>
	</div>
</div>