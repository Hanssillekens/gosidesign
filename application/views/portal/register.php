<?php if (isset($error) && '' != $error): ?>
    <span class="error"><?php print_r($error); ?></span>
<?php endif; ?>

<form method="POST">
    <label><?php echo tl('login.email'); ?></label>
    <input type="text" name="email" value="" placeholder="email">

    <label><?php echo tl('login.firstname'); ?></label>
    <input type="text" name="firstname" value="" placeholder="firstname">

    <label><?php echo tl('login.lastname'); ?></label>
    <input type="text" name="lastname" value="" placeholder="lastname">

    <label><?php echo tl('login.password'); ?></label>
    <input type="password" name="password" value="" placeholder="password">

    <label><?php echo tl('login.password'); ?></label>
    <input type="password" name="password_verify" value="" placeholder="password_verify">

    <br/>
    <br/>
    <input type="submit" value="submit">
</form>