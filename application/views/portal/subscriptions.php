<form method="POST" name="subscribe_form">
    <input type="hidden" name="subscribe_form" value="1"/>
    <?php foreach($categories AS $key => $category ): ?>
    <div>
        <?php $checked = ( array_key_exists($key, $categories_user) && $categories_user[$key] == 1 ? 'checked=checked' : '' ); ?>
        <input type="checkbox" name="categories[<?php echo $key; ?>]" value='1' <?php echo $checked; ?>/> <?php echo $category->title; ?>
    </div>
    <?php endforeach; ?>
    <br/>
    <br/>

    <div>
        <?php if( Session::has('subscription_error') ): ?>
        <div class="error"><?php echo Session::get('subscription_error'); ?></div>
        <?php endif; ?>

        <input type="checkbox" name="terms" value="1"/><?php echo tl('By clicking on the "Submit" button, you agree that you have read, understand, and accept the terms and conditions. If you do not agree to these terms and conditions, do not subscribe.'); ?>
    </div>

    <br/>
    <br/>

    <input type="submit" value="submit" />
</form>