<?php if(Auth::guest()): ?>
    <ul>
        <li><a href="<?php echo Scotty::$module_url; ?>">Login</a></li>
        <li><a href="<?php echo Scotty::$module_url; ?>register">Register</a></li>
    </ul>
<?php else: ?>
    <ul>
        <li><a href="<?php echo Scotty::$module_url; ?>profile">Profile</a></li>
        <li><a href="<?php echo Scotty::$module_url; ?>account">Account</a></li>
        <li><a href="<?php echo Scotty::$module_url; ?>subscription">Subscriptions</a></li>
        <li><a href="<?php echo Scotty::$module_url; ?>logout">Logout</a></li>
    </ul>
<?php endif; ?>