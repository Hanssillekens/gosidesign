<div id="tabs">
    <ul>
        <li><a href="#tab-1">Personal</a></li>
        <li><a href="#tab-2">Company</a></li>
        <li><a href="#tab-3">Address</a></li>
    </ul>

    <div id="tab-1">
        <h3>Personal Profile</h3>
        <form method="POST" action="<?php echo Scotty::$module_url.'save_user'; ?>">
            <label>First Name</label>
            <input type="text" value="<?php echo Auth::user()->firstname; ?>" name="firstname">
            <label>Last Name</label>
            <input type="text" value="<?php echo Auth::user()->lastname; ?>" name="lastname">
            <br/>
            <br/>
            <input type="submit" value="submit">
        </form>
    </div>

    <div id="tab-2">
        <h3>Company Profile</h3>
        <form method="POST" action="<?php echo Scotty::$module_url.'save_extended'; ?>">
            <label><?php echo tl('Company Name'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_name; ?>" name="company_name">
            <label><?php echo tl('Company Address'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_address; ?>" name="company_address">
            <label><?php echo tl('Company Address Extra'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_address_extra; ?>" name="company_address_extra">
            <label><?php echo tl('Company Number'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_number; ?>" name="company_number">
            <label><?php echo tl('Company Box'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_box; ?>" name="company_box">
            <label><?php echo tl('Company Postalcode'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_postalcode; ?>" name="company_postalcode">
            <label><?php echo tl('Company City'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_city; ?>" name="company_city">
            <label><?php echo tl('Company Region'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_region; ?>" name="company_region">
            <label><?php echo tl('Company Country'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_country_id; ?>" name="company_country_id">
            <label><?php echo tl('Company Language'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_language_id; ?>" name="company_language_id">
            <label><?php echo tl('Company Email'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_email; ?>" name="company_email">
            <label><?php echo tl('Company Phone'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_phone; ?>" name="company_phone">
            <label><?php echo tl('Company Fax'); ?></label>
            <input type="text" value="<?php echo $extended_user->company_fax; ?>" name="company_fax">
            <label><?php echo tl('vat'); ?></label>
            <input type="text" value="<?php echo $extended_user->vat; ?>" name="vat">
            <label><?php echo tl('kvk'); ?></label>
            <input type="text" value="<?php echo $extended_user->kvk; ?>" name="kvk">
            <br/>
            <br/>
            <input type="submit" value="submit">
        </form>
    </div>

    <div id="tab-3">
        <h3>Address</h3>
        <form method="POST" action="<?php echo Scotty::$module_url.'save_address'; ?>">
            <label><?php echo tl('Address'); ?></label>
            <input type="text" value="<?php echo $address_user->address; ?>" name="address">

            <label><?php echo tl('Address Extra'); ?></label>
            <input type="text" value="<?php echo $address_user->address_extra; ?>" name="address_extra">

            <label><?php echo tl('Number'); ?></label>
            <input type="text" value="<?php echo $address_user->number; ?>" name="number">
            <label><?php echo tl('Box'); ?></label>
            <input type="text" value="<?php echo $address_user->box; ?>" name="box">
            <label><?php echo tl('Postalcode'); ?></label>
            <input type="text" value="<?php echo $address_user->postalcode; ?>" name="postalcode">
            <label><?php echo tl('City'); ?></label>
            <input type="text" value="<?php echo $address_user->city; ?>" name="city">
            <label><?php echo tl('Region'); ?></label>
            <input type="text" value="<?php echo $address_user->region; ?>" name="region">
            <label><?php echo tl('Country'); ?></label>
            <input type="text" value="<?php echo $address_user->country_id; ?>" name="country_id">
            <br/>
            <br/>
            <input type="submit" value="submit">
        </form>

    </div>
</div>

<script>

    $(document).ready(function(){
        $('#tabs div').hide();
        $('#tabs div:first').show();
        $('#tabs ul li:first').addClass('active');

        $('#tabs ul li a').click(function(){
            $('#tabs ul li').removeClass('active');
            $(this).parent().addClass('active');
            var currentTab = $(this).attr('href');
            $('#tabs div').hide();
            $(currentTab).show();
            return false;
        });
    });

</script>