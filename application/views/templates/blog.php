<?php echo $head; ?>
    <body>
        <?php echo $header; ?>
        <?php echo Beam::make("widgets/fotoheader"); ?>
        <div id="container_website">
            <div id="content">
				<?php
				echo Beam::$module_content;
				?>
            </div>
        </div>
        <?php echo $footer; ?>
    </body>
</html>