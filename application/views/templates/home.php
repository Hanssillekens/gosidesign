<?php echo $head; ?>
    <body>	
        <?php echo $header; ?>
        <?php 
			//echo Beam::make("widgets/homeheader"); 
		?>
		<?php echo Shortcode::build('[slider header]'); ?>
        <div id="container_website" class="homepagina">
            <div id="content">
				<?php
				//Filterbalk opbouwen
				$filters_beschikbaar=Micros_Portfolio::geef_alle_gebruikte_filters();
				echo "<div class=\"filterbalk\">";
					echo "<ul>";
						echo("<li>".tl("portfolio-ons-werk")."</li>");
						echo("<li class=\"optie alles\"><a href=\"".Tools::get_page_url_by_module("micros|1")."\">".tl("portfolio-filters-alles")."</a></li>");
						foreach($filters_beschikbaar as $filter=>$dummy){
							echo("<li class=\"optie\"><a href=\"".Tools::get_page_url_by_module("micros|1")."#".$filter."\">".$filter."</a></li>");
						}
					echo "<ul>";
				echo "</div>";
				echo "<h3 class=\"desktop_only\">".tl("home-vers-werk-titel")."</h3>";
				//Widget vers werk (laatste 5) laten zien
				echo Micros_Portfolio::random_vers_werk_items(5);
				?>
				<div class="dummy">
					<div class="tekstblok eerste">
						<?php echo str_replace("<hr />","</div><div class=\"tekstblok\">",shortCode::build(Beam::$page->content)); ?>
					</div>
					<?php
					//Widget blog laten zien
					echo Micros_Blog::random_blogberichten();
                    ?>
				</div>
            </div>
        </div>
		<?php echo $footer; ?>
    </body>
</html>