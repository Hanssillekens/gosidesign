<?php echo $head; ?>
    <body>
        <?php echo $header; ?>
        <?php echo Beam::make("widgets/fotoheader"); ?>
        <div id="container_website" class="contactpagina">
			<a name="route"></a>
			<?php 
				//Routeplanner
				echo Shortcode::build("[routeplanner use_google_maps=true street=Stationsplein_27 city=Maastricht]"); 
			?>
            <div id="content">
				<img class="afbeelding" src="/images/afbeelding.contactpagina.content.png" alt="<?php echo tl("contactpagina-afbeelding-content-titel") ?>" title="<?php echo tl("contactpagina-afbeelding-content-titel") ?>" />
                <?php 
				//Alle h1 en h2 in de tekst naar een h3 veranderen	
				$content_pagina=str_replace("h1>","h3>",Beam::$page->content);
				$content_pagina=str_replace("h2>","h3>",$content_pagina);
				echo $content_pagina;
				?>
				<div class="blokken">
					<div class="blok">
						<div class="afbeelding"><img src="/images/afbeelding.contactpagina.blok.1.jpg" alt="<?php echo tl("contactpagina-blok-naw-titel"); ?>" title="<?php echo tl("contactpagina-blok-naw-titel"); ?>"></div>
						<h4><?php echo tl("contactpagina-blok-naw-titel"); ?></h4>
						<?php echo nl2br(tl("contactgegevens")); ?>
					</div>
					<div class="blok social">
						<div class="afbeelding"><img src="/images/afbeelding.contactpagina.blok.2.jpg" alt="<?php echo tl("contactpagina-blok-socialmedia-titel"); ?>" title="<?php echo tl("contactpagina-blok-naw-titel"); ?>"></div>
						<h4><?php echo tl("contactpagina-blok-socialmedia-titel"); ?></h4>
						<?php
							//nagaan welke links er naar de social media pagina's zijn ingevuld en deze tonen      
							if(trim(Scotty::$settings["linkedin_url"])!=""){ echo "<a href=\"".Scotty::$settings["linkedin_url"]."\" target=\"_blank\"><img src=\"/images/icoon.linkedin.contact.png\" alt=\"".tl("footer-slogan-linkedin")."\" title=\"".tl("footer-slogan-linkedin")."\"/> LinkedIn</a>"; }
							if(trim(Scotty::$settings["facebook_url"])!=""){ echo "<a href=\"".Scotty::$settings["facebook_url"]."\" target=\"_blank\"><img src=\"/images/icoon.facebook.contact.png\" alt=\"".tl("footer-slogan-facebook")."\" title=\"".tl("footer-slogan-facebook")."\"/> Facebook</a>"; }
							if(trim(Scotty::$settings["twitter_url"])!=""){ echo "<a href=\"".Scotty::$settings["twitter_url"]."\" target=\"_blank\"><img src=\"/images/icoon.twitter.contact.png\" alt=\"".tl("footer-slogan-twitter")."\" title=\"".tl("footer-slogan-twitter")."\"/> Twitter</a>"; }
							if(trim(Scotty::$settings["google_plus_url"])!=""){ echo "<a href=\"".Scotty::$settings["google_plus_url"]."\" target=\"_blank\"><img src=\"/images/icoon.googleplus.contact.png\" alt=\"".tl("footer-slogan-googleplus")."\" title=\"".tl("footer-slogan-googleplus")."\"/> Google+</a>"; }				
						?>
					</div>
					<div class="blok">
						<div class="afbeelding"><img src="/images/afbeelding.contactpagina.blok.3.jpg" alt="<?php echo tl("contactpagina-blok-nieuwsbrief-titel"); ?>" title="<?php echo tl("contactpagina-blok-nieuwsbrief-titel"); ?>"></div>
						<h4><?php echo tl("contactpagina-blok-nieuwsbrief-titel"); ?></h4>
						<?php echo nl2br(tl("contactpagina-blok-nieuwsbrief-tekst")); ?>
					</div>
				</div>
            </div>			
			<div id="contactformulier">
				<div class="content">
					<a name="contactformulier"></a>
					<?php 
					echo "<h3>".tl("contactpagina-titel-contactformulier")."</h3>"; 
					echo Shortcode::build("[forms id=1]");
					?>
				</div>
			</div>
			<div id="locatiefilm">
				<a name="locatie"></a>
				<?php 
				echo "<h3>".tl("contactpagina-titel-locatiefilm")."</h3>";
				echo Shortcode::build("[vimeo href=https://vimeo.com/74711215]"); 
				?>
			</div>
        </div>
        <?php echo $footer; ?>
		<script type="text/javascript">	
			$(document).ready(function(){
				$("#contactformulier form input[type=checkbox]").iCheck({
				   cursor: true
				});
			});
		</script>
    </body>
</html>

