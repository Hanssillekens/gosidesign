<?php echo $head; ?>
    <body>
        <?php echo $header; ?>
        <?php echo Beam::make("widgets/fotoheader"); ?>
        <div id="container_website" class="team_werken_bij">
            <div id="content">
				<a name="gosidesign"></a>
				<img class="afbeelding" src="/images/afbeelding.gosipagina.overons.png" alt="<?php echo tl("gosipagina-afbeelding-overons-titel") ?>" title="<?php echo tl("gosipagina-afbeelding-overons-titel") ?>" />
				<?php
				$inhoud="";
				$inhoud=Beam::$page->content;
				$inhoud=explode("<hr />",$inhoud);
				echo $inhoud[0];
				?>
            </div>
			<div id="team">
				<div class="content">
					<a name="onzemensen"></a>
					<h3><?php echo tl("medewerkers-titel"); ?></h3>
					<?php
					//Alle medewerkers laten zien
					echo Micros_Medewerkers::geef_medewerkers_overzicht();
                    ?>
				</div>
			</div>
			<div id="werken_bij">
				<a name="werkenbij"></a>
				<img class="afbeelding" src="/images/afbeelding.gosipagina.werkenbij.png" alt="<?php echo tl("gosipagina-afbeelding-werkenbij-titel") ?>" title="<?php echo tl("gosipagina-afbeelding-werkenbij-titel") ?>" />
				<?php
					echo $inhoud[1];
				?>
			</div>
        </div>
        <?php echo $footer; ?>
    </body>
</html>