<?php echo $head; ?>
    <body>
        <?php echo $header; ?>
        <?php echo Beam::make("widgets/fotoheader"); ?>
        <div id="container_website">
            <div id="content">
				<div class="rechts">
                    <?php 
					//Submenu weergeven als dat er is
					if(Beam::$menu_sub!=null){
                        echo "<div id=\"submenu\">";
                            echo Beam::$menu_sub;
                        echo "</div>";
                    }
                    ?>
                    <?php
					//Widget vers werk laten zien
					echo Micros_Portfolio::random_vers_werk_items();
					//Contactknop					
					echo "<a href=\"".Tools::get_page_url_by_template("5")."\" class=\"paginacontactknop\">".tl("pagina-contactknop-titel")."</a>";
					//Widget blog laten zien
					echo Micros_Blog::random_blogberichten_mini();
                    ?>
                </div>
                <div class="links">
                    <?php echo Beam::make("widgets/headerafbeeldingen"); ?>
                    <?php
                    echo "<div class=\"inhoud\">";
                        echo "<h2>".Beam::$title."</h2>";
                        echo shortCode::build(Beam::$page->content);
						echo Beam::$module_content;
                        echo Beam::make("widgets/delen");
                    echo "</div>";
                    ?>
                </div>                
            </div>
        </div>
        <?php echo $footer; ?>
    </body>
</html>