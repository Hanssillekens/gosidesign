<?php header ("Content-Type:text/xml"); ?>
<?php echo '<?xml version="1.0" encoding="utf-8"?>'; ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <title><?php echo Scotty::$settings['site_title']; ?></title>
        <link><?php echo $module_url.'rss'; ?></link>
        <language><?php echo Scotty::get_language(); ?></language>
        <copyright><?php echo Scotty::$settings['site_title']; ?></copyright>
        <ttl>30</ttl>
        <atom:link href="<?php echo $module_url.'rssfeed/'; ?>" rel="self" type="application/rss+xml" />
		<?php if(!empty($nieuwsitems)): ?>
            <?php foreach($nieuwsitems AS $nieuwsitem): ?>
                <?php $meta=$nieuwsitem->meta_data; ?>
                <item>
                    <title><![CDATA[<?php echo $meta->title; ?>]]></title>
                    <description><![CDATA[<?php echo $meta->introduction; ?>]]></description>
                    <link><![CDATA[<?php echo $module_url.$meta->slug."/" ?>]]></link>
					<pubDate><?php echo date("D, d M Y H:i:s",strtotime($nieuwsitem->date_from)); ?></pubDate>
                </item>
            <?php endforeach; ?>
        <?php endif; ?>
    </channel>
</rss>