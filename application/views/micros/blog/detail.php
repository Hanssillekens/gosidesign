<div id="fb-root"></div>
<script type="text/javascript">
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

<div id="blogdetail">
	<?php
	//Navigatie naar volgende en vorige alleen laten zien als er ook een vorige of volgende item is
	if((!empty($previous))or(!empty($next))){ ?>
		<div class="navigatie">
			<?php 
			if($previous!=null){
				echo "<a href=\"".Tools::get_page_url_by_module("micros|2").$previous->meta_data->slug."/\" class=\"vorige\">".$previous->meta_data->title."</a>";
			}
			if($next!=null){
				echo "<a href=\"".Tools::get_page_url_by_module("micros|2").$next->meta_data->slug."/\" class=\"volgende\">".$next->meta_data->title."</a>";
			}
			?>
		</div>	
	<?php } ?>	
	<h2><?php echo $item->meta_data->title;?></h2>
	<?php
		$afbeeldingen=array();
		$afbeeldingen=Media::get_all_images("microitems",$item->id,"Detailoverzicht");
		if(count($afbeeldingen)>0){
			echo "<div class=\"afbeeldingen\">";
				foreach($afbeeldingen as $afbeelding){
					echo "<img src=\"".$afbeelding["cropped"]."\" alt=\"".$afbeelding["title"]."\" title=\"".$afbeelding["title"]."\" />";
				}
			echo "</div>";
		}
	?>
	
	<div class="inhoud">
		<?php
		echo Shortcode::build($item->meta_data->content);
		?>
		<div class="auteur">
			<?php
			//Auteur ophalen
			if(isset($item->relation_with_module_item)){ $auteur=$item->relation_with_module_item; } else { $auteur=""; }
			//Afbeeldingen van die auteur ophalen
			$afbeelding=array();
			if($auteur!=""){ $afbeelding=Media::get_all_images("microitems",$auteur["micro_items_id"]); }	
						
			if(count($afbeelding)>0){
				$afbeeldingspad=$afbeelding[0]["cropped"];
			} else {
				//dummy afbeelding
				$afbeeldingspad="/images/dummies/blog.persoon.png";
			}	

			if($auteur!=""){ $auteurnaam=$auteur["title"]; } else { $auteurnaam=tl("blog-anoniem"); }
			?>
			
			<div class="afbeelding"><img src="<?php echo $afbeeldingspad; ?>" alt="<?php echo $auteurnaam ?>" title="<?php echo $auteurnaam ?>"/></div>
			<div class="info">
				<div class="datum"><?php echo Tools::geef_nederlands_datum($item->date_from); ?></div>
				<?php echo $auteurnaam."<br/>";
				if(isset($auteur["custom_fields_json"]->functie)){ echo "<br/>".$auteur["custom_fields_json"]->functie; } ?>
			</div>
			<div class="tags">
				<?php
				//Filters ophalen en tonen
				$array_filters=array();
				$filters=array();
				$filters=Filterpair::where_module_id($item->id)->where_module("microitems")->get();
				$data_filters="";$filters_opsomming="";
				foreach($filters as $filter){
					$data_filters.=$filtermeta[$filter->filter_id]." ";
					$array_filters[]=$filtermeta[$filter->filter_id];
				}
				if(count($array_filters)>0){
					echo "<div class=\"filters\">";
						echo implode(" - ",$array_filters);
					echo "</div>";
				}
				?>
			</div>
		</div>
		<div class="reactiebalk">
			<?php echo tl("blog-reactiebalk-titel"); ?>
		</div>
		<div class="reacties">
			<div class="fb-comments" data-num_posts="5" data-order_by="time" data-href="<?php echo Tools::get_page_url_by_module("micros|2").$item->meta_data->slug."/";?>" data-width="810"></div>
		</div>
	</div>	
</div>

<script type="text/javascript">
	//Reacties op de blog tonen of verbergen
	$(document).ready(function(){

		$("#blogdetail .reactiebalk").click(function(klik){
			klik.preventDefault();
			//Kijken of de reacties al getoond worden (actief als class van de reactiebalk)?
			if($(this).hasClass("actief")){
				$("#blogdetail .reacties").slideUp(100);
				$(this).removeClass("actief");
			} else {
				$("#blogdetail .reacties").slideDown(200);
				$(this).addClass("actief");
			}
		});
	});
</script>