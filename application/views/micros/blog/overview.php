<?php 
	$filters_beschikbaar=array();
	//Ophalen van alle unieke filters die worden gebruikt bij de portfolio items
	foreach($items as $item){		
		$filters=Filterpair::where_module_id($item->id)->where_module("microitems")->get();
		foreach($filters as $filter){		
			$filters_beschikbaar[$filtermeta[$filter->filter_id]]=1;
		}
	}		
	
	
	echo "<div id=\"blogoverzicht\">";	
		//Filterbalk opbouwen
		echo "<div class=\"filterbalk\">";
			//TODO: Link RSS feed
			echo "<a href=\"".Scotty::$module_url."rssfeed/\" class=\"rss_feed\" target=\"_blank\" alt=\"".tl("blog-slogan-rssfeed-link")."\" title=\"".tl("blog-slogan-rssfeed-link")."\"></a>";
			echo "<ul>";
				echo("<li>".tl("blog-categorieen")."</li>");				
				echo("<li class=\"optie alles actief\">".tl("blog-filters-alles")."</li>");
				foreach($filters_beschikbaar as $filter=>$dummy){
					echo("<li class=\"optie\" data-filter=\"".$filter."\">".$filter."</li>");
				}
			echo "<ul>";
		echo "</div>";		

		echo "<div class=\"items\">";
			echo "<div class=\"sliderrail\">";
				echo "<div class=\"slider\">";
					$counter=0;
					foreach($items as $item){
						
						$meta=$item->meta_data;
						$array_filters=array();
						$filters=array();
						$filters=Filterpair::where_module_id($item->id)->where_module("microitems")->get();
						$data_filters="";$filters_opsomming="";
						foreach($filters as $filter){
							$data_filters.=$filtermeta[$filter->filter_id]." ";
							$array_filters[]=$filtermeta[$filter->filter_id];
						}	
						$counter++;
						if($counter%2==0){ $extra_class="even"; } else { $extra_class="oneven"; }
						$datum_url=date("Y/n/j/",strtotime($item->date_from));
						echo "<a href=\"".Scotty::$module_url.$datum_url.$meta->slug."/\" class=\"item ".$extra_class."\" data-filters=\"".$data_filters." ".tl("portfolio-filters-alles")."\">\n";
							echo "<div class=\"afbeelding\"><div class=\"overlay\"></div>";
								$afbeeldingen_array=Media::get_random_image("microitems",$item->id,"Lijstoverzicht");
								if(count($afbeeldingen_array)>0){ echo "<img src=\"".$afbeeldingen_array["cropped"]."\" alt=\"".$meta->title."\" title=\"".$meta->title."\" />"; }
							echo "</div>";
							echo "<div class=\"info\">";
								echo "<h2>".$meta->title."</h2>";
								echo "<div class=\"details\">";
									if(count($array_filters)>0){
										echo "<div class=\"filters\">";
											echo implode(" - ",$array_filters);
										echo "</div>";
									}
									
									//Auteur ophalen
									if(isset($item->relation_with_module_item)){ $auteur=$item->relation_with_module_item; } else { $auteur=""; }
									//Afbeeldingen van die auteur ophalen
									$afbeelding=array();
									if($auteur!=""){ $afbeelding=Media::get_all_images("microitems",$auteur["micro_items_id"]); }
									
									if(count($afbeelding)>0){
										$afbeeldingspad=$afbeelding[0]["cropped"];
									} else {
										//dummy afbeelding
										$afbeeldingspad="/images/dummies/blog.persoon.png";
									}
									echo "<div class=\"dummy\">";
										if($auteur!=""){ 
											echo "<div class=\"blogger\"><img src=\"".$afbeeldingspad."\" alt=\"".$auteur["title"]."\" title=\"".$auteur["title"]."\"/></div>"; 
										} else {
											echo "<div class=\"blogger\"><img src=\"".$afbeeldingspad."\" alt=\"".tl("blog-anoniem")."\" title=\"".tl("blog-anoniem")."\"/></div>"; 
										}
										echo "<div class=\"overige_info\">";
											echo "<div class=\"datum\">".Tools::geef_nederlands_datum($item->date_from)."</div>";
											if($auteur!=""){ 
												echo $auteur["title"];
												if(isset($auteur["custom_fields_json"]->functie)){ echo "<br/>".$auteur["custom_fields_json"]->functie; }
											} else { 
												echo tl("blog-anoniem"); 
											}
										echo "</div>";
									echo "</div>";
								echo "</div>";
							echo "</div>";
						echo "</a>";
					}
				echo "</div>";
			echo "</div>";
			echo "<div class=\"navigatie\"><span class=\"vorige\">".tl("blogoverzicht-vorige-pagina")."</span><div class=\"paginas\"></div><span class=\"volgende\">".tl("blogoverzicht-volgende-pagina")."</span></div>";
		echo "</div>";	
	echo "</div>";	
?>


<script type="text/javascript">	
	$(document).ready(function(){
		
		//Overlay naar onder halen als er over een item wordt gehoverd
		$("#blogoverzicht .items .item").hover(function(){
			$(".afbeelding .overlay",this).stop(true,true).animate({top:"0px"},300,"easeOutQuart");
		},function(){
			$(".afbeelding .overlay",this).stop(true,true).animate({top:"-"+$(".afbeelding",this).height()+"px"},300,"easeOutQuart");
		});	
		
		//Dynamisch maken van de inhoud van alle blogberichten
		var aantal_items=$("#blogoverzicht .items .item").length;
		var aantal_items_per_pagina=4;
		var aantal_paginas=Math.ceil($("#blogoverzicht .items .item").length/aantal_items_per_pagina);
		var items_zelf=$("#blogoverzicht .items .item");
		var huidige_pagina=1;
		var hoogte_enkel_item=$("#blogoverzicht .items .item").outerHeight();
		var hoogte_marge_onderkant=parseInt($("#blogoverzicht .items .item").css("margin-bottom").replace("px",""));
		var hoogte_sliderrail=(hoogte_enkel_item*aantal_items_per_pagina)+(hoogte_marge_onderkant*(aantal_items_per_pagina-1));
		
		instellen_slider();
		
		function instellen_slider(){
			aantal_items=$("#blogoverzicht .items .item").length;
			aantal_paginas=Math.ceil($("#blogoverzicht .items .item").length/aantal_items_per_pagina);
			items_zelf=$("#blogoverzicht .items .item");
			
			//Hoogte slider rail instellen
			hoogte_enkel_item=$("#blogoverzicht .items .item").outerHeight();
			hoogte_marge_onderkant=parseInt($("#blogoverzicht .items .item").css("margin-bottom").replace("px",""));
			hoogte_sliderrail=(hoogte_enkel_item*aantal_items_per_pagina)+(hoogte_marge_onderkant*(aantal_items_per_pagina-1));
			$("#blogoverzicht .items .sliderrail").css("height",hoogte_sliderrail+"px");
			
			//Opdelen van de items over verschillende slides
			for(var i=0;i<aantal_items;i+=aantal_items_per_pagina){
				var opdeling=items_zelf.slice(i,i+aantal_items_per_pagina);  
				opdeling.first().addClass('eerste');
				opdeling.last().addClass('laatste');
				opdeling.wrapAll("<div class=\"slide\"></div>");
			}
			
			//Paginering aanmaken
			for(var i=1;i<=aantal_paginas;i++){
				$("#blogoverzicht .items .navigatie .paginas").append("<span class=\"pagina\">"+i+"</span>");
			}
			//Eerste pagina actief maken
			if(aantal_items>0){ $("#blogoverzicht .items .navigatie .paginas .pagina").first().addClass("actief");  }
			
			//Rest van de navigatie kloppend maken
			if(aantal_paginas>1){
				$("#blogoverzicht .items .navigatie .volgende").css("display","block");
				$("#blogoverzicht .items .navigatie .vorige").css("display","none");			
			}
			
			//Opvangen van de navigatieknoppen
			$("#blogoverzicht .items .navigatie span").click(function(klik){
				klik.preventDefault();
				//Alleen wat ermee doen als de knop niet actief is
				if(!$(this).hasClass("actief")){
					//Kijken of er naar een specifieke pagina moet worden genavigeerd of naar een volgende of vorige pagina
					if($(this).hasClass("pagina")){ beweeg_blogoverzichtslider_naar_slide(parseInt($(this).html())); }
					if($(this).hasClass("vorige")){ beweeg_blogoverzichtslider_naar_slide(huidige_pagina-1);  }
					if($(this).hasClass("volgende")){ beweeg_blogoverzichtslider_naar_slide(huidige_pagina+1);  }
				}			
			});
		}
		
		function beweeg_blogoverzichtslider_naar_slide(slide){
			if((slide>0)&&(slide<=aantal_paginas)){
				huidige_pagina=slide;
				//Bewegen
				$("#blogoverzicht .slider").stop(true,true).animate({marginTop:"-"+(hoogte_sliderrail*(slide-1))+"px"},300,"easeOutQuart");
				//Navigatie resetten
				$("#blogoverzicht .items .navigatie span").removeClass("actief");
				$("#blogoverzicht .items .navigatie .pagina:eq("+(slide-1)+")").addClass("actief");
				if(slide<aantal_paginas){ $("#blogoverzicht .items .navigatie .volgende").css("display","block"); } else { $("#blogoverzicht .items .navigatie .volgende").css("display","none"); }
				if(slide>1){ $("#blogoverzicht .items .navigatie .vorige").css("display","block"); } else { $("#blogoverzicht .items .navigatie .vorige").css("display","none"); }
				//Naar boven scrollen naar begin
				$("html, body").animate({scrollTop:$("#container_website").offset().top},500);
				//Hoogte van de sliderrail aanpassen naar het huidige slide aantal items				
				$("#blogoverzicht .items .sliderrail").css("height",$("#blogoverzicht .slider .slide:eq("+(slide-1)+")").height()+"px");				
			}
		}
		
	});
</script>
	
	
	