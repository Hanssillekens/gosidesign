<?php 
	echo "<div id=\"medewerkers_overzicht\">";
		echo "<div class=\"items\">";
			$counter=0;
			foreach($items as $item){
				echo "<div class=\"item\">\n";
					echo "<div class=\"afbeelding\">";
						$afbeeldingen_array=Media::get_all_images("microitems",$item->id,"none");
						//Afbeelding tonen als die er is (de eerste) ander de default afbeelding tonen
						if(count($afbeeldingen_array)>0){ 
							echo "<img src=\"".$afbeeldingen_array[0]["cropped"]."\" alt=\"".$item->meta_data->title."\" title=\"".$item->meta_data->title."\" />"; 
						} else {
							echo "<img src=\"/images/dummies/medewerkers.persoon.png\" alt=\"".$item->meta_data->title."\" title=\"".$item->meta_data->title."\" />"; 
						}
					echo "</div>";
					echo "<div class=\"informatie\">";
						$extra_gegevens="";
						$extra_gegevens=$item->meta_data->custom_fields_json;	
						echo "<h4>";
							echo $item->meta_data->title;
							echo "<div class=\"social_media\">";
								if(isset($extra_gegevens["facebook_profiel"])){ 
									if($extra_gegevens["facebook_profiel"]!=""){ echo "<a href=\"".$extra_gegevens["facebook_profiel"]."\" class=\"facebook\" target=\"_blank\"></a>"; } 
								}
								if(isset($extra_gegevens["linkedin_profiel"])){
									if($extra_gegevens["linkedin_profiel"]!=""){ echo "<a href=\"".$extra_gegevens["linkedin_profiel"]."\" class=\"linkedin\" target=\"_blank\"></a>"; }
								}
								if(isset($extra_gegevens["twitter_profiel"])){ 
									if($extra_gegevens["twitter_profiel"]!=""){ echo "<a href=\"".$extra_gegevens["twitter_profiel"]."\" class=\"twitter\" target=\"_blank\"></a>"; }
								}
								if(isset($extra_gegevens["google_plus_profiel"])){ 
									if($extra_gegevens["google_plus_profiel"]!=""){ echo "<a href=\"".$extra_gegevens["google_plus_profiel"]."\" class=\"google_plus\" target=\"_blank\"></a>"; }
								}
							echo "</div>";
						echo "</h4>";
						if(isset($extra_gegevens["functie"])){ echo $extra_gegevens["functie"]; }
					echo "</div>";
				echo "</div>";
			}
		echo "</div>";	
	echo "</div>";	
?>