<?php
if(isset($slug)){
?>
	<div id="portfoliodetail">
		<div class="content">
			<?php
			//Invullen van het detailoverzicht van een project als deze is gekozen
		    echo "<div class=\"afbeeldingen\">";
				echo "<div class=\"sliderrail\">";
					echo "<div class=\"slider\">";
						//Alle afbeeldingen hierin zetten, geen afbeelding = 1 dummy
						$afbeeldingen=array();
						$afbeeldingen=Media::get_all_images("microitems",$detailitem->id,"Detail");
						if(count($afbeeldingen)>0){
							foreach($afbeeldingen as $afbeelding){
								echo "<div class=\"slide\">";								
									echo "<img src=\"".$afbeelding["cropped"]."\" alt=\"".$afbeelding["title"]."\" title=\"".$afbeelding["title"]."\" />";
								echo "</div>";
							}
						} else {
							//TODO DUMMY
						}				
					echo "</div>";
				echo "</div>";
				echo "<div class=\"navigatie\">";
				
				echo "</div>";
			echo "</div>";
			echo "<div class=\"informatie\">";
				echo "<div class=\"opties\">";
					if(count($vorige)>0){
						echo "<a class=\"vorige\" href=\"".Scotty::$module_url.$vorige->meta_data->slug."/\" data-href-org=\"".Scotty::$module_url.$vorige->meta_data->slug."/\"></a>";
					}	
					if(count($volgende)>0){
						echo "<a class=\"volgende\" href=\"".Scotty::$module_url.$volgende->meta_data->slug."/\" data-href-org=\"".Scotty::$module_url.$volgende->meta_data->slug."/\"></a>";
					}							
					echo "<div class=\"sluiten\">".tl("portfoliodetail-sluiten")."</div>";
					if($detailitem->meta_data->website!=""){
						$url=$detailitem->meta_data->website;
						if(!strstr($url,"http://")){ $url="http://".$url; }						
						echo "<div class=\"website\"><a href=\"".$url."\" target=\"_blank\">".tl("portfoliodetail-bezoek-website")."</a></div>";
					}
					
				echo "</div>";
				echo "<h2>".$detailitem->meta_data->title."</h2>";
				$extra_gegevens="";
				$extra_gegevens=$detailitem->meta_data->custom_fields_json;
				$filters=Filterpair::where_module_id($detailitem->id)->where_module("microitems")->get();
				$filters_tekst="";
				foreach($filters as $filter){
					if($filters_tekst!=""){ $filters_tekst.=", "; }
					$filters_tekst.=$filtermeta[$filter->filter_id];
				}	
				if((isset($extra_gegevens["klant"]))or($filters_tekst!="")){
					echo "<div class=\"klantentags\">";
						if(isset($extra_gegevens["klant"])){ echo tl("portfolio-klant")." ".$extra_gegevens["klant"]."<br/>"; }	
						if($filters_tekst!=""){ echo $filters_tekst; }
					echo "</div>";
				}
				echo $detailitem->meta_data->introduction;
				echo "<div class=\"delenbalk\">";
					echo "<div class=\"addthis_toolbox\">";						
						echo "<a class=\"addthis_button_facebook\"><img src=\"/images/icoon.delen.facebook.png\" alt=\"".tl("delenbalk-facebook-titel")."\" title=\"".tl("delenbalk-facebook-titel")."\"/></a>";
						echo "<a class=\"addthis_button_twitter\"><img src=\"/images/icoon.delen.twitter.png\" alt=\"".tl("delenbalk-twitter-titel")."\" title=\"".tl("delenbalk-twitter-titel")."\"/></a>";
						echo "<a class=\"addthis_button_linkedin\"><img src=\"/images/icoon.delen.linkedin.png\" alt=\"".tl("delenbalk-linkedin-titel")."\" title=\"".tl("delenbalk-linkedin-titel")."\"/></a>";
						echo "<a class=\"addthis_button_google_plusone_share\"><img src=\"/images/icoon.delen.googleplus.png\" alt=\"".tl("delenbalk-googleplus-titel")."\" title=\"".tl("delenbalk-googleplus-titel")."\"/></a>";
					echo "</div>";
				echo "</div>";
				echo "<a href=\"".Tools::get_page_url_by_template("5")."\" class=\"contactknop\">".tl("portfolio-detail-contactknop-titel")."</a>";
			echo "</div>";			
			?>
		</div>
	</div>
<?php
}
?>
<div id="container_website">
	<div id="content">
		<?php 
		$filters_beschikbaar=array();
		//Ophalen van alle unieke filters die worden gebruikt bij de portfolio items
		foreach($items as $item){		
			$filters=Filterpair::where_module_id($item->id)->where_module("microitems")->get();
			foreach($filters as $filter){		
				$filters_beschikbaar[$filtermeta[$filter->filter_id]]=1;
			}
		}			
		echo "<div id=\"portfoliooverzicht\">";	
			//Filterbalk opbouwen
			echo "<div class=\"filterbalk\">";
				echo "<ul>";
					echo("<li>".tl("portfolio-ons-werk")."</li>");
					echo("<li class=\"optie alles actief\">".tl("portfolio-filters-alles")."</li>");			
					foreach($filters_beschikbaar as $filter=>$dummy){
						echo("<li class=\"optie\" data-filter=\"".$filter."\">".$filter."</li>");
					}			
				echo "<ul>";
			echo "</div>";		

			echo "<div class=\"items\">";
				foreach($items as $item){
					$filters=Filterpair::where_module_id($item->id)->where_module("microitems")->get();
					$data_filters="";
					foreach($filters as $filter){
						$data_filters.=$filtermeta[$filter->filter_id]." ";
					}	
					echo "<a href=\"".Scotty::$module_url.$item->meta_data->slug."/\" data-href-org=\"".Scotty::$module_url.$item->meta_data->slug."/\" class=\"item\" data-filters=\"".$data_filters." ".tl("portfolio-filters-alles")."\">\n";
						//Random afbeelding hierin zetten
						$afbeelding=array();
						$afbeeldingoverzicht="";
						$afbeelding=Media::get_random_image("microitems",$item->id,"Overzicht");
						if(count($afbeelding)>0){								
							$afbeeldingoverzicht="<img src=\"".$afbeelding["cropped"]."\" alt=\"".$afbeelding["title"]."\" title=\"".$afbeelding["title"]."\" />";
						}
						echo "<div class=\"afbeelding\"><div class=\"overlay\"></div>".$afbeeldingoverzicht."</div>";
						echo "<div class=\"info\">";
							echo "<h3>".$item->meta_data->title."</h3>";
							$extra_gegevens="";
							$extra_gegevens=$item->meta_data->custom_fields_json;
							if(isset($extra_gegevens["klant"])){ echo "<span class=\"onderschrift\">".tl("portfolio-klant")." ".$extra_gegevens["klant"]."</span><br/>"; }							
							echo "<span class=\"filters\">".str_replace(", /,"," /",str_replace(" ",", ",trim($data_filters)))."</span>";
						echo "</div>";
					echo "</a>";
				}
			echo "</div>";			
		echo "</div>";	
		?>
    </div>
</div>		


<script type="text/javascript">	
	$(document).ready(function(){	
		//Uitklappen detailview
		if($("#portfoliodetail").length>0){
			$("#portfoliodetail").slideDown(1000);
		}
		
		//Sluiten van de detailview
		$("#portfoliodetail .opties .sluiten").click(function(klik){
			klik.preventDefault();
			$("#portfoliodetail").slideUp(350);
		});
	
		//Hashtag toevoegen aan de links van de items
		if(window.location.hash!=""){
			$("#portfoliooverzicht .item").each(function(){
				$(this).attr("href",$(this).attr("data-href-org")+window.location.hash);
			});
			if($("#portfoliodetail .opties .volgende").length>0){
				$("#portfoliodetail .opties .volgende").attr("href",$("#portfoliodetail .opties .volgende").attr("data-href-org")+window.location.hash);
			}
			if($("#portfoliodetail .opties .vorige").length>0){
				$("#portfoliodetail .opties .vorige").attr("href",$("#portfoliodetail .opties .vorige").attr("data-href-org")+window.location.hash);
			}
		}
		
		//Bij het klikken op een (niet actieve) filter, alles ordenen
		$("#portfoliooverzicht .filterbalk ul li.optie").click(function(){
			if(!$(this).hasClass("actief")){
				//Nieuwe filter actief maken
				$("#portfoliooverzicht .filterbalk ul li.optie").removeClass("actief");
				$(this).addClass("actief");
				actieve_filter=$(this).html();
				//Alle items verbergen die niet aan de filter voldoen
				$("#portfoliooverzicht .items .item").each(function(){
					if($(this).attr("data-filters").indexOf(actieve_filter)>=0){
						$(this).stop(true,true).fadeIn(150);
					} else {
						$(this).stop(true,true).fadeOut(150);
					}					
				});	
				//Hashtag in de adresbalk aanpassen
				location.hash="#"+actieve_filter;
				//Opnieuw instellen links
				$("#portfoliooverzicht .item").each(function(){
					$(this).attr("href",$(this).attr("data-href-org")+window.location.hash);
				});
				if($("#portfoliodetail .opties .volgende").length>0){
					$("#portfoliodetail .opties .volgende").attr("href",$("#portfoliodetail .opties .volgende").attr("data-href-org")+window.location.hash);
				}
				if($("#portfoliodetail .opties .vorige").length>0){
					$("#portfoliodetail .opties .vorige").attr("href",$("#portfoliodetail .opties .vorige").attr("data-href-org")+window.location.hash);
				}
			}
		});
		
		//Initialiseren als er een filter al is gekozen
		if($("#portfoliooverzicht .filterbalk li").length>0){
			preset=window.location.hash;
			preset=preset.replace("#","");
			$("#portfoliooverzicht .filterbalk ul li.optie[data-filter='"+preset+"']").trigger("click");
		}		
		
	});
</script>

<script type="text/javascript">var addthis_config={ui_language:'nl'}</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4ddd3381257e5c61"></script>