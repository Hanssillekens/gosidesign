<?php if( !empty($photos) ): ?>

<div class="row">
    <?php $i=0; foreach( $photos AS $photo ): ?>

        <?php if(0 == $i % 3): ?>
            </div>
            <div class="row">
        <?php endif; ?>

        <div class="four columns">
            <div class="gallery-thumb">
                <a rel="lightbox[gallery]" href="<?php echo $photo['full']; ?>">
                    <img src="<?php echo $photo['path'].'200x200-'.$photo['original']; ?>"/>
                </a>
            </div>
        </div>

    <?php $i++; endforeach; ?>
</div>

<?php else: ?>
    Geen items gevonden!
<?php endif; ?>