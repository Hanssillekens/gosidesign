<?php if( !empty($galleries) ): ?>

    <div class="row">

    <?php $i=0; foreach($galleries AS $gallery ): ?>
        <?php $meta = $gallery->relationships['meta'][0]; ?>

        <?php if(0 == $i % 3): ?>
            </div>
            <div class="row">
        <?php endif; ?>

        <div class="four columns">
            <div class="gallery-thumb">
                <?php $img = $gallery->random_image(); if('' != $img): ?>
                    <a href="<?php echo $module_url.$meta->slug; ?>">
                        <img src="<?php echo $gallery->random_image(); ?>"/>
                    </a>
                <?php endif; ?>
                <span class="post-title"><a href="<?php echo $module_url.$meta->slug; ?>"><?php echo $meta->title; ?></a></span>
            </div>

        </div>

    <?php $i++; endforeach; ?>

    </div>
<?php else: ?>
    Geen items gevonden!
<?php endif; ?>