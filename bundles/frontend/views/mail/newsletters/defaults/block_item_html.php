<div class="content blockpart" data-part_id="<?php echo $id; ?>">
    
    <table>
    	<tr>
			<?php if( $block_data->image_visible == 1): ?>
				
				<?php if( $block_data->image_full_width == 1 ): ?>
					
					<td width="100%" style="vertical-align: top; text-align: center;">
						<?php
							if( $img != '' )
							{
								$src = 'http://' .$project->url .'/uploads/images/600x200-'.$img->src;
								echo HTML::image($src, '', array('class' => 'fullImg'));
							}
						?>
					</td>
				</tr>
				<tr>
				<?php else: ?>

					<td class="small" width="20%" style="vertical-align: top; padding-right:10px;">
						<?php
							if( $img != '' )
							{
								$src = 'http://' .$project->url .'/uploads/images/75x75-'.$img->src;
								echo HTML::image($src, '', array('class' => 'smallImg'));
							}
						?>
					</td>
				
				<?php endif; ?>
			
			<?php else: ?>

				<td class="small" width="20%" style="vertical-align: top; padding-right:10px;"></td>

			<?php endif; ?>
			<td>
				<h4>
					<?php echo ( $block_data->title != '' ) ? $block_data->title : Helper::__('toolbox::base.modules.newsletters.template.no_title'); ?>
				</h4>

				<h4>
					<small><?php echo ( $block_data->subject != '' ) ? $block_data->subject : ''; ?></small>
				</h4>

				<?php echo ( $block_data->content != '' ) ? $block_data->content : '<p>' .Helper::__('toolbox::base.modules.newsletters.template.no_content') .'</p>'; ?>
				
				<?php if( $block_data->readmore_link != '' ): ?> 
                    <?php
                        $url = 'http://' .$block_data->readmore_link;
                        $title = Helper::tl('Lees meer...');
                        echo HTML::link($url, $title, array('target' => '_blank', 'class' => 'btn readmore'))
                    ?>
				<?php endif; ?>
				
			</td>
		</tr>
        
		<?php if( $block_data->show_socialmedia == '1' ): ?>
			<tr>
                <?php if( $block_data->image_full_width != 1 || $block_data->image_visible != 1 ): ?>
				    <td class="small" width="20%" style="vertical-align: top; padding-right:10px;"></td>
                <?php endif; ?>
				<?php
                    $url = 'http://' .$project->url .'/__ONLINEVERSION' .'&amp;rm=' .$block_data->readmore_link;
                ?>
                <td>
                    <?php $fb_url = $url .'&amp;share=fb'; ?>
                    <a href="<?php echo $fb_url; ?>" class="fb_link"><?php echo Helper::tl('Facebook', true, $language); ?></a>
					&nbsp;
                    <span class="pipeline">&#124;</span>
					&nbsp;
					<?php  $tw_url = $url .'&amp;share=tw'; ?>
                    <a href="<?php echo $tw_url; ?>" class="tw_link"><?php echo Helper::tl('Twitter', true, $language); ?></a>
				</td>
			</tr>
		<?php endif; ?>
        
	</table>
</div>