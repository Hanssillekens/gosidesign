<table class="head-wrap">
    <tr>
        <td></td>
        <td class="header container view-online" align="center">
            <br />
            <?php
                $url = "http://" .$project->url ."/__ONLINEVERSION";
                $title = Helper::tl('Kan je deze e-mail niet goed lezen, bekijk dan de online versie.', true, $language);
                echo HTML::link($url, $title); 
            ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td class="header container" align="">
            
            <!-- /content -->
            <div class="content">
                <?php if( $newslettermeta->image_id != 0 ): ?>
                    <table >
                        <tr>
                            <?php if( $newslettermeta->image == null ): ?>
                                <td>
                                    <?php
                                        $image_settings = $data->image_width .'x' .$data->image_height .'-';
                                        $src = 'http://placehold.it/' .$image_settings;
                                        echo HTML::image($src, '', array('id' => 'template_main_img'));
                                    ?>
                                </td>
                            <?php else: ?>
                                <td>
                                    <?php
                                        if( $newslettermeta->image->x == 0 && $newslettermeta->image->y == 0 && $newslettermeta->image->h == 0 && $newslettermeta->image->w == 0 )
                                        {
                                            $image_settings = $data->image_width .'x' .$data->image_height .'-';
                                        }
                                        else
                                        {
                                            $image_settings = $data->image_width .'x' .$data->image_height .'-trim(' .$newslettermeta->image->x .',' .$newslettermeta->image->y .',' .$newslettermeta->image->w .',' .$newslettermeta->image->h .')-';
                                        }
                                        $src = 'http://' .$project->url .'/uploads/images/' .$image_settings .$newslettermeta->image->src;
                                        echo HTML::image($src, '', array('id' => 'template_main_img'));
                                    ?>
                                </td>
                            <?php endif; ?>
                        </tr>
                    </table>
                <?php endif; ?>
            </div><!-- /content -->
            
        </td>
        <td></td>
    </tr>
</table>

<table class="body-wrap" bgcolor="">
    <tr>
        <td></td>
        <td class="container" id="templateHeader" align="" bgcolor="#FFFFFF">
            
            <!-- content -->
            <div class="content">
                <table>
                    <tr>
                        <td>
                            <?php echo ( $newslettermeta->title != '' ) ? '<h1 id="template_title">' .$newslettermeta->title .'</h1>' : ''; ?>
                            <!-- A Real Hero (and a real human being) -->
                            <?php /* <p><img src="http://placehold.it/600x300" /></p><!-- /hero --> */?>
                            <?php echo ( $newslettermeta->content != '' ) ? '<p id="template_intro">' . nl2br($newslettermeta->content) .'</p>' : ''; ?>
                            
                        </td>
                    </tr>
                </table>
            </div><!-- /content -->
        </td>
        <td></td>
    </tr>
</table>