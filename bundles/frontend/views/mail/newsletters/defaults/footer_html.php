<table class="footer-wrap">
    <tr>
        <td></td>
        <td>
            <!-- content -->
    		<div class="content">
				<table bgcolor="">
					<tr>
						<td>
							
							<!-- social & contact -->
							<table bgcolor="" class="social" width="100%">
								<tr>
									<td>
										
										<!--- column 1 -->
										<div class="column">
											<table bgcolor="" cellpadding="" align="left">
										        <tr>
											        <td>				
												
												        <h5 class="">Connect with Us:</h5>
												        <p class="">
                                                            <a href="#" class="soc-btn fb">Facebook</a>
                                                            <a href="#" class="soc-btn tw">Twitter</a>
                                                            <a href="#" class="soc-btn gp">Google+</a>
                                                        </p>
											        </td>
										        </tr>
									        </table><!-- /column 1 -->
										</div>
										
										<!--- column 2 -->
										<div class="column">
											<table bgcolor="" cellpadding="" align="left">
										        <tr>
											        <td>										
												        <h5 class="">Contact Info:</h5>												
												        <p>Phone: <strong>408.341.0600</strong><br/>
                                                        Email: <strong><a href="emailto:hseldon@trantor.com">hseldon@trantor.com</a></strong></p>
                
											        </td>
										        </tr>
									        </table><!-- /column 2 -->	
										</div>
										
										<div class="clear"></div>
	
									</td>
								</tr>
							</table><!-- /social & contact -->
							
						</td>
					</tr>
				</table>
			</div><!-- /content -->
            
        </td>
        <td></td>
    </tr>
    
    <tr>
		<td></td>
		<td class="container">
			<!-- content -->
			<div class="content">
				<table>
					<tr>
						<td align="center">
							<p>
                                <?php
                                    $url = 'http://' .$project->url .'/__ONLINEVERSION&amp;unsubscribe=1';
                                    $title = Helper::tl('uitschrijven', true, $language);
                                    $attr = array('class' => 'unsubscribe_link');
                                    echo HTML::link($url, $title, $attr);
                                ?>
                            </p>
						</td>
					</tr>
				</table>
			</div><!-- /content -->	
		</td>
		<td></td>
	</tr>
</table>