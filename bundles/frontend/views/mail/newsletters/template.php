<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<!-- If you delete this meta tag, the ground will open and swallow you. -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $newslettermeta->subject; ?></title>
	
<style type="text/css">
    <?php echo ( isset($default_css) ) ? $default_css : ''; ?>
</style>

</head>
 
<body bgcolor="#FFFFFF" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<!-- HEADER -->
<?php 
	$data = array(
		'data' => $parent, 
		'newslettermeta' => $newslettermeta,
		'project' => $project,
		'language' => $language
	);
	echo Frontend_NewsletterView_Library::make( json_decode($parent->header_html), $data )->render(); 
?>
<!-- /HEADER -->

<!-- BODY -->

<table class="body-wrap" bgcolor="">
	<tr>
		<td></td>
		<td class="container" id="templateBody" align="" bgcolor="#FFFFFF">
						
			<div id="blocks-holder">
				<!-- content -->
				<?php if( !empty( $blocks ) ): ?>

					<?php foreach( $blocks as $key => $block ): ?>
						<?php
							$img = $img = Medium::find( $block->item_id ); 
							$img = ( is_null($img) ) ? '' : $img;

							$block_data = array(
								'block_data' => $block,
								'project' => $project,
								'id' => $block->id,
								'img' => $img,
								'language' => $language
							);																						
							echo Frontend_NewsletterView_Library::make( json_decode( $parent->block_item_html ), $block_data )->render();																								
						?>
					<?php endforeach; ?>

	        	<?php endif; ?>
	        	<!-- /content -->
			</div>		

		</td>
		<td></td>
	</tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<?php 
	$footer_data= array(
		'newsletter' => $parent,
		'project' => $project,
		'language' => $language
	);
	echo Frontend_NewsletterView_Library::make( json_decode( $parent->footer_html ), $footer_data )->render();																								

?>
<!-- /FOOTER -->

<!-- HIDDEN IMG -->
<?php if( strstr($_SERVER['HTTP_HOST'], 'toolbox.dev') ): ?>
	<img src="http://<?php echo $project->slug; ?>.toolbox.dev/admin/api/newsletter/opened?uid=__UNIQUEID" width="0" height="0"/>
<?php else: ?>
	<img src="http://<?php echo $project->slug; ?>.tool-box.nl/admin/api/newsletter/opened?uid=__UNIQUEID" width="0" height="0"/>
<?php endif; ?>
<!-- /HIDDEN IMG -->

<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!-- Fancybox -->
<link type="text/css" rel="stylesheet" href="//cdn.jsdelivr.net/fancybox/2.1.5/helpers/jquery.fancybox-buttons.css" />
<link type="text/css" rel="stylesheet" href="//cdn.jsdelivr.net/fancybox/2.1.5/helpers/jquery.fancybox-thumbs.css" />
<link type="text/css" rel="stylesheet" href="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.pack.js"></script>

<div id="tw_box">
	<h4>Volg ons op twitter</h4>
	<a href="https://twitter.com/gosidesign" class="twitter-follow-button" data-show-count="false" data-lang="nl" data-size="large">@gosidesign volgen</a>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1&appId=132228080317675";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="fb_box">
	<h4>Like ons op Facebook</h4>
	<div class="fb-like" data-href="" data-send="true" data-width="450" data-show-faces="false"></div>
</div>

<div id="gp_box">
	<!-- Plaats deze tag bovenaan of vlak voor je laatste inhoudstag. -->
	<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
		{lang: <?php echo $language; ?>}
	</script>
	<div class="g-plusone" data-size="tall" data-annotation="inline" data-width="300"></div>
</div>

<script>
	$(document).ready(function()
	{
		// Hide the SocialMedia boxes
		$('#fb_box, #tw_box, #gp_box').css({'position' : 'absolute', 'left' : -99999});

		urlParam = function(name, url)
		{
			var url = ( url == '' ) ? window.location.href : url;
	    	var results = new RegExp('[\\?&amp;]' + name + '=([^&amp;#]*)').exec( url );
	    	return results[1] || 0;
		}

		// Check if the user wants to share the company-credentials
		var share_from_mail = urlParam('share_comp', '');
		if( share_from_mail !== null )
		{
			var share_link = $('.soc-btn.' +share_from_mail);
			var share_url  = $(share_link).data('url');

			switch( share_from_mail )
			{
				case 'fb':
			  		doFacebook( share_url );
			  	break;
				case 'tw':
					doTwitter( share_url );
			  	break;
			  	case 'gp':
					doGoogle( share_url );
			  	break;
				default:
			}
		};

		// Like on FB
		function doFacebook( share_url )
		{
			var content = $('#fb_box').html();
			content = content.replace('data-href=""', 'data-href="' +share_url +'"');
			show_fancybox( content );
		}

		// Follow on Twitter
		function doTwitter( share_url )
		{
			var content = $('#tw_box').html();
			content = content.replace('data-url=""', 'data-url="' +share_url +'"');
			show_fancybox( content );
		}

		// Share via Google+
		function doGoogle( share_url )
		{
			// var content = $('#gp_box').html();
			// content = content.replace('data-url=""', 'data-url="' +share_url +'"');
			// show_fancybox( content );
		}

		// Show a popup with the social media options
		function show_fancybox( content )
		{
			$.fancybox({
				'minWidth'   : 600,
				'minHeight'  : 80,
				'content' : content,
			});
		}
	});
</script>

</body>
</html>