<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xml:lang="<?php echo Config::get('frontend::runtime.language.default'); ?>" prefix="og: http://ogp.me/ns#">
<head>
    <?php echo $header; ?>

    <script src="<?php echo URL::to_asset('bundles/frontend/js/lightbox.js'); ?>"></script>
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo URL::to_asset('bundles/frontend/css/lightbox.css'); ?>">
</head>

<body>
<nav role="navigation" id="topMenu" style="display: none;" >
    <?php echo Beam::menu("nav-bar", "nav"); ?>
</nav>


<header class="row">
    <div class="row">
        <div class="one column show-for-small">
            <ul class="mobile-menu" >
                <li class="left"><a href="#menu" id="menuButton"><span><img class="menu-toggle" src="<?php echo URL::to_asset('bundles/frontend/img/menu_expand.png'); ?>"/></span></a></li>
            </ul>
        </div>
        <div class="three columns logo-box">
            <h1>
                <a href="/<?php echo Config::get('frontend::runtime.language.default'); ?>/"><img class="logo" src="<?php echo URL::to_asset('bundles/frontend/img/gosidesign.png'); ?>"/></a>
            </h1>
        </div>

        <div class="nine columns">

            <!-- Contact Info -->
            <div class="contact-info hide-for-small">
                <ul>
                    <li><span></span></li>
                    <li><span></span></li>
                </ul>
            </div>

            <!-- Page Links -->
            <nav>
                <?php echo Beam::menu("navigation right hide-for-small", "mainNav"); ?>
            </nav>

        </div>

        <div class="twelve columns">
            <div class="bottom-border"></div>
        </div>
</header>

<div class="row wrapper">
    <div class="twelve columns " role="content">
        <?php echo (isset($content) ? $content : '' ); ?>

        <?php echo (isset(Beam::$module_content) ? Beam::$module_content: '' ); ?>
    </div>
</div>

<?php echo (isset($footer) ? $footer : '' ); ?>

<?php echo $base_js; ?>
</body>
</html>