<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xml:lang="<?php echo Config::get('frontend::runtime.language.default'); ?>" prefix="og: http://ogp.me/ns#">
<head>
    <?php echo $head; ?>
</head>

<body>
<?php echo $header; ?>

<div class="row wrapper">
    <div class="eight columns " role="content">
        <?php echo (isset($content) ? $content : '' ); ?>

        <?php echo (isset(Beam::$module_content) ? Beam::$module_content: '' ); ?>
    </div>

    <aside class="four columns" role="content">
        <ul class="widget-sidebar">

            <?php if(null != Beam::$menu_sub): ?>
                <li class="widget widget_categories">
                    <h5 class="widget-title"><?php echo Beam::sub_menu_title(); ?></h5>
                    <div class="textwidget">
                        <?php echo Beam::$menu_sub; ?>
                    </div>
                </li>
            <?php endif; ?>

            <?php if(0 < count(Beam::$blocks) && 0 < count(Beam::$blocks['right'])): ?>
                <?php foreach(Beam::$blocks['right'] AS $block): ?>
                    <li class="widget">
                        <?php if( 1 == $block->title_visible ): ?>
                            <h5 class="widget-title"><?php echo $block->title; ?></h5>
                        <?php endif; ?>
                        <div class="textwidget">
                            <?php echo Shortcode::build( $block->content ); ?>
                        </div>
                    </li>
                <?php endforeach; ?>

            <?php endif; ?>
        </ul>
    </aside>
</div>

<?php echo (isset($footer) ? $footer : '' ); ?>

<?php echo $base_js; ?>
</body>
</html>