<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xml:lang="<?php echo Config::get('frontend::runtime.language.default'); ?>" prefix="og: http://ogp.me/ns#">
    <head>
        <?php echo $head; ?>
    </head>

    <body>
        <?php echo $header; ?>

        <div class="row wrapper">
            <div class="twelve columns " role="content">
                <?php echo (isset($content) ? $content : '' ); ?>

                <?php echo (isset(Beam::$module_content) ? Beam::$module_content: '' ); ?>
            </div>
        </div>

        <?php echo (isset($footer) ? $footer : '' ); ?>

        <?php echo $base_js; ?>
    </body>
</html>