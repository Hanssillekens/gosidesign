<?php if( !empty($articles) ): ?>

    <?php foreach($articles AS $article ): ?>
        <?php if( isset($article->relationships['meta'][0]) ): ?>
            <?php $meta = $article->relationships['meta'][0]; ?>

            <article class="post">
                <h1 class="post-title"><a href="<?php echo $module_url.Newsitem::path( $meta->news_id ); ?>"><?php echo $meta->title; ?></a></h1>

                <div class="entry post-content">
                    <?php echo $meta->introduction; ?>
                </div>
            </article>
        <?php endif; ?>
        
    <?php endforeach; ?>
<?php else: ?>
    <article class="post">
        <div class="entry post-content">
            Geen nieuws berichten gevonden.
        </div>
    </article>
<?php endif; ?>