<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xml:lang="<?php echo Config::get('frontend::runtime.language.default'); ?>" prefix="og: http://ogp.me/ns#">
<head>
    <title><?php echo $settings['site_title'] .' - '. $page->name .' '.$title_extended; ?></title>

    <?php foreach($og AS $og_row): ?>
    <?php echo $og_row; ?>
    <?php endforeach; ?>


    <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="<?php echo URL::to_asset('bundles/frontend/js/jquery.backstretch.js'); ?>"></script>

    <?php echo Asset::container('frontend')->styles();?>
    <?php echo Asset::container('frontend')->scripts();?>
    <link rel="stylesheet" href="<?php echo URL::to_asset('bundles/frontend/css/print.css'); ?>" type="text/css" media="print" />
</head>

<body>
    <div class="headerbg"></div>

    <img class="doprint printlogo" src="<?php echo URL::to_asset('bundles/frontend/img/logo.png'); ?>"/>

    <div class="wrapper">
        <nav>
            <a href="/<?php echo Config::get('frontend::runtime.language.default'); ?>/"><img class="logo" src="<?php echo URL::to_asset('bundles/frontend/img/logo.png'); ?>"/></a>
            <?php echo $menu; ?>
        </nav>

        <header class="header">
            <hgroup> </hgroup>
            <div class="overlaycontainer">
                <div class="overlay" style="display:none;"></div>
            </div>
        </header>

        <aside class="sidebar">
            <ul class="widget-sidebar">

                <?php if('' != $menu_sub): ?>
                    <li class="widget widget_categories">
                        <h3 class="widget-title">Submenu</h3>
                        <?php echo $menu_sub; ?>
                    </li>
                <?php endif; ?>

                <?php if(0 < count($blocks) && 0 < count($blocks['right'])): ?>
                    <?php foreach($blocks['right'] AS $block): ?>
                        <li class="widget">
                            <?php if( 1 == $block->title_visible ): ?>
                                <h3 class="widget-title"><?php echo $block->title; ?></h3>
                            <?php endif; ?>
                            <div class="textwidget">
                                <?php echo Shortcode::build( $block->content ); ?>
                            </div>
                        </li>
                    <?php endforeach; ?>

                <?php endif; ?>
            </ul>
        </aside>

        <section class="content">
            <?php echo $module_content; ?>
        </section>



        <footer>
            <div class="footer-left">
                <?php echo $settings['site_copy']; ?>
            </div>
        </footer>
    </div>

    <?php echo $base_js; ?>

    </body>
</html>