<div class="routewidget">

    <?php if('true' == $use_google_maps): ?>
        <div id="googlemap" style="width:100%; height:400px;"></div>
    <?php endif; ?>
    <div class="adres">
        <label><?php echo "Street + Number"; ?></label><input type="text" id="maps-adres" />
        <label><?php echo "City"; ?></label><input type="text" id="maps-plaats" />
        <a href="#getdirections" class="knop"><?php echo "plan route"; ?></a>
    </div>
    <div id="directions"></div>
</div>

<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
<script>
    $('a[href="#getdirections"]').click(function(e) {
        e.preventDefault();
        var adres = $('#maps-adres').val();
        var plaats = $('#maps-plaats').val();
        if(adres != '' && plaats != ''){
            getDirections(adres+', '+ plaats);
        } else {
            alert('Please fill in all the fields');
        }
    });
    $('#maps-plaats').keyup(function(e){
        if(e.keyCode == 13) {
            $('a[href="#getdirections"]').click();
        }
    });

    var directionDisplay;
    var map;
    function doGoogleMaps() {
        directionsDisplay = new google.maps.DirectionsRenderer({draggable:true, suppressMarkers:true});
        var geocoder = new google.maps.Geocoder();
        if(geocoder) {
            geocoder.geocode({'address':'<?php echo str_replace( Shortcode::$spaceDelimiter, ' ', $street).', '.$city; ?>'}, function(results, status) {
                if(status == google.maps.GeocoderStatus.OK) {
                    var gLat = results[0].geometry.location.lat();
                    var gLng = results[0].geometry.location.lng();
                }
                var latlng = new google.maps.LatLng(gLat, gLng);
                var myOptions = {zoom:14, center:latlng, mapTypeId:google.maps.MapTypeId.ROADMAP};
                map = new google.maps.Map(document.getElementById('googlemap'), myOptions);
                new google.maps.Marker({position:latlng, map:map});
                directionsDisplay.setMap(map);
                directionsDisplay.setPanel(document.getElementById("directions"));
            });
        }
    }

    function getDirections(address){
        var directionsService = new google.maps.DirectionsService();
        directionsService.route({origin:address, destination:'<?php echo str_replace( Shortcode::$spaceDelimiter, ' ', $street).', '.$city; ?>', provideRouteAlternatives:false, travelMode:google.maps.DirectionsTravelMode.DRIVING, unitSystem:google.maps.DirectionsUnitSystem.METRIC}, function(result, status){
            if(status == google.maps.DirectionsStatus.OK){
                directionsDisplay.setDirections(result);
                var geocoder = new google.maps.Geocoder();
                if(geocoder){
                    geocoder.geocode({'address':address}, function(results, status){
                        if(status == google.maps.GeocoderStatus.OK){
                            var gLat = results[0].geometry.location.lat();
                            var gLng = results[0].geometry.location.lng();
                        }
                        var latlng2 = new google.maps.LatLng(gLat, gLng);
                        new google.maps.Marker({position:latlng2, map:map});
                    });
                }
            } else {
                alert('Your adress "' + address + '" can\'t be found. Please check your adress for spelling mistakes.');
            }
        });
    }

    <?php if('true' == $use_google_maps): ?>
        doGoogleMaps();
    <?php endif; ?>
</script>