<?php if( !empty($categories) ): ?>
    <article class="post faqs-list">
    <?php foreach($categories AS $category ):  ?>
        <h2><?php echo $category->title; ?></h2>

        <?php if(!empty($faqs[$category->category_id])): ?>
            <?php foreach($faqs[$category->category_id] AS $faq ): ?>
                <h3 class="faqs-title">
                    <?php echo strip_tags($faq->question); ?>
                    <div class="bottom-border"></div>
                </h3>

                <div class="faqs-answer" style="display:none;">
                    <?php echo $faq->answer; ?>
                    <div class="bottom-border"></div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php endforeach; ?>
    </article>
<?php elseif(!empty($faqs[0])): ?>
    <article class="post faqs-list">
        <?php foreach($faqs[0] AS $faq ): ?>
            <h3 class="faqs-title">
                <?php echo strip_tags($faq->question); ?>
                <div class="bottom-border"></div>
            </h3>

            <div class="faqs-answer" style="display:none;">
                <?php echo $faq->answer; ?>
                <div class="bottom-border"></div>
            </div>
        <?php endforeach; ?>
    </article>
<?php else: ?>
    <article class="post">
        <div class="entry post-content">
            <?php echo tl('No FAQ available'); ?>
        </div>
    </article>
<?php endif; ?>

<script>
    $('.faqs-title').on('click', function(){
        $('.faqs-answer').hide();
        $(this).next().show();
    });
</script>