<article class="post">
    <h1 class="post-title"><?php echo $form->page_title; ?></h1>

    <div class="entry post-content">
        <?php echo $form->content; ?>
    </div>
</article>