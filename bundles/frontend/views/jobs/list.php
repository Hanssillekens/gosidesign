<?php if( !empty($jobs) ): ?>
    <article class="post jobs-list">
        <ul>


        <?php foreach($jobs AS $job ): ?>
            <?php if(!empty($job->relationships['meta'])): $meta = $job->relationships['meta'][0]; ?>
                <li><a href="<?php echo Scotty::$module_url.$meta->slug; ?>" title="<?php echo $meta->title; ?>"><?php echo $meta->title; ?></a></li>

            <?php endif; ?>
        <?php endforeach; ?>
        </ul>
    </article>


<?php else: ?>
    <article class="post">
        <div class="entry post-content">
            <?php echo tl('No vacancies available.'); ?>
        </div>
    </article>
<?php endif; ?>