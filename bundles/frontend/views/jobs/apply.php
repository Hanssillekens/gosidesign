<article class="post">
    <h1 class="post-title"><?php echo $job->title; ?></h1>

    <div class="entry jobs-meta">
        <div class="apply-now">

        </div>

        <div class="social">
            <a class="addthis_button_compact" href="#">
                <img width="89" height="21" border="0" alt="Share" src="/bundles/frontend/img/share.png">
            </a>
            <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5174f5fd6e5902a8" type="text/javascript" tabindex="1000"></script>
            <span class="print-button" ><a class="noprint" href="javascript:if(window.print)window.print()"><img src="<?php echo URL::to_asset('bundles/frontend/img/grayprint.png'); ?>"/></a></span>
        </div>
    </div>

    <div class="entry jobs-content">
        <?php echo $applyform; ?>
    </div>
</article>