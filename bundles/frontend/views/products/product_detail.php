<div class="row">
    <div class="four columns"><img src="http://placehold.it/161x194"/></div>
    <div class="four columns"><img src="http://placehold.it/161x194"/></div>
    <div class="four columns">
        <div>subname : <?php echo isset($product->attr['product subname']) ? $product->attr['product subname'] : '-'; ?></div>
        <div>formula : <?php echo isset($product->attr['formula']) ? $product->attr['formula'] : '-'; ?></div>
    </div>
</div>

<div class="row"><br/></div>

<div class="row">
    <?php echo isset($product->attr['commercial text']) ? $product->attr['commercial text'] : '-'; ?>
</div>

<div class="row"><br/></div>

<div class="row">
    <div class="product-tabs">
        <div data-target="product-benefits">benefits</div>
        <div data-target="product-application">application</div>
        <div data-target="product-longevity">longevity</div>
        <div data-target="product-downloads">downloads</div>
    </div>

    <div class="product-contents">
        <div id="product-benefits">
            <ul>
                <li><?php echo isset($product->attr['benefit 1']) ? $product->attr['benefit 1'] : '-'; ?></li>
                <li><?php echo isset($product->attr['benefit 2']) ? $product->attr['benefit 2'] : '-'; ?></li>
                <li><?php echo isset($product->attr['benefit 3']) ? $product->attr['benefit 3'] : '-'; ?></li>
            </ul>
        </div>

        <div id="product-application" style="display:none;">
            <?php echo isset($product->attr['Application']) ? $product->attr['Application'] : '-'; ?>
            <ul>
                <li>Application recommended period <?php echo isset($product->attr['Application recommended period'])   ? $product->attr['Application recommended period']  : '-'; ?></li>
                <li>Average recommended rate <?php echo isset($product->attr['Average recommended rate'])               ? $product->attr['Average recommended rate']        : '-'; ?></li>
                <li>Application method <?php echo isset($product->attr['Application method'])                           ? $product->attr['Application method']              : '-'; ?></li>
                <li>Average response <?php echo isset($product->attr['Average response'])                               ? $product->attr['Average response']                : '-'; ?></li>
            </ul>
        </div>

        <div id="product-longevity" style="display:none;">
            <ul>
                <li>longevity table: months at 15C <?php echo isset($product->attr['longevity table: months at 15C']) ? $product->attr['longevity table: months at 15C'] : '-'; ?></li>
                <li>longevity table: months at 21C <?php echo isset($product->attr['longevity table: months at 21C']) ? $product->attr['longevity table: months at 21C'] : '-'; ?></li>
                <li>longevity table: months at 27C <?php echo isset($product->attr['longevity table: months at 27C']) ? $product->attr['longevity table: months at 27C'] : '-'; ?></li>
            </ul>
        </div>

        <div id="product-downloads" style="display:none;">

        </div>
    </div>
</div>


<script>
    $('.product-tabs > div').click(function(){
        var self = this;

        $('.product-contents > div').hide();
        $('.product-contents > #'+ $(self).data('target') ).show();
    });
</script>