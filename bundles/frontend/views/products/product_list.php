<?php if(!empty($products) ): ?>
    <div class="row">
        <ul>
        <?php foreach($products AS $product ): ?>
            <li>
                <a href="<?php echo $module_url.$slug.$product->slug; ?>"><?php echo $product->title; ?></a>
            </li>
        <?php endforeach; ?>
        </ul>
    </div>
<?php else: ?>
    <div><?php echo 'Geen producten beschikbaar'; ?></div>
<?php endif; ?>