<?php if(!empty($categories) ): ?>
    <div class="row">

    <?php $i=0; foreach($categories AS $category ): ?>
    <?php if(0 == $i % 3): ?>
    </div>
    <div class="row">
    <?php endif; ?>
        <div class="four columns">
            <div class="product-category-thumb">
                <?php $img = $category->image; ?>
                <?php if('' != $img): ?>
                    <a href="<?php echo $module_url.$parent_slug.$category->slug; ?>">
                        <img src="<?php echo $category->image; ?>"/>
                    </a>
                <?php else: ?>
                    <a href="<?php echo $module_url.$parent_slug.$category->slug; ?>">
                        <img width="175" height="122" src="http://placehold.it/175x122"/>
                    </a>
                <?php endif; ?>
                <span class="post-title"><a href="<?php echo $module_url.$parent_slug.$category->slug; ?>"><?php echo $category->title; ?></a></span>
            </div>
        </div>
        <?php $i++; endforeach; ?>
    </div>

    <div class="row product-category-content">
        <?php echo $category->content; ?>
    </div>
<?php else: ?>
    <div><?php echo 'Geen categorieën beschikbaar'; ?></div>
<?php endif; ?>