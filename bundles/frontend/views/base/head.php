    <title><?php echo Beam::page_title(); ?></title>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="<?php echo URL::to_asset('bundles/frontend/js/jquery.backstretch.js'); ?>"></script>

    <?php foreach(Beam::$og AS $og_row): ?>
        <?php echo $og_row; ?>
    <?php endforeach; ?>

    <?php echo Asset::container('frontend')->styles();?>
    <?php echo Asset::container('frontend')->scripts();?>