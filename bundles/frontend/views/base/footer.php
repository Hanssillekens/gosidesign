<footer class="row">
    <div class="twelve columns">
        <div class="bottom-border"></div>
    </div>
    <div class="twelve columns">
        <div class="row">
            <div class="eight columns hide-for-small">
                <?php echo Beam::menu("link-list left"); ?>
            </div>
            <div class="four columns">
                <p class="copyright"><?php echo Scotty::$settings['site_copy']; ?></p>
            </div>
        </div>
    </div>
</footer>