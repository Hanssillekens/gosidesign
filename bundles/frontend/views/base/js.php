<script>
    $(function(){
        $(window).resize(function() {
            menu_monitor();
        });

        $('#menuButton').on('click', function(){
            toggle_menu();
        });

        menu_monitor();
    });

    var menu_monitor = function (){
        var width = $(window).width();
        var menu  = $('#topMenu');

        if (menu.length > 0){
            if( width < 740 ){
                menu.css("margin-top", 0 );
            }else{
                menu.css("margin-top", menu.height() * -1).stop();
            }
        }
    }

    var toggle_menu = function(){
        var menu  = $('#topMenu');
        menu.stop().slideToggle();
    }

    <?php  $register = array(); ?>
    $(".header2").backstretch([
        <?php
            $i=1;
            if(0 < count(Beam::$headers))
            {
                foreach(Beam::$headers AS $img )
                {
                    echo (1 != $i ? ',' : '');
                    echo '"'.$img['cropped'].'"';
                    $i++;
                }
            }else
            {
                echo '"'.URL::to_asset('bundles/frontend/img/default-header.png').'"';
            }
        ?>
    ], {duration: 3000, fade: 250});


    $.backstretch([
        "<?php echo URL::to_asset('bundles/frontend/img/default-background.png'); ?>"
        <?php $i=1; foreach(Beam::$backgrounds AS $img ): ?>
        ,"<?php echo $img['cropped']; ?>"
        <?php $i++; endforeach; ?>
    ], {duration: 2000, fade: 750});
</script>