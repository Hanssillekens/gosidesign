<!-- Navigation for mobile, hidden by default -->
<nav role="navigation" id="topMenu" style="display: none;" >
    <?php echo Beam::menu("nav-bar", "nav"); ?>
</nav>

<header class="row">
    <div class="row">
        <div class="one column show-for-small">
            <ul class="mobile-menu">
                <li class="left"><a href="#menu" id="menuButton"><span><img class="menu-toggle" src="<?php echo URL::to_asset('bundles/frontend/img/menu_expand.png'); ?>"/></span></a></li>
            </ul>
        </div>
        <div class="three columns logo-box">
            <h1>
                <a href="/<?php echo Config::get('frontend::runtime.language.default'); ?>/"><img class="logo" src="<?php echo URL::to_asset('bundles/frontend/img/gosidesign.png'); ?>"/></a>
            </h1>
        </div>

        <div class="nine columns">

            <!-- Contact Info -->
            <div class="contact-info hide-for-small">
                <?php echo Shortcode::build('[search]'); ?>
                <?php echo Beam::language_menu('language-menu', true); ?>
            </div>

            <!-- Page Links -->
            <nav>
                <?php echo Beam::menu("navigation right hide-for-small", "mainNav"); ?>
            </nav>

        </div>
</header>