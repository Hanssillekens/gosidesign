<div id="my-form-builder"></div>


<?php
	$form = Formitem::find($id);
?>
<div id="formExample" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo $form->title; ?></h3>
    </div>
    <div class="modal-content"></div>
    <div class="modal-footer">
        <button type="button" class="btn-primary btn" data-dismiss="modal" aria-hidden="true"><?php echo Helper::__('toolbox::base.modules.forms.close') ?></button>
    </div>
</div>

<script>
	$(function(){
		$('#my-form-builder').formbuilder({
			'save_url': '/admin/forms/saveJson/<?php echo $id .'/' .$language; ?>', // save the form
			'load_url': '/admin/forms/loadJson/<?php echo $id .'/' .$language; ?>', // load the form
			'check_url': '/admin/forms/checkJson/<?php echo $id .'/' .$language; ?>', // render the form
			'useJson' : false
		});
		$(function() {
			$("#my-form-builder > ul").sortable({ 
				opacity: 0.6, 
				cursor: 'move',
				placeholder: "ui-state-highlight"
			});
		});
	});

	$('#submitFormDemo').live('click', function(e) {
		if($("#formExample form")[0].checkValidity() == true)
		{
			e.preventDefault();
		}
	});

</script>