<?php

class NewsletterItem extends Eloquent {

    public static $table = 'newsletters';

    public function meta()
    {
        return $this->has_many('Newslettermeta', 'newsletter_id');
    }
}