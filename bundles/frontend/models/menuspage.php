<?php

class Menuspage extends Eloquent {

    public static $key   = 'id';
    public static $table = 'menus_pages';

    public function newsitem()
    {
        return $this->belongs_to('Menus');
    }
}