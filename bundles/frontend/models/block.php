<?php

class Block extends Eloquent {

    public static $key   = 'id';
    public static $table = 'blocks';

    public function page(){
        return $this->belongs_to('Page', 'page_id');
    }

    public function meta(){
        return $this->has_many('Blockmeta', 'block_id');
    }
}