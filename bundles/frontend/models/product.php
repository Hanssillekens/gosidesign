<?php

class Product extends Eloquent {

    public static $table = 'products';
    public static $id = 'id';

    public function meta()
    {
        return $this->has_many('Productmeta', 'product_id');
    }

    public function manufacture()
    {
        return $this->belongs_to('Manufacture', 'manufacture_id');
    }

    public static $accessible = array(
        'manufacture_id',
    );

    public static function get_default_fields()
    {
        $default_fields = array(
            'manufacture_id'
        );

        return $default_fields;
    }


    public static function products_by_category( $category_id, $language_id = '' )
    {
        $language = ('' != $language_id ? $language_id : Scotty::get_language() );

        // get product ID's in the category
        $product_ids  = Categorymodule::select('item_id')->where_module_id('products')->where_category_id($category_id)->lists('item_id');

        $products = array();

        foreach( $product_ids AS $product )
        {
            $products[] = self::product_detail_by_id( $product, $language);
        }

        return $products;
    }


    public static function products_list_by_category( $category_id, $language_id = '' )
    {
        $language = ('' != $language_id ? $language_id : Scotty::get_language() );

        // get product ID's in the category
        $product_ids  = Categorymodule::where_module_id('products')->where_category_id($category_id)->lists('item_id');

        $products = array();

        foreach( $product_ids AS $product )
        {
            $products[] = Productmeta::where_product_id( $product )->where_language_id($language)->first();
        }

        return $products;
    }


    public static function product_detail( $slug, $language_id = '' )
    {
        $language = ('' != $language_id ? $language_id : Scotty::get_language() );

        $product = Productmeta::where_slug( $slug )->where_language_id($language)->first();

        if(null != $product)
        {
            // get product attributes
            $sql = 'SELECT a.fieldname, pa.value
                    FROM   attributes         AS a
                    JOIN   product_attributes AS pa ON ( pa.attribute_id = a.id )
                    WHERE  pa.product_id = ?
                    AND    pa.language_id = ?  ';
            $attributes = Db::query( $sql, array($product->product_id, $language) );

            $attr = array();
            foreach( $attributes AS $attribute )
            {
                $attr[$attribute->fieldname] = $attribute->value;
            }

            // get product image
            $product->image = Media::get_first_image('products', $product->product_id );
            $product->attr  = $attr;
        }

        return $product;
    }



    public static function product_detail_by_id( $id, $language_id = '' )
    {
        $language = ('' != $language_id ? $language_id : Scotty::get_language() );

        $product = Productmeta::where_product_id( $id )->where_language_id($language)->first();

        if(null != $product)
        {
            // get product attributes
            $sql = 'SELECT a.fieldname, pa.value
                    FROM   attributes         AS a
                    JOIN   product_attributes AS pa ON ( pa.attribute_id = a.id )
                    WHERE  pa.product_id = ?
                    AND    pa.language_id = ?  ';
            $attributes = Db::query( $sql, array($product->product_id, $language) );

            $attr = array();
            foreach( $attributes AS $attribute )
            {
                $attr[$attribute->fieldname] = $attribute->value;
            }

            // get product image
            $product->image = Media::get_first_image('products', $product->product_id );
            $product->attr  = $attr;
        }

        return $product;
    }


    public static function path( $id, $language = '' )
    {
        $language = ('' == $language ? Scotty::get_language() : $language );
        $path = '';

        // get category for the product
        $productmeta = Productmeta::where_product_id( $id )->where_language_id($language)->first();

        $category = Categorymodule::select('category_id')->where_module_id('products')->where_item_id( $id )->first();


        $catmeta = Categorymeta::where_category_id( $category->category_id )->where_language_id($language)->first();

        $slug = '';
        if(null != $catmeta )
        {
            $cat = $catmeta->category;
            if( null != $cat )
            {
                $catmeta_head = Categorymeta::where_category_id( $cat->category_id )->where_language_id($language)->first();
                if(null != $catmeta_head)
                {
                    $slug.= $catmeta_head->slug.'/';
                }

                $slug.= $catmeta->slug.'/';
            }
        }

        return Tools::get_page_url_by_module('products').$slug.$productmeta->slug;
    }

}