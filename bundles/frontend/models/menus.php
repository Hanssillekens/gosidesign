<?php

class Menus extends Eloquent {

    public static $key   = 'id';
    public static $table = 'menus';

    public function meta()
    {
        return $this->has_many('Menuspage', 'menu_id');
    }
}