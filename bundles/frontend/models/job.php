<?php 

class Job extends Eloquent {
    
    public static $table = 'jobs';
    public static $id = 'id';

    public function meta()
    {
        return $this->has_many('Jobmeta', 'job_id');
    }

}