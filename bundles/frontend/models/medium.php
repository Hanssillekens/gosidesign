<?php

class Medium extends Eloquent {
    
    public static $table = 'media';
    public static $id = 'id';

    
    public static $timestamps = false;


    public function meta(){
        return $this->has_many('Mediummeta', 'media_id');
    }

}