<?php

class Microitemmeta extends Eloquent {

    public static $key          = 'id';
    public static $table        = 'micro_items_meta';

    public function microitem()
    {
        return $this->belongs_to('Microitem', 'micro_items_id');
    }
}