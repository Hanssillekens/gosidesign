<?php

class Gallerymeta extends Eloquent {

    public static $key   = 'id';
    public static $table = 'galleries_meta';
    public static $timestamps = false;

    public function gallery(){
        return $this->belongs_to('Galleries', 'id');
    }
}