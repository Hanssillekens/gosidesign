<?php 

class Language extends Eloquent {
    
    public static $table = 'languages';
    public static $id = 'id';

    
    public static $timestamps = false;

}