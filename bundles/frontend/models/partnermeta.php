<?php

class Partnermeta extends Eloquent {

    public static $key   = 'id';
    public static $table = 'partners_meta';
    public static $timestamps = false;

    public function partner(){
        return $this->belongs_to('Partner', 'id');
    }
}