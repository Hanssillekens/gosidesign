<?php 

class Faq extends Eloquent {
    
    public static $table = 'faqs';
    public static $id = 'id';

    public function meta()
    {
        return $this->has_many('Faqmeta', 'faq_id');
    }

    public function category()
    {
        return $this->belongs_to('Category', 'category_id');
    }
}