<?php

class Filtermeta extends Eloquent {

    public static $key        = 'id';
    public static $table      = 'filter_meta';

    public function filter(){
        return $this->belongs_to( 'Filteritem', 'filter_id' );
    }

}