<?php 

class NewsletterProductCategory extends Eloquent {
    
    public static $table = 'newsletters_products_categories';

    public static $timestamps = false;
}