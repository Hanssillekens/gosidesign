<?php 

class Partner extends Eloquent {
    
    public static $table = 'partners';
    public static $id = 'id';

    public function meta(){
        return $this->has_many('Partnermeta', 'partner_id');
    }

}