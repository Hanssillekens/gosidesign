<?php

class Formmeta extends Eloquent {

    public static $key   = 'id';
    public static $table = 'forms_meta';
    public static $timestamps = false;

    public function block(){
        return $this->belongs_to('Form', 'form_id');
    }
}