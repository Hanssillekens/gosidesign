<?php

class Faqmeta extends Eloquent {

    public static $key   = 'id';
    public static $table = 'faqs_meta';
    public static $timestamps = false;

    public function faq()
    {
        return $this->belongs_to('Faq');
    }
}