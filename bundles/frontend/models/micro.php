<?php

class micro extends Eloquent {

    public static $key   = 'id';
    public static $table = 'micros';

    public function meta()
    {
        return $this->has_many('Microsmeta', 'micro_id');
    }
}