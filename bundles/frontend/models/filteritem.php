<?php

class Filteritem extends Eloquent {

    public static $table      = 'filter';
    public static $id         = 'id';


    public function meta(){
        return $this->has_many('Filtermeta', 'filter_id');
    }
}