<?php

class Newslettercategoriesusers extends Eloquent{

    public static $table = 'newsletter_categories_users';

    public function category()
    {
        return $this->belongs_to('Category');
    }
}