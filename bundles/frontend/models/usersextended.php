<?php

class Usersextended extends Eloquent{

    public static $key   = 'user_id';
    public static $table = 'users_extended';
    public static $timestamps = false;

    public function user()
    {
        return $this->belongs_to('User', 'user_id');
    }
}