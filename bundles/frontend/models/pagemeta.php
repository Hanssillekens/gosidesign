<?php

class Pagemeta extends Eloquent {
    
    public static $table = 'pages_meta';
    
    public function page()
    {
        return $this->belongs_to('Page');
    }

}