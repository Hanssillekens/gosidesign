<?php

class Page extends Eloquent {
    
    public static $table = 'pages';
    
    
    public function meta(){
        return $this->has_many('Pagemeta');
    }
    
    public function children(){
        return $this->has_many('Page');
    }
    
    public function parent(){
        return $this->belongs_to('Page');
    }


    public function __toString() {
        return $this->name;
    }


    public static function full_page_array( $page_id, &$arr = array() ){

        // if we have a full array of pages for the current level.
        // we can easily check if the current page is in the array.
        // if so, the page is automatically active.
        $arr = ( !empty($arr) ? $arr : array() );

        $page = self::select(array('id', 'page_id'))->where_id($page_id)->first();

        if( null != $page ){
            array_unshift($arr, $page->id );

            if(0 < $page->page_id){
                self::full_page_array($page->page_id, $arr);
            }
        }

        return $arr;
    }


    public function page_template()
    {
        if( 0 < $this->attributes['template_id'] )
        {
            return Template::where_id( $this->attributes['template_id'] )->first()->slug;
        }
        else
        {
            return Template::where_default( 1 )->first()->slug;
        }
    }
}