<?php

class Category extends Eloquent {

    public static $key   = 'id';
    public static $table = 'categories';

    public function meta(){
        return $this->has_many('Categorymeta', 'category_id');
    }

    public static function by_module( $module, $parent_id = 0, $language_id = '' )
    {
        $language = ('' != $language_id ? $language_id : Scotty::get_language() );

        $sql = "SELECT cme.*
                FROM   categories_meta   AS cme
                JOIN   categories        AS c   ON ( c.id = cme.category_id )
                WHERE  c.module_id      = ?
                AND    c.deleted        = 0
                AND    cme.language_id  = ?
                AND    c.category_id    = ?
                GROUP BY cme.category_id
                ORDER BY c.sort ";

        $items = DB::query($sql, array($module, $language, $parent_id) );
        $data  = array();
        foreach($items AS $item){
            $item->image = Media::get_first_image( 'newsletters_category', $item->category_id );

            $data[ $item->category_id ] = $item;
        }

        return $data;
    }
}