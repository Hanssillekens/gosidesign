<?php

class Categorymeta extends Eloquent {

    public static $table = 'categories_meta';

    public function category()
    {
        return $this->belongs_to('Category');
    }
}