<?php

class Microsmeta extends Eloquent {

    public static $key   = 'id';
    public static $table = 'micros_meta';
    public static $timestamps = false;

    public function micro(){
        return $this->belongs_to('Micros', 'id');
    }
}