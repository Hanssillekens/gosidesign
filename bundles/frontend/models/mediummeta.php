<?php

class Mediummeta extends Eloquent {

    public static $key   = 'id';
    public static $table = 'media_meta';

    public function medium()
    {
        return $this->belongs_to('Medium');
    }
}