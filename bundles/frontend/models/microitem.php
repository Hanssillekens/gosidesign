<?php 

class Microitem extends Eloquent {
    
    public static $table        = 'micro_items';
    public static $id           = 'id';


    public function meta()
    {
        return $this->has_many('Microitemmeta', 'micro_items_id');
    }


    public static function get_module_id( $id )
    {
        $item = Microitem::find( $id );

        if( null != $item )
        {
            return $item->micro_id;
        }

        return $id;
    }

    /**
     * Get all items from a micro-module
     * @param  [type]  $module_id         Micro ID
     * @param  array   $new_conditions    Conditions = array('meta' => array(), 'item' => array())
     * @param  array   $new_order_options Order conditions
     * @param  string  $limit             Limit
     * @param  boolean $paginate          Results for paginate -> limit will be override for setting the amount of items for pagination
     * @return [type]                     [description]
     */
    public static function getAllItems( $module_id, $new_conditions = array(), $new_order_options = array(), $limit = '', $paginate = false )
    {
        /**
         *  Example of overriding
         *
         *  conditions => array(
         *      *fieldname* => *conditional operator*|*condition*
         *  )
         * 
         *  $new_conditions = array(
         *      'meta' => array(
         *          'public' => 0,
         *      ),
         *      'item' => array(
         *          'conditions' => array(
         *              'date_from' => '>|2013-12-07 00:00:00'
         *          )
         *      )
         *  );
         * 
         */
        # Default conditions for this method
        $default_conditions = array(
            'meta' => array(
                'language' => Scotty::get('language'),
                'public' => 1,
                'conditions' => array()
            ),
            'item' => array(
                'deleted' => 0,
                'archived' => 0,
                'conditions' => array()
            )
        );

        # Merge the data
        $conditions['meta'] = array_merge($default_conditions['meta'], (isset($new_conditions['meta'])) ? $new_conditions['meta'] : array() );
        $conditions['item'] = array_merge($default_conditions['item'], (isset($new_conditions['item'])) ? $new_conditions['item'] : array() );                        

        # Default order options
        if( empty($new_order_options) )
        {
            $order_options = array(
                'sort' => 'ASC'
            );
        }
        else
        {
            $order_options = $new_order_options;
        }            

        $base_query = Microitem::with(array('meta' => function( $query ) use ($conditions)
        {
            $query->where_language_id( $conditions['meta']['language'] );
            $query->where_public( $conditions['meta']['public'] );

            if( is_array($conditions['meta']["conditions"]) )
            {
                foreach($conditions['meta']["conditions"] AS $field => $value )
                {
                    $mode = '=';
                    if( str_contains( $value, '|' ) )
                    {
                        list($mode, $value) = explode('|', $value);
                    }

                    $query->where( $field, $mode, $value  );
                }
            }

            $query->get();

        }))->where_micro_id( $module_id )
           ->where_deleted( $conditions['item']['deleted'] )
           ->where_archived( $conditions['item']['archived'] );            

        # Custom conditons
        foreach ($conditions['item']['conditions'] as $key => $condition)
        {
            $condition_parts = explode('|', $condition);
            $base_query->where($key, $condition_parts[0], $condition_parts[1]);
        }

        # Order options
        if( $order_options == 'random' )
        {
            # Random
            $base_query->order_by( \DB::raw('RAND()') );
        }
        else
        {
            # Loop through the for each conditions
            foreach ($order_options as $key => $option)
            {
                $base_query->order_by($key, $option);
            }
        }

        # Check if return needs to paginated
        if( $paginate == true )
        {
            $results = $base_query->paginate( $limit );
            $items = $results->results; 
        }
        else
        {
            # Limit on the results?
            if( $limit != ''  )
            {
                $base_query->take( $limit );
            }         
            $items = $base_query->get();            
        }

        $return_items = array();

        # Remove empty meta's
        foreach ($items as $key => $item)
        {               
            if( empty($item->meta) )
            {
                unset( $items[$key] );
            }
            else
            {
                
                $items[$key]->meta_data = $item->meta[0];
                                    

                if( $items[$key]->meta_data->custom_fields_json != '' )
                {
                    # Rewrite JSON to Array
                    $items[$key]->meta_data->custom_fields_json = (array) json_decode( $items[$key]->meta_data->custom_fields_json );
                }                    

                # Check if item has relations
                $items[$key]->relation_with_module_item = static::getRelationItem( $items[$key]->id,  'micros_' .$items[$key]->micro_id);

                # Check for ShortCodes
                $items[$key]->meta_data->content = \Shortcode::build( $items[$key]->meta_data->content );

                # Reorder the items
                $return_items[] = $items[$key];
            }
        }
        
        # if paginate
        if( $paginate == true )
        {
            $results->results = $return_items;
            $return_items = $results;
        }            

        return $return_items;
    }

    /**
     * Get a specific Micro item
     * @param  [type] $module_id       Micro ID
     * @param  array  $meta_conditions Conditions
     * @return [type]                  [description]
     */
    public static function getItem( $module_id, $meta_conditions = array() )
    {
        $microitemids = self::where_micro_id( $module_id )
            ->where_deleted( 0 )
            ->lists('id', 'id');            

        if( !empty($microitemids) )
        {
            $base_query = Microitemmeta::where_in('micro_items_id', $microitemids)
                ->where_language_id( Scotty::get('language') )
                ->where_public( 1 );

            foreach ($meta_conditions as $key => $condition)
            {
                $condition_parts = explode('|', $condition);                    
                $base_query->where($key, $condition_parts[0], $condition_parts[1]);
            }

            $item = $base_query->first();

            if( !empty($item) )
            {
                $microitem = self::find( $item->micro_items_id );
                $microitem->meta_data = $item;

                # Check if item has relations
                $microitem->relation_with_module_item = static::getRelationItem( $microitem->id,  'micros_' .$microitem->micro_id);

                # Check for ShortCodes
                $microitem->meta_data->content = \Shortcode::build( $microitem->meta_data->content );

                # Rewrite JSON to Array
                $microitem->meta_data->custom_fields_json = (array) json_decode( $microitem->meta_data->custom_fields_json );
                return $microitem;
            }
        }

        return null;
    }

    /**
     * Get the previous item based on the micro-id and current-item 
     * @param  integer $module_id       Micro id
     * @param  integer $current_item    Current-item id
     * @param  array   $new_conditions  Search-conditions
     * @param  array   $new_order       New order conditions
     * @return [type]               [description]
     */
    public static function getPreviousItem( $module_id, $current_item, $new_conditions = array(), $new_order = array() )
    {
        # Default conditions for searching (only item)
        $default_conditions = array(
            'item' => array(
                'conditions' => array(
                    'date_from' => '<|' .$current_item->date_from,
                )
            )
        );

        # Merge the default conditions
        $conditions['item'] = array_merge($default_conditions['item'], (isset($new_conditions['item'])) ? $new_conditions['item'] : array() );

        # Order options
        $default_order = array(
            'date_from' => 'DESC'
        );

        # Merge the order conditions
        $order = array_merge($default_order, $new_order);            

        $items = self::getAllItems( $module_id, $conditions, $order );

        return ( isset($items[0]) ) ? $items[0] : array();
    }

    /**
     * Get the next item based on the micro-id and current-item 
     * @param  integer $module_id       Micro id
     * @param  integer $current_item    Current-item id
     * @param  array   $new_conditions  Search-conditions
     * @param  array   $new_order       New order conditions
     * @return [type]               [description]
     */
    public static function getNextItem( $module_id, $current_item, $new_conditions = array(), $new_order = array() )
    {
        # Default conditions for searching (only item)
        $default_conditions = array(
            'item' => array(
                'conditions' => array(
                    'date_from' => '>|' .$current_item->date_from,
                )
            )
        );

        # Merge the default conditions
        $conditions['item'] = array_merge($default_conditions['item'], (isset($new_conditions['item'])) ? $new_conditions['item'] : array() );

        # Order options
        $default_order = array(
            'date_from' => 'ASC'
        );

        # Merge the order conditions
        $order = array_merge($default_order, $new_order); 

        $items = self::getAllItems( $module_id, $conditions, $order );

        return ( isset($items[0]) ) ? $items[0] : array();
    }

    /**
     * Check if the micro-item has a relation with an item from another module
     * @param  integer $microItemId      MicroItem-ID
     * @param  integer $microItemMicroId MicroItem-MicroId
     * @return [type]                    [description]
     */
    public static function getRelationItem($microItemId = 0, $microItemMicroId = 0)
    {
        $relation = ModuleRelation::where_current_item_id( $microItemId )
            ->where_current_module( $microItemMicroId )
            ->first();            

        if( !is_null($relation) )
        {
            # Set the model
            $model = Config::get('frontend::models.' .$relation->relation_module .'_meta');
            
            # Check if relation is with a micro-module
            if( strstr($relation->relation_module, 'micros|') )
            {
                $model = array('model' => 'Microitemmeta');
            }                      

            $item = $model['model']::find( $relation->relation_item_id );
            $data = ( !is_null($item) ) ? $item->to_array() : array();

            # Check if relation is a microitem
            # Return the relation micro-name
            $data['relation_module'] = $relation->relation_module;
            if( strstr($data['relation_module'], 'micros|') )
            {
                # Update Address and Custom fields
                $data['custom_fields_json'] = json_decode( $data['custom_fields_json'] );
                $data['address_json'] = json_decode( $data['address_json'] );

                $parts = explode('|', $data['relation_module']);
                $microId = end( $parts );
                    
                $micrometa = Microsmeta::where_language_id( Scotty::get('language') )
                    ->where_micro_id( $microId )
                    ->first();

                if( !is_null($micrometa) )
                {
                    $data['relation_module_micro_name'] = $micrometa->title;
                }
            }                

            return $data;
        }

        return array();
    }
}