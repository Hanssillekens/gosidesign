<?php

class Formitem extends Eloquent {
    
    public static $table = 'forms';
    public static $id = 'id';

    public function meta(){
        return $this->has_many('Formmeta', 'form_id');
    }

}