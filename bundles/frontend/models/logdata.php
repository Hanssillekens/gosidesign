<?php

class Logdata extends Eloquent {

    public static $table = 'log_data';
    public static $id = 'id';


    public static $timestamps = true;

}