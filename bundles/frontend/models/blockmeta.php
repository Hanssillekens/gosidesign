<?php

class Blockmeta extends Eloquent {

    public static $key   = 'id';
    public static $table = 'blocks_meta';

    public function block()
    {
        return $this->belongs_to('Block', 'block_id');
    }


    public function image()
    {
        $language = Scotty::get_language();

        $images = Medium::with(array('meta' => function($query) use($language){
            $query->where_language_id( $language )->first();
        }))->where_module('blocks')->where_type('p')->where_type_module('none')->where_item_id( $this->block_id )->first();

        if( null != $images )
        {
            $img = '<img src="'.Config::get('application.cdn_url').'modules/blocks/'.$images->src.'" />';

            if( !empty($images->relationships['meta']) ){
                $reg = $images->relationships['meta'][0];

                if( 'internal' ==  $reg->link_type ){
                    return '<a href="'.Tools::path_to_page_by_id( $reg->link_src ).'">'.$img.'</a>';
                }elseif( 'external' ==  $reg->link_type){
                    return '<a target="_blank" href="'.$reg->link_src.'">'.$img.'</a>';
                }
            }

            return $img;
        }

        return null;
    }
}