<?php

class Locationmeta extends Eloquent {

    public static $key   = 'id';
    public static $table = 'locations_meta';
    public static $timestamps = false;

    public function location(){
        return $this->belongs_to('Location', 'location_id');
    }
}