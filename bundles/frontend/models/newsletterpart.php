<?php

class Newsletterpart extends Eloquent {

    public static $table = 'newsletters_parts';

    public function newsletter()
    {
        return $this->belongs_to('Newsletter');
    }   
}