<?php

class Translationmeta extends Eloquent {

    public static $key   = 'id';
    public static $table = 'translations_meta';

    public function translation()
    {
        return $this->belongs_to('Translation', 'translation_id');
    }
}