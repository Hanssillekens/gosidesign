<?php

class Filterpair extends Eloquent {

    public static $key        = 'filter_id';
    public static $table      = 'filter_pair';
    public static $timestamps = false;

}