<?php

class Translation extends Eloquent {

    public static $table = 'translations';
    public static $id = 'id';

    public function meta()
    {
        return $this->has_many('Translationmeta', 'translation_id');
    }

}