<?php

class Location extends Eloquent {

    public static $key   = 'id';
    public static $table = 'locations';

    public function meta(){
        return $this->has_many('Locationmeta', 'location_id');
    }
    
}