<?php

class Jobmeta extends Eloquent {

    public static $key   = 'id';
    public static $table = 'jobs_meta';
    public static $timestamps = false;

    public function job()
    {
        return $this->belongs_to('Jobs', 'id');
    }
}