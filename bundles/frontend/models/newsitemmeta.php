<?php

class Newsitemmeta extends Eloquent {

    public static $key   = 'id';
    public static $table = 'news_meta';

    public function newsitem()
    {
        return $this->belongs_to('Newsitem');
    }
    
}