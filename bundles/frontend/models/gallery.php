<?php

class Gallery extends Eloquent {
    
    public static $table = 'galleries';
    public static $id = 'id';

    public function meta(){
        return $this->has_many('Gallerymeta', 'gallery_id');
    }


    public function random_image()
    {
        return Medium::where_module('galleries')->where_type('p')->where_type_module('none')->where_item_id( $this->id )->first();
    }


    public function images()
    {
        return Medium::where_module('galleries')->where_type('p')->where_type_module('none')->where_item_id( $this->id )->get();;
    }
}