<?php

class Productattribute extends Eloquent {

    public static $key   = 'id';
    public static $table = 'product_attributes';

    public static $accessible = array(
        'product_id',
        'language_id',
        'attribute_id',
        'value',
    );
}