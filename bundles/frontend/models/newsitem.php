<?php

class Newsitem extends Eloquent {

    public static $key   = 'id';
    public static $table = 'news';

    public function meta()
    {
        return $this->has_many('Newsitemmeta', 'news_id');
    }

    public function by_date( $args = null )
    {
        $tot = count($args);

        if( 3 <= $tot )
        {
            return $this->where('date_from', '=', $args[0].'-'.$args[1].'-'.$args[2].' 00:00:00' );
        }
        elseif( 2 == $tot )
        {
            return $this->where('date_from', '>', $args[0].'-'.$args[1].'-01 00:00:00' )->where('date_from', '<', $args[0].'-'.$args[1].'-31 00:00:00' );
        }
        elseif( 1 == $tot )
        {
            return $this->where('date_from', '>', $args[0].'-01-01 00:00:00' )->where('date_from', '<', $args[0].'-12-31 00:00:00' );
        }

        return $this;
    }

    public static function path( $id, $language = '' )
    {
        $language = ('' == $language ? Scotty::get_language() : $language );
        $path = '';

        $item = self::with(array('meta' => function($query) use($language) {
            $query->where_language_id( $language )->first();
        }))->where_id( $id )->first();

        if(null != $item )
        {
            $slug = $item->relationships['meta'][0]->slug;
            list( $date, $time ) = explode(' ', $item->date_from );

            $path = str_replace('-', '/', $date ).'/'.$slug.'/';
        }



        return $path;
    }
}