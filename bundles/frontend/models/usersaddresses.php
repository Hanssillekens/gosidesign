<?php

class Usersaddresses extends Eloquent{

    public static $key   = 'id';
    public static $table = 'users_addresses';
    public static $timestamps = false;

    public function user()
    {
        return $this->belongs_to('User', 'user_id');
    }
}