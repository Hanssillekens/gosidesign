<?php

class Newslettermeta extends Eloquent {

    public static $table = 'newsletters_meta';

    public function newsletter()
    {
        return $this->belongs_to('Newsletter');
    }   
}