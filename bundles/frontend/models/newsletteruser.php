<?php 

class NewsletterUser extends Eloquent {
    
    public static $table = 'newsletters_users';

    public static $timestamps = false;
}