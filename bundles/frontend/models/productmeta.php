<?php

class Productmeta extends Eloquent {

    public static $key   = 'id';
    public static $table = 'products_meta';
    public static $timestamps = false;

    public function product()
    {
        return $this->belongs_to('Product', 'product_id');
    }

    public static $accessible = array(
        'title',
        'slug',
        'content',
        'public'
    );

    public static function get_default_fields()
    {
        $default_fields = array(
            'title',
            'slug',
            'content',
            'public'
        );

        return $default_fields;
    }
}