<?php

return array(

	'page'				=> array(
		'model' => 'Page',
	),
	'pages_meta'	=> array(
		'model' 		=> 'Pagemeta',
		'parent_field'  => 'page_id',
	),

	'blocks_meta'	=> array(
		'model' 		=> 'Blockmeta',
		'parent_field'  => 'block_id',
	),

	'galleries'			=> 'Gallery',
	'galleries_meta'	=> array(
		'model'  	    => 'Gallerymeta',
		'parent_field'  => 'gallery_id'
	),

	'faqs'				=> 'Faq',
	'faqs_meta'		=> array(
		'model'  	    => 'Faqmeta',
		'parent_field'  => 'faq_id'
	),

	'partners'			=> 'Partner',
	'partners_meta'		=> array(
		'model'  	    => 'Partnermeta',
		'parent_field'  => 'partner_id'
	),

	'news'      		=> 'Newsitem',
	'news_meta' 		=> array(
		'model' 	    => 'Newsitemmeta',
		'parent_field'  => 'news_id',
	),

	'products'  		=> 'Products',
	'products_meta' 	=> array(
		'model' 		=> 'Productmeta',
		'parent_field'	=> 'product_id'
	),

	'jobs'				=> 'Job',
	'jobs_meta'			=> array(
		'model' 		=> 'Jobmeta',
		'parent_field'  => 'job_id',
	),
	'events_meta'			=> array(
		'model' 		=> 'Eventitemmeta',
		'parent_field'  => 'event_id',
	),

	'newsletters_meta'	=> array(
		'model' 		=> 'Newslettermeta',
		'parent_field'  => 'newsletter_id',
	),

	'microitems_meta'	=> array(
		'model' 		=> 'Micrometa',
		'parent_field'  => 'micro_id',
	),

);