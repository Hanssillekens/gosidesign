<?php

return array(
    'news' => array(
        'archive' => array(
             'nl' => 'archief'
            ,'en' => 'archive'
            ,'fr' => 'archives'
            ,'de' => 'archiv'
            ,'it' => 'archivio'
            ,'es' => 'archivo'
        ),
        'category' => array(
             'nl' => 'categorie'
            ,'en' => 'category'
            ,'fr' => 'categorie'
            ,'de' => 'kategorie'
            ,'it' => 'categoria'
            ,'es' => 'categoria'
        ),
        'rss' => array(
            '*' => 'rss'
        )
    ),
    'forms' => array(
        'thanks' => array(
             'nl' => 'bedankt'
            ,'en' => 'thanks'
            ,'fr' => 'merci'
            ,'de' => 'danke'
            ,'it' => 'grazie'
            ,'es' => 'gracias'
        )
    ),
    'jobs' => array(
        'apply' => array(
             'nl' => 'solliciteren'
            ,'en' => 'apply'
            ,'fr' => 'appliquer'
            ,'de' => 'anwenden'
            ,'it' => 'applicare'
            ,'es' => 'aplicar'
        )
    )
);