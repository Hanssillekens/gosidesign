<?php

return array(
    'news'          => 'News::shortcode',
    'forms'         => 'Forms::shortcode',
    'partners'      => 'partners::shortcode',
    'galleries'     => 'Galleries::shortcode',
    'INTERNALLINK'  => 'Tools::internallink',
    'INTERNALMEDIA' => 'Tools::internalmedia',
    'internallink'  => 'Tools::internallink',
    'internalmedia' => 'Tools::internalmedia',
    'routeplanner'  => 'Tools::routeplanner',
    'sitemap'       => 'Tools::sitemap',
    'search'        => 'Search::shortcode',
    'product_search'=> 'Products::shortcode_search',
    'micros'        => 'Micros::shortcode',
    'youtube'       => 'Media::youtube',
    'vimeo'         => 'Media::vimeo',
    'slider'        => 'Sliders::shortcode',
);