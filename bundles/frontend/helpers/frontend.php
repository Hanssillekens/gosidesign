<?php

function tl( $key, $striptags = true, $language = '')
{
    $language = ('' == $language ? Scotty::get_language() : $language );

    $sql = "SELECT tm.content
            FROM   translations      AS t
            JOIN   translations_meta AS tm ON ( tm.translation_id = t.id AND tm.language_id = ? )
            WHERE  t.key_code = ? ";

    $ret = DB::query($sql, array( $language, $key ) );

    if( null != $ret )
    {
        $key = ( '' != $ret[0]->content ? $ret[0]->content : $key );
    }
    else
    {
        $translation = new Translation();
        $translation->key_code = $key;
        $translation->save();

        $id = $translation->id;

        foreach( Scotty::get_languages() AS $lang_key => $language )
        {
            $translation_meta = new Translationmeta();

            $translation_meta->language_id    = $lang_key;
            $translation_meta->translation_id = $id;
            $translation_meta->save();
        }
    }

    return ( true === $striptags ? strip_tags($key) : $key);
}