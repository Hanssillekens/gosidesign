<?php

class Frontend_Newsletter_Controller extends Base
{
	public static $restfulController = true;

	public function __construct(){}

	public static function get_view_newsletter()
	{
		# No UID, redirect to home
		if( !Input::get('uid') )
		{
			return Redirect::home();
			exit;
		}

		# Update the newsletter-data
		$nl_user = NewsletterUser::where_unique_id( Input::get('uid') )
			->first();
		// If unique-id doenst exists, redirect to home
		if( is_null($nl_user) )
		{
			return Redirect::home();
			exit;
		}
		// User has openend the newsletter
		$nl_user->openend = 1;
		$nl_user->save();

		# Set variables
		$id 	   = Input::get('nl'); // Newsletter-id
		$language  = Input::get('lng'); // Language
        $unique_id = Input::get('uid'); // Unique-id;

        # Retrieve the user data
        $user = User::find( $nl_user->user_id );

        # User wants to unsubscribe
        if( Input::get('unsubscribe') && Input::get('unsubscribe') == 1 )
        {
        	# Get the categories
        	$categories = Categorymodule::where_item_id( $id )
        		->where_module_id( 'newsletters' )
        		->get('category_id');
        	
        	if( !is_null($categories) )
        	{
	        	foreach ($categories as $key => $category)
	        	{
	        		$user = Newslettercategoriesusers::where_category_id( $category->category_id )
	        			->where_user_id( $user->id )
	        			->first();

	        		if( !is_null($user) )
	        		{
	        			$user->status = 2;
	        			$user->save();
	        		}
	        	}
	        	return Redirect::to('/newsletter/unsubscribe/' .$language);
	        	exit;
        	}
        }

        # Build the newsletter-template
		$newsletter = NewsletterItem::find($id);
        $template = static::build_template($newsletter, $language);

        # View online version - Also replace the social media settings
        $url = 'newsletter?uid=' .$unique_id .'&amp;lng=' .$language .'&amp;nl=' .$id;
        $template = str_replace('__ONLINEVERSION', $url, $template);

        # Add the unique code to the template
        $template = str_replace('__UNIQUEID', $unique_id, $template);   	
        # Add the firstname, lastname and emailaddress to the template
        $template = str_replace('__FIRSTNAME', $user->firstname, $template);
        $template = str_replace('__LASTNAME', $user->lastname, $template);
        $template = str_replace('__EMAILADDRESS', $user->email, $template);

        return $template; 
	}

	/**
     * Build the complete Newsletter Template
     * @param  array  $newsletter Newsletter object
     * @param  string $language   Language
     * @return [type]             HTML
     */
    protected static function build_template($newsletter = array(), $language = '')
    {
        # Get the template
        $item = Newslettermeta::where_newsletter_id( $newsletter->id )
            ->where_language_id( $language )
            ->first();

        # Main image
        if( $item->image_id != 0 )
        {
            $image = Medium::find( $item->image_id );
            $image = ( is_null($image) ) ? null : $image;
            $item->attributes['image'] = $image;
        }
        
        # Get the block-parts 
        $blocks = Newsletterpart::where_newsletter_meta_id($item->id)
            ->order_by('sort', 'asc')
            ->get();

        # Combine the default template CSS with our custom CSS
        $default_css = Beam::make('mail.newsletters.default_css')->render();
        $newsletter->css = htmlspecialchars( json_decode( $newsletter->css ) );
        $css = $default_css . $newsletter->css;            

        # Project URL
        $project = new stdClass;
        $project->url  = $_SERVER['HTTP_HOST'];        
        $project->slug = $_SERVER['HTTP_HOST'];        
        if( $_SERVER['HTTP_HOST'] == 'demotb.dev' )
       	{
       		$project->url  = 'demo.internetanders.nl';        
        	$project->slug = 'demo.internetanders.nl'; 
       	}               	         

        $data = array(
            'default_css' => $css,
            'parent' => $newsletter,
            'language' => $language, 
            'newslettermeta' => $item,
            'blocks' => $blocks,
            'project' => $project
        );       	

        # Render the HTML
        $html = Beam::make('mail.newsletters.template', $data)->render();        	

        # Emogrifier
        $emogrifier = new Emogrifier();
        $emogrifier->setHTML($html);
        $emogrifier->setCSS($css);

        # Render the template
        $template = $emogrifier->emogrify();

        return $template;
    }

    /**
     * [get_unsubscribe_view description]
     * @return [type] [description]
     */
    public static function get_unsubscribe_view()
    {
    	return 'U bent uitgeschreven (hier komt de view)';	
    }

}

?>