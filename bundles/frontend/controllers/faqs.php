<?php

class Frontend_Faqs_Controller extends Base{
    private static $args = array();

    public static function dynamic(){
        self::$args = func_get_args();

        return self::output();
    }

    protected static function output(){
        switch(count(self::$args)){
            default : return self::get_faqs(); break;
        }
    }

    protected static function get_faqs()
    {
        // get FAQ categories
        $categories = Category::by_module('faqs');

        $faqs = array();
        foreach($categories AS $key => $category )
        {
            $categories_items = Categorymodule::select('item_id')->where_module_id('faqs')->where_category_id($key)->lists('item_id');
            $categories_items = ( empty($categories_items) ) ? array(0) : $categories_items;
            $faqs[$key]  = Faqmeta::with('Faq')->where_in('faq_id', $categories_items)->where_language_id( Scotty::get_language() )->where_public(1)->get();
        }

        // get uncategorized
        $product_ids = Categorymodule::select('item_id')->where_module_id('faqs')->lists('item_id');
        $product_ids = ( empty($product_ids) ) ? array(0 => 0) : $product_ids;
        $faqs[0]     = Faqmeta::with('Faq')->where_not_in('faq_id', $product_ids)->where_language_id( Scotty::get_language() )->where_public(1)->get();

        static::before_render();

        return Beam::make('faqs/list', array('faqs' =>  $faqs, 'categories' => $categories) );
    }
}