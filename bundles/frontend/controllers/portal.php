<?php

class Frontend_Portal_Controller extends Base
{
    private static $args = array();

    public static $restfulController = true;


    public function __construct()
    {
        Beam::$menu_sub = Beam::make('portal/menu');
    }


    public function get_index()
    {
        return Redirect::to( Scotty::$module_url.'login' );
    }


    public function get_login()
    {
        return Beam::make('portal/login');
    }


    public function get_logout()
    {
        Auth::logout();

        return Redirect::to( Scotty::$module_url );
    }


    public function get_profile()
    {
        if( Auth::guest() ){
            return Redirect::to( Scotty::$module_url );
        }

        return Beam::make('portal/profile');
    }


    public function get_account()
    {
        // get extended user info.
        $extended_user = Usersextended::where_user_id( Auth::user()->id )->first();

        if(null == $extended_user){
            $extended_user = new Usersextended;
            $extended_user->user_id = Auth::user()->id;
            $extended_user->save();
        }

        // get user main adress
        $address_user = Usersaddresses::where_user_id( Auth::user()->id )->where_title('main')->first();

        if(null == $address_user){
            $address_user           = new Usersaddresses;
            $address_user->user_id  = Auth::user()->id;
            $address_user->title    = 'main';
            $address_user->save();
        }

        return Beam::make('portal/account')->with('extended_user', $extended_user)->with('address_user', $address_user);
    }


    public function get_register()
    {
        return Beam::make('portal/register' );
    }


    public function get_subscription()
    {
        // get all newsletter categories
        $data['categories'] = Category::by_module('newsletters');
        // get all active newsletters for the user

        $data['categories_user'] = Newslettercategoriesusers::where_user_id( Auth::user()->id )->lists('status', 'category_id');

        return Beam::make('portal/subscriptions', $data);
    }


    public function post_login()
    {
        $error = '';
        if( Input::has('email') && Input::has('password') )
        {
            $rules = array(
                'email'     => 'required|email',
                'password'	=> 'required|min:5',
            );
            // Validate all input
            $validator = Validator::make( Input::all(), $rules );

            if( $validator->fails() )
            {
                return Redirect::to( Scotty::$module_url.'login' )->with_errors($validator->errors);
            }

            $auths = array(
                'username' => Input::get('email'),
                'password' => Input::get('password'),
            );

            if( Auth::attempt( $auths ) ){
                return Redirect::to( Scotty::$module_url.'profile' );
            }else{
                $error = tl('Username or password incorrect.');
            }
        }

        return Redirect::to( Scotty::$module_url.'login' )->with( 'error', $error );
    }


    public function post_register()
    {
        $error = '';

        if( Input::has('email') && Input::has('password') )
        {
            $rules = array(
                'firstname'     => 'required|alpha|max:80',
                'lastname'      => 'required|alpha|max:120',
                'email'         => 'required|email|unique:users',
                'password'      => 'required|min:6'
            );

            $validator = Validator::make( Input::all(), $rules );

            if( ! $validator->valid() )
            {
                return Redirect::to( Scotty::$module_url.'register')->with_errors($validator->errors);

            }elseif(Input::get('password') != Input::get('password_verify') ){
                return Redirect::to( Scotty::$module_url.'register')->with_errors( tl('Passwords do not match') );
            }
            else{
                // Create a new user
                $user = new User;
                $user->fill( Input::all() );

                unset( $user->password_verify );

                $user->password     = Hash::make( $user->password );
                $user->role_id      = 1;
                $user->language_id  = Scotty::get_language();

                $user->save();
            }
        }

        return Redirect::to( Scotty::$module_url.'register')->with_success( 'ok' );
    }


    public function post_subscription()
    {
        $data['error'] = array(
            'subscription' => '',
        );

        if( Input::has('subscribe_form') && Input::has('categories') )
        {
            if( Input::has('terms') )
            {
                // deactivate all
                Newslettercategoriesusers::query()->where('user_id','=', Auth::user()->id )->update( array(
                    'status' => 2,
                ) );

                //subscribe checked newsletters
                foreach(Input::get('categories') AS $key => $value )
                {
                    $subscription = Newslettercategoriesusers::where_user_id( Auth::user()->id )->where_category_id($key)->first();
                    if( null == $subscription )
                    {
                        $subscription               = new Newslettercategoriesusers;
                        $subscription->category_id  = $key;
                        $subscription->user_id      = Auth::user()->id;
                        $subscription->status       = 1;
                        $subscription->save();
                    }else
                    {
                        $subscription->status       = 1;
                        $subscription->save();
                    }
                }
            }
            else{
                return Redirect::to( Scotty::$module_url.'subscription')->with('subscription_error', tl('In order to subscribe you need to accept our terms and conditions.') );
            }
        }elseif( Input::has('subscribe_form') && !Input::has('categories') )
        {
            //unsubscribe all newsletters
            Newslettercategoriesusers::query()->where('user_id','=', Auth::user()->id )->update( array(
                'status' => 2,
            ) );
        }

        return Redirect::to( Scotty::$module_url.'subscription');
    }


    public function post_save_user()
    {
        $input = array(
            'firstname'     => Input::get('firstname'),
            'lastname'      => Input::get('lastname'),
        );

        $rules = array(
            'firstname'     => 'required|alpha|min:2|max:80',
            'lastname'      => 'required|alpha|min:2|max:120',
            'email'         => 'required|email|unique:users',
            'password'      => 'required|min:6'
        );

        $validator = Validator::make( $input, $rules );

        if( !$validator->valid() )
        {
            return Redirect::to( Scotty::$module_url.'account')->with_errors($validator->errors);
        }

        $user = User::find( Auth::user()->id );

        $user->firstname     = Input::get('firstname');
        $user->lastname      = Input::get('lastname');

        $user->save();

        return Redirect::to( Scotty::$module_url.'account')->with('success', tl('Profile updated successfully') );
    }


    public function post_save_extended()
    {
        $extended_user = Usersextended::where_user_id( Auth::user()->id )->first();
        $extended_user->fill( Input::get() );
        $extended_user->save();

        return Redirect::to( Scotty::$module_url.'account')->with('success', tl('Profile updated successfully') );
    }


    public function post_save_address()
    {
        $address_user = Usersaddresses::where_user_id( Auth::user()->id )->where_title('main')->first();
        $address_user->fill( Input::get() );
        $address_user->save();

        return Redirect::to( Scotty::$module_url.'account')->with('success', tl('Profile updated successfully') );
    }
}