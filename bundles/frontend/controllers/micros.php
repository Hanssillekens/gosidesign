<?php

class Frontend_Micros_Controller extends Base
{
    private static $args = array();
    public static $restfulController = true;


    public function __call($method, $parameters)
    {
        $rest   = Request::method();            

        // Exclude the DETAIL in the url
        if( !strstr($method, 'index') )
        {   
            # Check if it contains a year in the URL
            if( (isset($parameters[0]) && isset($parameters[1]) && isset($parameters[2])) && checkdate( $parameters[1], $parameters[2], $parameters[0] ) )
            {                                                      
                # Parse date and slug
                $date = $parameters[2] .'/' .$parameters[1] .'/' .$parameters[0];
                $parameters = array(array('date' => $date, 'slug' => end($parameters)));
                $method = $rest .'_detail';                   
                return call_user_func_array( array( $this, $method ) , $parameters );
            }

            # Check if the micro-module still is public
            $micro_meta_slug = str_replace('GET_', '', $method);                
            $micro_meta = Microitemmeta::where_slug( $micro_meta_slug )
                ->where_public( 1 )
                ->where_language_id( Scotty::get_language() )
                ->first();                                                                                                    

            if( !is_null($micro_meta) )
            {
                # Get all availeble micro-items
                $microitemslist = Microitem::where_id( $micro_meta->micro_items_id )
                    ->lists('id');                    

                # Get the meta based on the micro-item
                $microitemmeta = Microitemmeta::where_slug( $micro_meta_slug )
                    ->where_in('micro_items_id', $microitemslist)
                    ->where_public( 1 )
                    ->where_language_id( Scotty::get_language() )
                    ->first();                                                                                    

                if( !empty($microitemmeta) )
                {
                    # Is a detail page
                    $parameters = array($microitemmeta->slug);
                    $method = $rest .'_detail';
                }                   

                return call_user_func_array( array( $this, $method ) , $parameters );
            }                 
                
        }

        return 'missing => '.$method.'<br/><br/><pre>'.print_r($parameters, true).'</pre>';
    }


    public static function navigation( $id = 0, $language = '' )
    {
        $language = ('' == $language ? Scotty::get_language() : $language );

        $data  = array();
        $items = Microitem::where_micro_id( Scotty::$module_id )->where_deleted(0)->get();

        if( null != $items )
        {
            foreach( $items AS $item )
            {
                $meta = $item->meta()->where_language_id( $language )->first();

                if( null != $meta ){
                    $data[] = array(
                        'slug'  => $meta->slug,
                        'title' => $meta->title
                    );
                }
            }

        }

        return $data;
    }

    public static function shortcode(){
        $params = func_get_args();
        $params = ( array_key_exists(0 , $params) ? $params[0] : array() );

        if( array_key_exists('module', $params) && array_key_exists('method', $params) )
        {
            $controller = 'Micros_'.ucfirst( $params['module'] );

            if(class_exists($controller))
            {
                unset($params['module']);

                $method = 'sc_'.$params['method'];

                if( method_exists($controller, $method ) )
                {
                    unset($params['method']);
                    return call_user_func_array( array($controller,$method), array($params) );
                }
            }
        }

        return '';
    }


    protected static function get_micro_module_id( $module_name )
    {
        $item = Microsmeta::select('micro_id')->where_slug( $module_name )->first();

        if(null != $item)
        {
            return $item->micro_id;
        }

        return 0;
    }
}