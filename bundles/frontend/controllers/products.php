<?php

class Frontend_Products_Controller extends Base{
    protected static $args = array();

    public function __construct( $module_params ){
        self::$args = $module_params;

        return $this;
    }


    public function build(){
        $output = static::output();

        if( null != $output )
        {
            Beam::$module_content = $output->render();
        }

        static::before_render();

        return Beam::make('')
            ->nest('content',       'base/content')
            ->nest('head',          'base/head')
            ->nest('header',        'base/header')
            ->with( 'og',           static::$og )
            ->nest('footer',        'base/footer')
            ->nest('base_js',       'base/js')
            ->render();
    }


    protected static function output()
    {
        static::$args = ( empty(static::$args) ? Scotty::$module_params : static::$args );

        if( 2 == count(static::$args) && 'ajax' == static::$args[0] )
        {
            return static::ajax();
        }else
        {
            switch(count(static::$args)){
                // category/sub-category/product   -> one product
                case 3  : return static::get_product_item();        break;
                // category/sub-category   -> product for sub-category
                // category/               -> product for category
                default : return static::get_product_by_category(); break;
            }
        }
    }


    protected static function get_product_item()
    {
        // get parent category id based on the slug
        $parent_category = Categorymeta::select()->where_slug( self::$args[0] )->where_language_id( Scotty::get_language() )->first();

        $category    = null;
        $product     = null;
        $slug        = self::$args[0].'/'.self::$args[1].'/';
        $title       = '';
        if( null != $parent_category )
        {
            $category = Categorymeta::where_slug( self::$args[1] )->where_language_id( Scotty::get_language() )->first();

            $title.= ' - '.$parent_category->title;

            if( null != $category )
            {
                $title.= ' - '.$category->title;
                $product = Product::product_detail( self::$args[2] );

                if( null != $product){
                    $title.= ' - '.$product->title;
                }
            }
        }

        Beam::$page->name.= $title;
        Beam::$page->content = Beam::make('products/product_detail')
            ->with('parent_category', $parent_category)
            ->with('category', $category)
            ->with('product', $product)
            ->with('slug', $slug )
            ->with('module_url', Scotty::$module_url);

        return;
    }


    protected static function get_product_by_category()
    {
        if( empty( self::$args) )
        {
            return static::make_category_list();
        }
        elseif( isset( self::$args[0] ) && !isset( self::$args[1] )  ) // detect category
        {
            Beam::$disable_page_content = true;
            return static::make_category();
        }
        elseif( isset( self::$args[0] ) && isset( self::$args[1] )  ) // detect subcategory
        {
            Beam::$disable_page_content = true;
            return static::make_sub_category();
        }
    }


    protected static function make_category_list()
    {
        $categories  = Category::by_module('products');
        $parent_slug = '';

        return Beam::make('products/category_list', array('categories' => $categories, 'parent_slug' => $parent_slug, 'module_url' => Scotty::$module_url ) );
    }


    protected static function make_category()
    {
        // get parent category id based on the slug
        $category = Categorymeta::where_slug( self::$args[0] )->where_language_id( Scotty::get_language() )->first();

        $categories  = array();
        $parent_slug = self::$args[0].'/';

        if( null != $category )
        {
            Beam::$page->page_title = $category->title;
            Beam::$page->content    = $category->content;

            Beam::$page->name.= ' - '.$category->title;
            $categories  = Category::by_module('products', $category->category_id );
        }

        return Beam::make('products/category_list', array('categories' => $categories, 'parent_slug' => $parent_slug,  'module_url' => Scotty::$module_url ) );
    }


    protected static function make_sub_category(){
        // get parent category id based on the slug
        $parent_category = Categorymeta::select()->where_slug( self::$args[0] )->where_language_id( Scotty::get_language() )->first();

        $category    = null;
        $products    = array();
        $slug        = self::$args[0].'/'.self::$args[1].'/';
        $title       = '';
        if( null != $parent_category )
        {
            $category = Categorymeta::where_slug( self::$args[1] )->where_language_id( Scotty::get_language() )->first();

            $title.= ' - '.$parent_category->title;

            $products = array();
            if( null != $category )
            {
                $title.= ' - '.$category->title;
                $products  = Product::products_list_by_category( $category->category_id );
            }
        }

        Beam::$page->name      .= $title;
        Beam::$page->page_title = trim($title, ' -');
        Beam::$page->content    = '';

        return Beam::make('products/product_list_sub')
            ->with('parent_category', $parent_category)
            ->with('category', $category)
            ->with('products', $products)
            ->with('slug', $slug )
            ->with('module_url', Scotty::$module_url);
    }


    public static function ajax()
    {
        switch(static::$args[1]){
            case 'json_product_list' : return self::json_product_list();
        }
    }


    public static function json_product_list()
    {
        $category_id = Input::get('category_id');
        $products    = Product::products_by_category( $category_id );
        $data        = array();

        foreach( $products AS $product )
        {
            $data[] = array(
                'title' => $product->title,
                'url'   => Product::path( $product->product_id )
            );
        }

        echo json_encode( $data ); exit;
    }


    public static function shortcode_search(){

    }
}