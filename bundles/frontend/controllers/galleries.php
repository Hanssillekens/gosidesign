<?php

class Frontend_Galleries_Controller extends Base{
    private static $args = array();

    public function __construct( $module_params ){
        self::$args = $module_params;

        return $this;
    }


    public function build(){
        Beam::$module_content = self::output()->render();

        static::before_render();

        echo Beam::make('')
            ->nest('head',      'base/head')
            ->nest('header',    'base/header')
            ->nest('content',   'base/content')
            ->with( 'og',       self::$og )
            ->nest('footer',    'base/footer')
            ->nest('base_js',   'base/js')
            ->render();
        die;
    }


    protected static function output(){
        switch(count(self::$args)){
            // year/month/day/article   -> one article
            case 1  : return self::get_gallery_items(); break;
            default : return self::get_gallery_list();  break;
        }
    }


    protected static function get_gallery_items()
    {
        $slug     = self::$args[0];
        $language = Scotty::get_language();

            // get the gallery id by slug
        $gallery = Gallerymeta::where_language_id($language)->where_slug($slug)->first();
        $photos  = Media::get_all_images('galleries', $gallery->gallery_id);

        return Beam::make( 'gallery/items', array('photos' =>  $photos, 'module_url' => Scotty::$module_url ) );
    }


    protected static function get_gallery_list()
    {
        $language     = Scotty::get_language();

        $base_query = Gallery::with(array('meta' => function($query) use($language){
            $query->where_language_id( $language )
                  ->where_public(1)
                  ->first();
        }))
        ->where_deleted(0);

        $galleries = $base_query->get();

        return Beam::make('gallery/list', array('galleries' =>  $galleries, 'module_url' => Scotty::$module_url ) );
    }


    public static function shortcode( $args )
    {
        self::$args = array();

        if( !empty($args) )
        {
            Scotty::$module_url= Tools::get_page_url_by_module( 'galleries' );

            if( array_key_exists('random', $args) )
            {
                return self::shortcode_random( $args['random'] );
            }elseif(array_key_exists('categories', $args)){
                return self::shortcode_categories( $args['categories'] );

            }
        }

        return '';
    }


    protected static function shortcode_random( $random )
    {
        $image    = $photos  = Media::get_random_image( 'galleries' );
        $language = Scotty::get_language();

        if( !empty( $image) )
        {
            // generate path to gallery
            $gallery = Gallerymeta::where_gallery_id( $image['item_id'] )->where_language_id($language)->where_public(1)->first();

            return '<a class="shortcode-gallery-random-link" href="'.Scotty::$module_url.$gallery->slug.'" title="'.$gallery->title.'" ><img src="'.$image['src'].'"/></a>';
        }

        return '';
    }


    protected static function shortcode_categories( $random )
    {
        $language     = Scotty::get_language();

        $base_query = Gallery::with(array('meta' => function($query) use($language){
            $query->where_language_id( $language )
                  ->where_public(1)
                  ->first();
        }));

        $galleries = $base_query->get();

        return Beam::make('gallery/shortcode_categories', array('galleries' =>  $galleries, 'module_url' => Scotty::$module_url ) );
    }


    public static function navigation( $id = 0, $language = '' )
    {
        $language = ('' == $language ? Scotty::get_language() : $language );

        $data  = array();

        if(0 < $id ){
            $items = Gallery::where_id( $id )->where_deleted(0)->get();
        }
        else
        {
            $items = Gallery::where_deleted(0)->get();
        }

        if( null != $items )
        {
            foreach( $items AS $item )
            {
                $meta = $item->meta()->where_language_id( $language )->where_public(1)->first();

                if( null != $meta )
                {
                    $data[] = array(
                        'slug'  => $meta->slug,
                        'title' => $meta->title
                    );
                }
            }

        }

        return $data;
    }
}