<?php

class Frontend_Locations_Controller extends Base{
    private static $args = array();

    public static function dynamic(){
        self::$args = func_get_args();

        return self::output();
    }

    protected static function output(){
        switch(count(self::$args)){
            default : return self::get_locations(); break;
        }
    }

    protected static function get_locations()
    {
        $locations = Locationmeta::where_language_id( Scotty::get_language() )->get();

        $markers = array();

        foreach($locations AS $loc)
        {
            $markers[] = array(
                'lat'       => $loc->location->latitude,
                'lng'       => $loc->location->longitude,
                'title'     => $loc->title,
                'content'   => $loc->content,
            );
        }

        return Beam::make('locations/map', array('markers' =>  json_encode($markers)) );
    }
}