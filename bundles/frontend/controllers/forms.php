<?php

class Frontend_Forms_Controller extends Base{
    protected static $args = array();

    public function __construct( $module_params )
    {
        static::$args = $module_params;

        return $this;
    }

    public function build(){
        Beam::$module_content = static::output();

        static::before_render();

        return Beam::make('')
            ->nest('head',      'base/head')
            ->nest('header',    'base/header')
            ->nest('content',   'base/content')
            ->with( 'og',       static::$og )
            ->nest('footer',    'base/footer')
            ->nest('base_js',   'base/js')
            ->render();
    }

//    public static function dynamic()
//    {
//        self::$args = func_get_args();
//        return self::output();
//    }


    public static function shortcode( $args )
    {
        static::$args = array();

        if( !empty($args) )
        {
            if( array_key_exists('id', $args) )
            {
                Scotty::$module_url = Tools::get_page_url_by_module( 'forms|'.$args['id'] );
                Scotty::$module_id  = $args['id'];

                return static::shortcode_form();
            }
        }

        return '';
    }


    protected static function shortcode_form()
    {
        $translations   = \Config::get('frontend::translations.forms');

        $input = ( Input::get() ) ? Input::get() : null;
        $form = static::get_form($input, null, $translations, Scotty::get_language() );

        return Beam::make('forms/shortcode', array('form' => $form) )->render();
    }


    protected static function output(){
        $errors         = null;
        $input          = Input::all();

        $language       = Scotty::get_language();
        $translations   = Config::get('frontend::translations.forms');


        if( 0 < count(Scotty::$module_params) )
        {
            if( static::$args[0] == $translations['thanks'][ $language ] )
            {
                // get thank you from the form
                $form = Formmeta::where_form_id( Scotty::$module_id )->where_language_id( $language )->first();

                return Beam::make('forms/thanks' )->with( 'form', $form );
            }
        }

        return static::get_form($input, $errors, $translations, $language);
    }


    protected static function get_form( $input, $errors, $translations, $language ){
        // get the form
        $formmeta = Formmeta::where_form_id( Scotty::$module_id )
            ->where_language_id( $language )
            ->first();                                                                                    

        if(null != $formmeta){
            if(0 < count($input) ){
                $data = json_decode($formmeta->form_data, true);
                $rules = array();
                foreach( $data AS $field )
                {
                    if( $field['name_tag'] != '' )
                    {
                        $name = strtolower( str_replace(' ', '_', $field['name_tag']) );
                    }
                    else
                    {
                        $name = strtolower( str_replace(' ', '_', $field['values']) );
                    }

                    if( 'true' == $field['required'] )
                    {
                        $rules[ $name ] = 'required';
                    }else{
                        //$rules[ strtolower($field['values']) ] = '';
                    }

                    if( array_key_exists('validation', $field) && $field['validation'] != '' )
                    {
                        if( isset( $rules[ $name ] ) )
                        {
                            $rules[ $name ].= '|'.$field['validation'];
                        }else{
                            $rules[ $name ] =  $field['validation'];
                        }
                    }
                }

                $validation = Validator::make($input, $rules);

                if( $validation->fails() ){
                    $errors = $validation->errors;
                }else{
                    static::sendEmail($formmeta, $input);

                    $log_data = new logdata;
                    $log_data->module       = 'Forms';
                    $log_data->item_id      = Scotty::$module_id;
                    $log_data->language_id  = $language;
                    $log_data->ip           = Request::ip();
                    $log_data->form_data    = serialize($input);
                    $log_data->save();

                    $form_action = Scotty::$module_url;
                    // # Override the $form_action when is module is a micro -> post to itselfs
                    // # Or when it is a page (without a module), ShortCode is used
                    // if( strstr(Beam::$page->relationships['page']->module_id, 'micros') || Beam::$page->relationships['page']->module_id == 0)
                    // {
                    //     $form_action = URI::current() .'/';
                    // }

                    echo Redirect::to( $form_action.$translations['thanks'][ $language ].'/' )->send();
                    exit;
                }
            }

            $form_structure['form_structure'] = $formmeta->form_data;
            
            $formDb = Formitem::find( Scotty::$module_id );
            $form_css = array(
                'form_id'    => $formDb->form_id,
                'form_class' => $formDb->form_class,
            );

            $form            = new Formbuilder($form_structure, $form_css);
            Formbuilder::$db = $formDb;

            $form_action = Scotty::$module_url;
            # Override the $form_action when is module is a micro -> post to itselfs
            # Or when it is a page (without a module), ShortCode is used
            if( strstr(Beam::$page->relationships['page']->module_id, 'micros') || Beam::$page->relationships['page']->module_id == 0)
            {
                $form_action = URI::full();
            }

            return $form->generate_html( $form_action, $errors );
        }
    }


    protected static function sendEmail( $form, $data, $view = '' )
    {
        $mailer = IoC::resolve('phpmailer');

        try {
            $mailer->isMail();
            $mailer->ishtml(true);
            $mailer->AddAddress( $form->mail_to );

            $mailer->From     = 'no-reply@tool-box.nl';
            $mailer->FromName = 'Toolbox Mailer';

            if( is_array($data) && array_key_exists('email', $data) ){
                $mailer->addReplyTo( $data['email'], $data['email'] );
            }

            if( '' != $form->mail_cc )
            {
                $mailer->addCC( $form->mail_cc );
            }

            if( '' != $form->mail_bcc )
            {
                $mailer->addBCC( $form->mail_bcc );
            }

            $mailer->Subject  = $form->page_title;
            $mailer->Body     = $view;

            $mailer->Send();
        } catch (Exception $e) {
            echo 'Message was not sent.';
            echo 'Mailer error: ' . $e->getMessage();
        }
    }
}