<?php

class Frontend_Sliders_Controller extends Base{

    protected static $args = array();

    public static function before_render()
    {

    }

    public static function shortcode( $args = array() )
    {
        static::$args = array();

        if( !empty($args) )
        {
            $slider = $args[0];

            // check if the revslider folder is in place
            if( !is_dir( path('app') . DS . 'revslider') )
            {
                return 'revolution slider map toevoegen in de application folder.';
            }

            // include the helper
            require path('app') . DS . 'revslider'. DS .'models'. DS . 'helper.php';

            // copy the js files and needed files to the public folder.
            if( !is_dir( path('public') . DS . 'rs-plugin') )
            {
            	if (file_exists( path('app') . DS . 'revslider'. DS .'rs-plugin'  )){
                	recurse_copy( path('app') . DS . 'revslider'. DS .'rs-plugin', path('public') . DS . 'rs-plugin');
            	}
            }

            // Configure the autoloader
            define('C5_EXECUTE', 'TRUE');
            define('REVROOT', dirname(__FILE__) );
            define('DIR_REL', path('public') );

            //include frameword files
            require path('app') . DS . 'revslider'. DS .'models'. DS . 'include_framework.php';

            //include bases
            require path('app') . DS . 'revslider'. DS .'models'. DS . 'base.class.php';
            require path('app') . DS . 'revslider'. DS .'models'. DS . 'elements_base.class.php';
            require path('app') . DS . 'revslider'. DS .'models'. DS . 'base_admin.class.php';

            //include product files
            require path('app') . DS . 'revslider'. DS .'models'. DS . 'revslider_settings_product.class.php';
            require path('app') . DS . 'revslider'. DS .'models'. DS . 'revslider_globals.class.php';
            require path('app') . DS . 'revslider'. DS .'models'. DS . 'revslider_operations.class.php';
            require path('app') . DS . 'revslider'. DS .'models'. DS . 'revslider_slider.class.php';
            require path('app') . DS . 'revslider'. DS .'models'. DS . 'revslider_output.class.php';
            require path('app') . DS . 'revslider'. DS .'models'. DS . 'revslider_slide.class.php';


            //@todo - Gosidesign maak Slider weer werkend
            //$output = new RevSliderOutput();
            //$output->putSlider($slider);
        }

        return '';
    }
}