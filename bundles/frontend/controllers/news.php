<?php

class Frontend_News_Controller extends Base{
    private static $args = array();

    public function __construct( $module_params ){
        self::$args = $module_params;

        return $this;
    }


    public function build(){
        Beam::$module_content = self::output()->render();

        static::before_render();

        return Beam::make('')
            ->nest('content',       'base/content')
            ->nest('head',          'base/head')
            ->nest('header',        'base/header')
            ->with( 'og',           self::$og )
            ->nest('footer',        'base/footer')
            ->nest('base_js',       'base/js')
            ->render();
    }


//    public static function dynamic(){
//        self::$args = func_get_args();
//
//        return self::output();
//    }
    public static function shortcode( $args )
    {
        self::$args = array();

        if( !empty($args) )
        {
            if( array_key_exists('latest', $args) )
            {
                Scotty::$module_url= Tools::get_page_url_by_module( 'news' );
                return static::latest( $args['latest'] );
            }
        }

        return '';
    }




    protected static function output()
    {
        self::$args = ( empty(self::$args) ? Scotty::$module_params : self::$args );

        switch(count(self::$args)){
            // year/month/day/article   -> one article
            case 4  : return static::get_news_item(); break;

            // year/month/day/          -> articles for one day
            // year/month/              -> articles for one month
            // category/category-name   -> articles for category
            // year/                    -> articles for one year
            default : return static::get_news_list(); break;
        }
    }


    protected static function get_news_item()
    {
        self::$args = ( empty(self::$args) ? Scotty::$module_params : self::$args );
        $slug       = self::$args[3];

        unset(self::$args[3]);
        $language = Scotty::get_language();

        $article = Newsitemmeta::where_language_id( $language )->where_slug( $slug )->join('news', 'news.id', '=', 'news_meta.news_id' )->first();

        $og = array(
             'og:title'         => Scotty::$settings['site_title'].' - '.$article->title
            ,'og:site_name'     => Scotty::$settings['site_title']
            ,'og:type'          => 'website'
            ,'og:description'   => strip_tags($article->introduction)
            ,'og:url'           => Scotty::$module_url.Newsitem::path( $article->news_id )
        );

        static::$title_extended = ' - '.$article->title;

        $img     = Media::get_first_image('news', $article->id);
        $image   = '';
        if(!empty($img)){
            $og['og:image'] = $image = $img['cropped'];
        }

        static::set_og( $og );

        return Beam::make('news/item', array('article' =>  $article, 'module_url' => Scotty::$module_url, 'og' => self::$og, 'image' => $image ));
    }


    protected static function get_news_list()
    {
        $translations = Config::get('frontend::translations.news');
        $language     = Scotty::get_language();

        $base_query = Newsitem::with(array('meta' => function($query) use($language){
            $query->where_language_id( $language )
                  ->where_public(1)
                  ->first();
        }));

        // detect category
        if( isset(self::$args[0]) && isset(self::$args[1]) && $translations['category'][ $language ] == self::$args[0] )
        {
            $category_id = 0;

            // get all archived items
            $articles = $base_query->where_news_category_id( $category_id )->order_by('date_from', 'DESC')->get();
        }
        elseif( isset(self::$args[0]) && $translations['archive'][ $language ] == self::$args[0] )
        {
            // get all archived items
            $articles = $base_query->where_archived( '1' )->order_by('date_from', 'DESC')->get();
        }
        elseif( isset(self::$args[0]) && $translations['rss']['*'] == self::$args[0] )
        {
            // get all archived items
            $articles = $base_query->where_deleted('0')->order_by('date_from', 'DESC')->get();

            // build the RSS, kill the APP
            $content = Beam::make('news/rss', array('articles' =>  $articles, 'module_url' => Scotty::$module_url ) );

            echo \Response::make($content, 200, array('Content-Type' => 'text/xml') )->render();
            exit;
        }
        else
        {
            // $articles = $base_query->by_date( self::$args )->where_deleted('0')->order_by('date_from', 'DESC')->get();
            $articles = $base_query->where_deleted('0')->order_by('date_from', 'DESC')->get();
        }            

        return Beam::make('news/list', array('articles' =>  $articles, 'module_url' => Scotty::$module_url ) );
    }


    protected static function latest( $limit = 5 )
    {
        $translations = Config::get('frontend::translations.news');
        $language     = Scotty::get_language();

        $articles = Newsitem::with(array('meta' => function($query) use($language){
            $query->where_language_id( $language )->where_public(1)->first();
        }))->where_deleted( 0 )->order_by('date_from', 'DESC')->join('news_meta', 'news_meta.news_id', '=', 'news.id' )->where('news_meta.public','=', '1')->where('news_meta.language_id','=', $language )->take( $limit )->get();

        return Beam::make('news/shortcode_actueel', array('articles' =>  $articles, 'module_url' => Scotty::$module_url, 'translations' => $translations, 'language' => $language ) );
    }

}