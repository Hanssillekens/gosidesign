<?php

class Frontend_Base_Controller extends Controller
{
    public static $og               = array();
    public static $title_extended   = '';


    public function __construct()
    {
        parent::__construct();
    }


    public static function set_og( $og ){
        foreach($og AS $key => $value){
            Beam::$og[] = '<meta property="'.$key.'" content="'.$value.'" />' . PHP_EOL;
        }
    }

    public static function before_render(){ }



    public function __call($method, $parameters)
    {
        return '';
    }
}