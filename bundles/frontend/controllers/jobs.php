<?php

class Frontend_Jobs_Controller extends Base{
    private static $args = array();


    public function build(){
        Beam::$module_content = self::output()->render();

        static::before_render();

        return Beam::make('')
            ->nest('content',       'base/content')
            ->nest('head',          'base/head')
            ->nest('header',        'base/header')
            ->with( 'og',           self::$og )
            ->nest('footer',        'base/footer')
            ->nest('base_js',       'base/js')
            ->render();
    }

    protected static function output()
    {
        self::$args = ( empty(self::$args) ? Scotty::$module_params : self::$args );

        switch(count(self::$args)){
            // year/month/day/article   -> one article
            case 3  :
            case 2  : return self::get_jobs_apply(); break;
            case 1  : return self::get_jobs_item(); break;
            default : return self::get_jobs_list(); break;
        }
    }


    protected static function get_jobs_list()
    {
        $translations = Config::get('frontend::translations.news');
        $language     = Scotty::get_language();

        $base_query = Job::with(array('meta' => function($query) use($language){
            $query->where_language_id( $language )->where_public(1)->first();
        }));

        $jobs = $base_query->get();

        return Beam::make('jobs/list', array('jobs' =>  $jobs, 'module_url' => Scotty::$module_url ) );
    }


    protected static function get_jobs_item()
    {
        Beam::$disable_page_content = true;

        self::$args = ( empty(self::$args) ? Scotty::$module_params : self::$args );
        $slug       = self::$args[0];

        unset(self::$args[0]);
        $language = Scotty::get_language();

        $jobs = Jobmeta::where_language_id( $language )->where_slug( $slug )->join('jobs', 'jobs.id', '=', 'jobs_meta.job_id' )->first();

        self::$title_extended = ' - '.$jobs->title;

        return Beam::make( 'jobs/item', array('job' =>  $jobs ) );
    }


    protected static function get_jobs_apply()
    {
        Beam::$disable_page_content = true;
        $translations   = \Config::get('frontend::translations.forms');
        $language       = Scotty::get_language();

        self::$args = ( empty(self::$args) ? Scotty::$module_params : self::$args );
        $slug       = self::$args[0];


        $job = Jobmeta::where_language_id( Scotty::get_language() )->where_slug( $slug )->join('jobs', 'jobs.id', '=', 'jobs_meta.job_id' )->first();

        if( 3 == count(self::$args) )
        {
            if( self::$args[2] == $translations['thanks'][ $language ] )
            {
                // get thank you from the form
                $form = Formmeta::where_form_id( $job->form_id )->where_language_id($language)->first();

                return Beam::make( 'jobs/thanks')->with( 'form', $form );
            }
        }
        else
        {
            self::$title_extended = ' - '.$job->title;

            $form = self::get_form( $job->form_id, $slug );

            return Beam::make( 'jobs/apply', array('job' =>  $job, 'applyform' => $form ) );
        }
    }


    protected static function get_form( $form_id, $slug )
    {
        $errors         = null;

        $input          = Input::all();
        $translations   = \Config::get('frontend::translations');
        $language       = Scotty::get_language();

        // get the form
        $formmeta = Formmeta::where_form_id( $form_id )
            ->where_language_id( $language )
            ->first();

        if(null != $formmeta){
            if(0 < count($input) ){
                $rules = array();

                // check for file
                $files = Input::file();
                if( !empty( $files ) ){
                    foreach($files AS $inputname => $file_data){
                        $rules[ strtolower( str_replace(' ', '_', $inputname)) ] = 'mimes:doc,pdf';
                    }
                }

                $data = json_decode($formmeta->form_data, true);

                foreach( $data AS $field )
                {
                    if( 'true' == $field['required'] )
                    {
                        $rules[ strtolower( str_replace(' ', '_', $field['values'])) ] = 'required';
                    }else{
                        //$rules[ strtolower($field['values']) ] = '';
                    }
                }

                $validation = Validator::make($input, $rules);

                if( $validation->fails() )
                {
                    $errors = $validation->$errors;
                }else
                {
                    if( !empty($files) )
                    {
                        foreach($files AS $inputname => $file_data)
                        {
                            $file_type = explode('/', $file_data['type'] );
                            $file_name = Str::random(4).'.'.Str::random(4).'.'.$file_type[1];

                            Input::upload( $inputname, path('public').'uploads/jobsapplications/', $file_name );
                            $input[ $inputname ] = rtrim( URL::base(), '/').'/uploads/jobsapplications/' . $file_name;
                        }
                    }

                    $log_data = new logdata;
                    $log_data->module       = 'Forms';
                    $log_data->item_id      = $form_id;
                    $log_data->language_id  = $language;
                    $log_data->ip           = Request::ip();
                    $log_data->form_data    = serialize($input);
                    $log_data->save();

                    echo Redirect::to( Scotty::$module_url.$slug.'/'.$translations['jobs']['apply'][ $language ].'/'.$translations['forms']['thanks'][ $language ] )->send();
                    exit;
                }
            }

            $form_structure['form_structure'] = $formmeta->form_data;
            $form = new Formbuilder($form_structure);

            return $form->generate_html( Scotty::$module_url.$slug.'/'.$translations['jobs']['apply'][ $language ], $errors );
        }
    }


}