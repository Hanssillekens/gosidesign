<?php

class Frontend_Search_Controller extends Base{

    public static $restfulController = true;


    public function __construct()
    {

    }


    public static function shortcode()
    {
        $url = Tools::get_page_url_by_module('search');

        return Beam::make('search/shortcode')->with( 'url', $url );
    }


    public function get_index(){
        if( Input::has('term') )
        {
            return static::search_results();
        }

    }



    public static function search_results()
    {
        $url     = Tools::get_page_url_by_module('search');
        $term    = Input::get('term');
        $modules = Scotty::$settings['module_search_filters'];
        $results = array();

        if( null != $modules )
        {
            $modules = json_decode($modules, true);

            foreach( $modules AS $module => $status )
            {
                $method = 'search_module_'.$module;

                if( method_exists(__CLASS__, $method) )
                {
                    $results[$module] = static::$method( $term );
                }
            }

        }

        return Beam::make('search/results')->with( 'url', $url )->with( 'results', $results );
    }



    public static function search_module_news( $term )
    {
        $sql   = "SELECT * FROM news_meta WHERE language_id = ? AND ( `introduction` LIKE '%$term%' OR
                                                                      `content`      LIKE '%$term%' OR
                                                                      `title`        LIKE '%$term%' OR
                                                                      `sub_title`    LIKE '%$term%' ) ";

        $items = DB::query( $sql, array(Scotty::get_language()) );
        $module_url = Tools::get_page_url_by_module('news');

        $data = array();
        if( null != $items )
        {
            foreach($items AS $item)
            {
                $data[] = array(
                     'title' => $item->title
                    ,'text'  => $item->introduction
                    ,'url'   => $module_url.''.Newsitem::path($item->news_id)
                );
            }
        }

        return $data;
    }


    public static function search_module_faqs( $term )
    {
        $sql   = "SELECT * FROM faqs_meta WHERE language_id = ? AND ( `question` LIKE '%$term%' OR
                                                                      `answer`   LIKE '%$term%' ) ";

        $items      = DB::query( $sql, array( Scotty::get_language() ) );
        $module_url = Tools::get_page_url_by_module('faqs');

        $data = array();
        if( null != $items )
        {
            foreach($items AS $item)
            {
                $data[] = array(
                     'title' => strip_tags($item->question)
                    ,'text'  => $item->answer
                    ,'url'   => $module_url.'#'.$item->slug
                );
            }
        }

        return $data;
    }


    public static function search_module_jobs( $term )
    {
        $sql   = "SELECT * FROM jobs_meta WHERE language_id = ? AND ( `title`    LIKE '%$term%' OR
                                                                      `content`  LIKE '%$term%' ) ";

        $items      = DB::query( $sql, array( Scotty::get_language() ) );
        $module_url = Tools::get_page_url_by_module('jobs');

        $data = array();
        if( null != $items )
        {
            foreach($items AS $item)
            {
                $data[] = array(
                     'title' => $item->title
                    ,'text'  => $item->content
                    ,'url'   => $module_url.''.$item->slug
                );
            }
        }

        return $data;
    }


    public static function search_module_catalogs( $term )
    {
        $sql   = "SELECT pm.*
                  FROM products_meta AS pm
                  JOIN products      AS p  ON ( p.id = pm.product_id AND p.deleted = 0 )
                  WHERE pm.language_id = ? AND ( pm.`title`     LIKE '%$term%' OR
                                                                          pm.`introtext` LIKE '%$term%' OR
                                                                          pm.`content`   LIKE '%$term%'    ) ";

        $items      = DB::query( $sql, array( Scotty::get_language() ) );
        $module_url = Tools::get_page_url_by_module('products');

        $data = array();
        if( null != $items )
        {
            foreach($items AS $item)
            {
                $data[] = array(
                     'title' => $item->title
                    ,'text'  => $item->content
                    ,'url'   => Product::path( $item->product_id )
                );
            }
        }

        return $data;
    }
}