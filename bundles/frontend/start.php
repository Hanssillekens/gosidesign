<?php
require_once( __DIR__ .'/helpers/frontend.php');

Autoloader::map(array(
    'Frontend_Faqs_Controller'        => Bundle::path('frontend').'controllers/faqs.php',
    'Frontend_Forms_Controller'       => Bundle::path('frontend').'controllers/forms.php',
    'Frontend_Galleries_Controller'   => Bundle::path('frontend').'controllers/galleries.php',
    'Frontend_Jobs_Controller'        => Bundle::path('frontend').'controllers/jobs.php',
    'Frontend_Locations_Controller'   => Bundle::path('frontend').'controllers/locations.php',
    'Frontend_Micros_Controller'      => Bundle::path('frontend').'controllers/micros.php',
    'Frontend_News_Controller'        => Bundle::path('frontend').'controllers/news.php',
    'Frontend_Partners_Controller'    => Bundle::path('frontend').'controllers/partners.php',
    'Frontend_Products_Controller'    => Bundle::path('frontend').'controllers/products.php',
    'Frontend_Portal_Controller'      => Bundle::path('frontend').'controllers/portal.php',
    'Frontend_Search_Controller'      => Bundle::path('frontend').'controllers/search.php',
    'Frontend_Newsletter_Controller'  => Bundle::path('frontend').'controllers/newsletter.php',
    'Frontend_Sliders_Controller'     => Bundle::path('frontend').'controllers/sliders.php',

    'Frontend_Beam_Library'           => Bundle::path('frontend').'libraries/beam.php',
    'Frontend_Formbuilder_Library'    => Bundle::path('frontend').'libraries/formbuilder.php',
    'Frontend_Menu_Library'           => Bundle::path('frontend').'libraries/menu.php',
    'Frontend_Scotty_Library'         => Bundle::path('frontend').'libraries/scotty.php',
    'Frontend_Shortcode_Library'      => Bundle::path('frontend').'libraries/shortcode.php',
    'Frontend_Tools_Library'          => Bundle::path('frontend').'libraries/tools.php',
    'Frontend_Media_Library'          => Bundle::path('frontend').'libraries/media.php',
    'Frontend_NewsletterView_Library' => Bundle::path('frontend').'libraries/newsletterview.php',
    'Frontend_Mail_Library'           => Bundle::path('frontend').'libraries/mail.php',

    'Helper'                          => Bundle::path('frontend').'libraries/helper.php',
    'Mobile_Detect'                   => Bundle::path('frontend').'libraries/mobiledetect.php',
    'Emogrifier'                      => Bundle::path('frontend').'libraries/emogrifier.php',
));


Autoloader::directories(array(
    Bundle::path('frontend').'models',
));


// Prepare Language shizzle
$languages = Language::all();
$lang = array();
$lang_preview = array();
$default_language = '';

foreach( $languages AS $language )
{
    if( '1' == $language->status )
    {
        $lang[ $language->id ] = $language->native;
        // Set the default language
        if($language->default == 1)
        {
            $default_language = $language->id;
            $locale = ( $language->locale == '' ) ? 'nl_NL' : $language->locale;
            setlocale(LC_TIME, $locale);
        }
    }

    $lang_preview[ $language->id ] = $language->native;
}

Config::set('frontend::runtime.languages', $lang );
Config::set('frontend::runtime.languages_preview', $lang_preview );
Config::set('frontend::runtime.language.default', $default_language);

Asset::container('frontend')
    ->bundle('frontend')
    ->add('frontend-base', 'css/base-cleaned.css')
    ->add('frontend-style', 'css/style-cleaned.css');