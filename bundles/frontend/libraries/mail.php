<?php

class Frontend_Mail_Library extends Base
{
    /**
     * Send an e-mail, data comes from the Log
     * @param  integer $logdata_id The Log_id
     * @param  string  $language   Language
     * @param  array   $options    Custom options
     *   - send_to :: emailaddress from receiver
     *   - send_name :: name from receiver
     *   - subject :: mail subject
     *   - emailTemplate :: custom e-mailatemplate -> Full path
     *   - send_from_email :: emailaddress from sender
     *   - send_from_name :: name from sender
     * @param  boolean  $json      Return with json
     * @return [type]              [description]
     */
    public static function sendMail($logdata_id = 0, $language = '', $custom_options = array(), $json = false)
    {
        # All Site Settings
        $default_settings = \Setting::lists('value', 'name');

        # Default options
        $default_options = array(
            'send_to'           => '',
            'send_name'         => '',
            'subject'           => '',
            'emailTemplate'     => '',
            'send_from_email'   => $default_settings['site_default_emailaddress_sender'],
            'send_from_name'    => $default_settings['site_title'],
        );
        # Merge the options
        $options = array_merge($default_options, $custom_options);            

        # Set the language
        $language = ( $language == '' ) ? Scotty::get_language() : $language;
        
        # Check language
        if($language == '')
        {
            return ($json) ? \Response::JSON(array('status' => 0, 'message' => 'No language avaible')) : 'No language available';
        }

        # Get the Logdata
        $logdata = \Logdata::find($logdata_id);

        # Check Logdata
        if( is_null($logdata) )
        {
            return ($json) ? \Response::JSON(array('status' => 0, 'message' => 'No logdata avaible')) : 'No logdata available';
        }
        $logdata->form_data = unserialize($logdata->form_data);

        # Get the Form
        $form = \Formitem::with(array('meta' => function($query) use ($language)
                {
                    $query->where_language_id( $language );
                }))
            ->where_id($logdata->item_id)
            ->first();

        # Check Form
        if( is_null($form) )
        {
            return ($json) ? \Response::JSON(array('status' => 0, 'message' => 'No form avaible')) : 'No form available';
        }

        # Load PHPMailer
        \Bundle::start('phpmailer');
        $mailer = \IoC::resolve('phpmailer');

        # Email Template
        $emailTemplate  = ( $options['emailTemplate'] == '' ) ? 'frontend::mail.forms.' .$form->type : $options['emailTemplate'];            

        # Check Form
        if( is_null($emailTemplate) )
        {
            return ($json) ? \Response::JSON(array('status' => 0, 'message' => 'No email-template avaible')) : 'No email-template available';
        }

        # Default sender
        $send_to = ( $options['send_to'] == '' ) ? $default_settings['site_default_emailaddress_sender'] : $options['send_to'];       

        if( isset($form->relationships['meta'][0]) )
        {
            $senddata = $form->relationships['meta'][0]->to_array();
            
            # Send to
            $send_to = ( $senddata['mail_to'] != '' ) ? $senddata['mail_to'] : $send_to;
            
            # Send to CC
            if( $senddata['mail_cc'] != '' )
            {
                $cc_array = explode(',', $senddata['mail_cc']);
                foreach ($cc_array as $key => $value)
                {
                    $mailer->AddCC($value, '');
                }
            }
            
            # Send to BCC
            if( $senddata['mail_bcc'] != '' )
            {
                $bcc_array = explode(',', $senddata['mail_bcc']);
                foreach ($bcc_array as $key => $value)
                {
                    $mailer->AddBCC($value, '');
                }
            }

            # Subject
            $tmp_subject = ( $senddata['page_title'] == '' ) ? false : $senddata['page_title'];
            $subject = ( $options['subject'] == '' ) ? $tmp_subject : $options['subject'];          
        }            

        $subject = ( $subject == false || $subject == '' ) ? '' : $subject;

        # Check if view excist            
        if( !\View::exists($emailTemplate) )
        {
            return ($json) ? \Response::JSON(array('status' => 0, 'message' => 'Email-template doesn\'t exists')) : 'Email-template doesn\'t exists';
        }            

        # Render template
        $html = \View::make($emailTemplate, array('data' => $logdata->form_data))->render();            

        # Set up PHPMailer
        $mailer->Mailer  = 'mail';
        $mailer->Subject = $subject;
        $mailer->Body    = $html;
        $mailer->IsHTML(true);

        $mailer->SetFrom($options['send_from_email'], $options['send_from_name']);
        $mailer->AddAddress($send_to, $options['send_name']);
        
        if( $mailer->Send() )
        {
            return ($json) ? \Response::JSON(array('status' => 1, 'message' => 'Mail sended to: ' .$send_to)) : 'Mail sended to: ' .$send_to;
        }
        else
        {
            return ($json) ? \Response::JSON(array('status' => 0, 'message' => 'Mail not sended')) : 'Mail not sended';
        }
    }

}