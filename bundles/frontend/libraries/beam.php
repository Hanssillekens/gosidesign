<?php

/**
 * Class extends the default View Class from laravel
 * Basically we check if the application view exists.
 * If it does exist use that one, OR, fallback to the bundle view.
 *
 * @author Sebastiano Bellinzis
 */
class Frontend_Beam_Library extends View{

    public static $headers        = null;
    public static $og             = array();
    public static $backgrounds    = null;
    public static $blocks         = null;
    public static $page           = null;
    public static $template       = 'default';
    public static $menu           = null;
    public static $menu_sub       = null;
    public static $crumbs         = null;
    public static $module_content = null;

    public static $title          = null;

    public static $disable_page_content = false;



    public static function make( $view, $data = array() )
    {
        $view = ( '' == $view ? self::use_template() : $view );

        // Check for tablet OR mobile Templates
        if( Scotty::$detect->isMobile() && !Scotty::$detect->isTablet() )
        {
            if( parent::exists( $view.'_mobile') )
            {
                $view = $view.'_mobile';
            }
        }
        elseif( Scotty::$detect->isTablet() )
        {
            if( parent::exists( $view.'_tablet') )
            {
                $view = $view.'_tablet';
            }
        }

        # Check if it is a Qiwi-site
        if( Config::get('qiwi::is_qiwi') == 1 )
        {
            $qiwi_template = Config::get('qiwi::settings');                
            return parent::make('frontend::qiwi.' .$qiwi_template->template .'.' .$view, $data);
        }
        else
        {
            if( parent::exists($view) )
            {
                return parent::make($view, $data );
            }

            return parent::make('frontend::'.$view, $data );
        }

    }


    public function nest($key, $view, $data = array())
    {
        return $this->with($key, self::make($view, $data));
    }


    public static function page_title( $val = '' ){
        if( '' == $val )
        {
            return (null != self::$title ? self::$title.' - ' : '' ).Scotty::$settings['site_title'];
        }

        if(null != self::$title)
        {
            self::$title.= ' - '.$val;
        }
        else
        {
            self::$title = $val;
        }
    }


    public function menu( $class = 'dir', $id="navigation" )
    {
        $out = '<ul id="'.$id.'" class="'.$class.'">';
        $out.= Beam::$menu;
        $out.= '</ul>';

        return $out;
    }


    public function language_menu( $class = 'language_selector',  $with_flags = false )
    {
        $language_current = URI::$segments[0];
        $language_all     = Scotty::get_languages();

        if(array_key_exists($language_current, $language_all)) {
            $value = $language_all[$language_current];
            unset($language_all[$language_current]);

            $language_all = array($language_current => $value) + $language_all;
        }

        $menu = '<ul class="'.$class.'">';
        foreach( $language_all AS $language => $language_name )
        {
            $li_class  = ($language_current == $language ? 'active' : '' );
            $link_name = (true == $with_flags ? '<img src="'.URL::to_asset('images/flags/'.$language.'.png').'" alt="'.$language_name.'"/>' : $language );
            $menu.= '<li class="'.$li_class.'"><a href="'.Scotty::url($language).'/">'.$link_name.'</a></li>';
        }

        $menu.= '</ul>';

        return $menu;
    }


    public function sub_menu_title()
    {
        if( array_key_exists( Scotty::$level-1, Menu::$crumbs ) )
        {
            return Menu::$crumbs[ Scotty::$level-1 ]['name'];
        }elseif( array_key_exists( Scotty::$level, Menu::$crumbs ) ){
            return Menu::$crumbs[ Scotty::$level ]['name'];
        }

        return '';
    }


    public static function use_template( $template = '' )
    {
        $template = ( '' == $template ? self::$template : $template );            

        # First check on application-level
        if( parent::exists('templates/'.$template) )
        {
            return 'templates/'.$template;
        }

        # No view found, check on frontend-bundle-level
        if( parent::exists('frontend::templates/'.$template) )
        {
            return 'templates/'.$template;
        }

        # Check if it is a Qiwi-site
        if( Config::get('qiwi::is_qiwi') == 1 )
        {
            # No view found, check on Qiwi-level
            $qiwi_template = Config::get('qiwi::settings')->template;
            if( parent::exists('frontend::qiwi.' .$qiwi_template .'.templates/'.$template) )
            {
                return 'templates/'.$template;
            }
        }

        return 'templates/default';
    }


    public static function pages_by_menu( $id = '', $class = '', $item_id = '', $separator = '' )
    {
        $page_ids = Menuspage::select('page_id')->where_menu_id($id)->order_by('sort')->lists('page_id' );

        return static::build_pages_structure( $page_ids, $class, $item_id, $separator );
    }


    public static function pages_by_menu_columns( $id = '', $class = '', $item_id = '', $separator = '' )
    {
        $columns_ids = Menuspage::select('column_place')->where_menu_id($id)->order_by('sort')->lists('column_place' );
        $columns_ids = array_values( array_unique($columns_ids) );

        $out = '<div class="menu-columns" '.( '' != $item_id ? 'id="'.$item_id.'"' : '' ).' >';

        foreach( $columns_ids AS $key => $column )
        {
            $page_ids = Menuspage::select('page_id')->where_menu_id($id)->where_column_place($column)->order_by('sort')->lists('page_id' );

            $out.= static::build_pages_structure( $page_ids, 'menu-column-'.($key+1).' '.$class, '', $separator );
        }

        $out.= '</div>';

        return $out;
    }


    protected static function build_pages_structure( $page_ids, $class = '', $item_id = '', $separator = '')
    {
        $menu  = '';

        if( !empty($page_ids) )
        {
            $tot  = count($page_ids);
            $i    = 1;
            $menu.='<ul class="'.$class.'" '.( '' != $item_id ? 'id="'.$item_id.'"' : '' ).' >';

            foreach($page_ids AS $page_id )
            {
                $page = Tools::page_info_by_id($page_id);

                if(!empty($page)){
                    $li_class = '';
                    if($i == 1){
                        $li_class = 'first';
                    }elseif( $i == $tot ){
                        $li_class = 'last';
                    }

                    if( Menu::$home_page_id == $page['id'] )
                    {
                        $url = Scotty::url().'/';
                    }else{
                        $url = $page['url'];
                    }

                    $menu.='<li class="'.$li_class.( true == $page['active'] ? ' active' : '').'"><a href="'.$url.'" title="'.$page['page_title'].'">'.$page['name'].'</a>';

                    $menu.= Menu::navigation( $page['id'], 1, '', $url );

                    $menu.='</li>';

                    if($separator != '' && $i != $tot ){
                        $menu.='<li class="spacer">'.$separator.'</li>';
                    }

                    if( in_array($page['id'], Scotty::$page_array) ){
                        Menu::$crumbs[] = array( 'url'  => $url
                        ,'name' => $page['name'] );
                    }

                    $i++;
                }
            }

            $menu.='</ul>';
        }

        return $menu;
    }
}