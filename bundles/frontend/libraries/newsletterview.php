<?php

class Frontend_NewsletterView_Library extends \View {

	/**
	 * The name of the view.
	 *
	 * @var string
	 */
	public $view;

	/**
	 * The view data.
	 *
	 * @var array
	 */
	public $data;

	/**
	 * The path to the view on disk.
	 *
	 * @var string
	 */
	public $path;

	/**
	 * All of the shared view data.
	 *
	 * @var array
	 */
	public static $shared = array();

	/**
	 * All of the registered view names.
	 *
	 * @var array
	 */
	public static $names = array();

	/**
	 * The cache content of loaded view files.
	 *
	 * @var array
	 */
	public static $cache = array();

	/**
	 * THe last view to be rendered.
	 *
	 * @var string
	 */
	public static $last;

	/**
	 * The render operations taking place.
	 *
	 * @var int
	 */
	public static $render_count = 0;

	/**
	 * The Laravel view loader event name.
	 *
	 * @var string
	 */
	const loader = 'laravel.view.loader';

	/**
	 * The Laravel view engine event name.
	 *
	 * @var string
	 */
	const engine = 'laravel.view.engine';

	/**
	 * Create a new view instance.
	 *
	 * <code>
	 *		// Create a new view instance
	 *		$view = new View('home.index');
	 *
	 *		// Create a new view instance of a bundle's view
	 *		$view = new View('admin::home.index');
	 *
	 *		// Create a new view instance with bound data
	 *		$view = new View('home.index', array('name' => 'Taylor'));
	 * </code>
	 *
	 * @param  string  $view
	 * @param  array   $data
	 * @return void
	 */
	public function __construct($view, $data = array())
	{
		$this->view = $view;
		$this->data = $data;
	}

	/**
	 * Retrieve the source of the path
	 * @param  [type] $view [description]
	 * @return [type]       [description]
	 */
	public static function html($view)
	{
		list($bundle, $view) = \Bundle::parse($view);

		$view = str_replace('.', '/', $view);

		$path = \Event::until(static::loader, array($bundle, $view));

		$content = file_get_contents($path);

		return htmlspecialchars( $content );		
	}

	/**
	 * Determine if the given view exists.
	 *
	 * @param  string       $view
	 * @param  boolean      $return_path
	 * @return string|bool
	 */
	public static function exists($view, $return_path = false)
	{
		if (starts_with($view, 'name: ') and array_key_exists($name = substr($view, 6), static::$names))
		{
			$view = static::$names[$name];
		}
		
		list($bundle, $view) = Bundle::parse($view);

		$view = str_replace('.', '/', $view);

		// We delegate the determination of view paths to the view loader event
		// so that the developer is free to override and manage the loading
		// of views in any way they see fit for their application.
		$path = Event::until(static::loader, array($bundle, $view));

		if ( ! is_null($path))
		{
			return $return_path ? $path : true;
		}

		return false;
	}

	/**
	 * Get the contents of the view file from disk.
	 *
	 * @return string
	 */
	protected function load()
	{
		// static::$last = array('name' => $this->view, 'path' => $this->path);

		// if (isset(static::$cache[$this->path]))
		// {
		// 	return static::$cache[$this->path];
		// }
		// else
		// {
			return $this->view;
		// }
	}

}