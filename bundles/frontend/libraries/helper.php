<?php

class Helper
{
     /**
     * Translations by keys
     * @param  string $key [description]
     * @return [type]      [description]
     */
    public static function tl($key = '', $striptags = true, $language = '')
    {
        return tl( $key, $striptags, $language );
    } 
}