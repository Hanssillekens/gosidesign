<?php

class Frontend_Tools_Library{


    public  static function path_to_page_by_id( $id, $language = '' ){
        $language = ('' == $language ? Scotty::get_language() : $language );

        $page = Page::with(array('meta' => function($query) use($language){
            $query->where_language_id( $language )->first();
        }))->where_id( $id )->first();

        $path = '';

        if( null != $page && 0 < count($page->relationships['meta'] ) ){

            if(0 != $page->page_id){
                $path = self::get_path_to_page($page->id, '', $language );
            }else{
                $path = $page->relationships['meta'][0]->slug.'/';
            }
        }

        return Scotty::url( $language ).'/'.$path;
    }


    public static function get_path_to_page( $page_id, $path = '', $language = '' ){
        $language = ('' == $language ? Scotty::get_language() : $language );

        if('0' != $page_id ){
            $page     = Pagemeta::with('page')->where_language_id( $language )->where_page_id($page_id)->first();

            if( null != $page ){
                $path = $page->slug.'/'.$path;

                if('0' != $page->page_id ){
                    $path = self::get_path_to_page( $page->relationships['page']->page_id, $path, $language );
                }
            }
        }

        return $path;
    }


    public static function get_page_url_by_module( $module, $language = '' ){
        $language = ('' == $language ? Scotty::get_language() : $language );

        $page = \DB::table('pages_meta')
            ->where('pages_meta.language_id', '=', $language)
            ->join('pages', 'pages_meta.page_id', '=', 'pages.id')
            ->where('pages.module_id', '=', $module)
            ->where('pages.deleted', '=', '0' )
            ->first(array('*', 'pages.id as main_page_id', 'pages.page_id as parent_page_id', 'pages_meta.id as sub_page_id'));

        $path = '';

        if( null != $page )
        {
            $module_slug = $page->slug;

            if(0 != $page->parent_page_id )
            {
                $path = self::get_path_to_page($page->parent_page_id, '', $language ) .$module_slug .'/';
            }
            else
            {
                $path = $page->slug .'/';
            }
        }

        return Scotty::url().'/'.$path;
    }


    public static function internallink( $args )
    {
        if(!empty($args))
        {
            $val = preg_replace("/[^0-9,.]/", "", $args[0]);
            return self::path_to_page_by_id( $val );
        }

        return '';
    }


    public static function internalmedia( $args )
    {
        if(!empty($args))
        {
            $val = preg_replace( "/[^0-9,.]/", "", $args[0] );
            return URL::base().'/serve-media/' .$val;
        }

        return '';
    }


    public static function page_info_by_id( $id, $language = '' )
    {
        $language = ('' == $language ? Scotty::get_language() : $language );

        $page = Page::with(array('meta' => function($query) use($language){
            if( Scotty::get_preview() ){
                $query->where_language_id( $language )->first();
            }
            else{
                $query->where_language_id( $language )->where_public(1)->first();
            }
        }))->where_id( $id )->first();

        $data = array();

        if( null != $page && 0 < count( $page->relationships['meta'] ) ){
            $data = array(
                'name'       => '',
                'page_title' => '',
                'url'        => '',
                'active'     => false
            );

            $data['name']       = $page->relationships['meta'][0]->name;
            $data['page_title'] = $page->relationships['meta'][0]->page_title;

            if(0 != $page->page_id){
                $path = self::get_path_to_page($page->id, '', $language );
            }else{
                $path = $page->relationships['meta'][0]->slug.'/';
            }

            $data['url']    = Scotty::url( $language ).'/'.$path;
            $data['active'] = ( in_array( $page->id, Scotty::$page_array ) ? true : false );
            $data['id']     = $page->id;
        }


        return $data;
    }


    public static function routeplanner( $args = array() )
    {
        return Beam::make('tools/route', $args );
    }


    /**
     * Convert a date or time to a human-readable or MySQL time
     * @param  string $date        Time
     * @param  string $readability Type of readability, normal or for mysql
     * @return string date()       Human-readable / MySQL time
     *
     * @author Kevin Gorjan
     */
    public static function time($date = '', $readability = '')
    {
        if(!strstr($date, '0000'))
        {
            $newdate = ($date === '') ? date('Y-m-d 00:00:00') : $date;
            $newdate = str_replace('/', '-', $newdate);
            $newdate = strtotime($newdate);

            switch ($readability)
            {
                case 'mysql':
                    $readability = 'Y-m-d H:i:s';
                    break;

                case 'none':
                    $readability = 'Y-m-d H:i:s';
                    $newdate = '0';
                    break;

                default:
                    $readability = 'd-m-Y';
                    break;
            }

            return date($readability, $newdate);
        }
        else
        {
            return '---';
        }
    }


    /**
     * Get the website sitemap. Basically it's the same as the menu wrapped inside a div called sitemap.
     *
     * @author Sebastiano Bellinzis
     *
     * @return string
     */
    public static function sitemap(){
        return '<div class="sitemap"><ul class="top-level">'.Menu::navigation().'</ul></div>';
    }


    /**
     * Tool for generating lists of the categories by a specific module.
     * This can also be used to inject into the
     *
     * @param $module
     * @param int $category_id
     * @param string $language
     * @param string $path
     *
     * @author Sebastiano Bellinzis
     *
     * @return string
     */
    public static function category_tree( $module, $category_id = 0, $language = '', $path = '' ){
        $language = ('' == $language ? Scotty::get_language() : $language );
        $path     = ('' == $path ? self::get_page_url_by_module( $module ) : $path);

        $categories = Category::with(array('meta'=> function($query) use($language){
            $query->where_language_id($language)->first();
        }))->where_category_id( $category_id )->where_module_id( $module )->where_deleted(0)->order_by('sort', 'asc')->get();

        $out = '';
        $tot = count($categories);
        if( 0 < $tot ) {
            $out.= "\n";

            $ul_css = ( 0 == $category_id ? 'category-tree' : 'category-tree-child' );
            $out.= '<ul class="'.$ul_css.'" >'.PHP_EOL;

            $i = 1;
            foreach( $categories AS $category ) {
                $class = '';

                if( empty($category->relationships) ){
                    break;
                }

                if( empty($category->relationships['meta']) ){
                    break;
                }

                $meta = $category->relationships['meta'][0];
                $url  = $child_url = $path.$meta->slug.'/';

                if( $category_id == 0 )
                {
                    if( 1 == $tot && 1 == $i  )
                    {
                        $class.= ' last';
                    }
                    else
                    {
                        $class.= ( 1 == $i ? ' first' : ( $i == $tot ? ' last' : '' ) );
                    }
                }

                Menu::$crumbs[] = array( 'url'  => $url, 'name' => $meta->name );

                if( stristr( rtrim(URL::full(),'/').'/', rtrim($url,'/').'/' ) )
                {
                    $class.= ' active';
                }

                $children = self::category_tree( $module, $category->id, $language, $child_url );

                $out.= '<li class="'.$class.'">'.PHP_EOL;
                if( is_object( $meta ) ){
                    $out.= '<a href="'.$url.'" title="'.$meta->title.'">';
                    $out.= $meta->title;
                    $out.= '</a>';
                }
                $out.= $children;
                $out.= "</li>\n";

                $i++;
            }

            $out.="</ul>\n";
        }

        return $out;
    }


    public static function formatBytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        // $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }


    public static function shorten($input, $length = 10, $ellipses = true, $strip_html = true){
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }

        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }

        return $trimmed_text;
    }
}