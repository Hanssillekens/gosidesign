<?php

class Frontend_Media_Library{

    protected static $item_id = null;

    /**
     * Serve an media item.
     *
     * @param $id
     *
     * @author Sebastiano Bellinzis
     *
     * @return Response|null
     */
    public static function serve( $id )
    {
        $image = Medium::find($id);

        if( null != $image )
        {
            $img = $image;
            $image->downloads += 1;
            $image->save();

            switch( $img->type )
            {
                case 'p' : $folder = 'images';    break;
                case 'd' : $folder = 'documents'; break;
                case 'v' : $folder = 'videos';    break;
                default  : $folder = 'images';    break;
            }

            return Response::download( path('public').'uploads/'.$folder.'/'.$img->src, $img->name );
        }

        return null;
    }


    /**
     * @param $module_id
     * @param $item_id
     * @param null $type
     * @param null $language
     *
     * @author Sebastiano Bellinzis
     *
     * @return mixed
     */
    public static function get_all_documents($module_id, $item_id, $type = null, $language = null)
    {
        $query = static::base_query_documents($module_id, $item_id, $type, $language);

        $documents = $query->get();

        return static::to_array( $documents, array(), '/uploads/documents/' );
    }



    /**
     * @param $module_id
     * @param $item_id
     * @param null $type
     * @param null $language
     *
     * @author Sebastiano Bellinzis
     *
     * @return mixed
     */
    public static function get_first_image( $module_id, $item_id, $type = null, $language = null )
    {
        if( stristr($type, '|') )
        {
            $ret = array();
            $types = explode('|', $type);
            foreach($types AS $t){
                $ret = array_merge($ret, static::get_all_images( $module_id, $item_id, $t, $language  ) );
            }

            return $ret;
        }

        static::$item_id = $item_id;

        $query = static::base_query_image($module_id, $item_id, $type, $language);

        $query->order_by( 'sort' );

        $images = $query->take(1)->get();

        $settings = static::getSettingsArray($module_id, $type);
        $data     = static::to_array( $images, $settings );

        return ( empty($data) ? array() : $data[0] );
    }


    /**
     * @param $module_id
     * @param $item_id
     * @param null $type
     * @param null $language
     *
     * @author Sebastiano Bellinzis
     *
     * @return mixed
     */
    public static function get_random_image( $module_id, $item_id = 0, $type = null, $language = null )
    {
        if( stristr($type, '|') )
        {
            $ret = array();
            $types = explode('|', $type);
            foreach($types AS $t){
                $ret = array_merge($ret, static::get_all_images( $module_id, $item_id, $t, $language  ) );
            }

            return $ret;
        }

        static::$item_id = $item_id;

        $query = static::base_query_image($module_id, $item_id, $type, $language);

        $query->order_by( DB::raw('RAND()') );

        $images = $query->take(1)->get();

        $settings = static::getSettingsArray($module_id, $type);
        $data     = static::to_array( $images, $settings );

        return ( empty($data) ? array() : $data[0] );
    }


    /**
     * @param $module_id
     * @param $item_id
     * @param null $type
     * @param null $language
     *
     * @author Sebastiano Bellinzis
     *
     * @return mixed
     */
    public static function get_all_images( $module_id, $item_id, $type = null, $language = null )
    {
        if( stristr($type, '|') )
        {
            $ret = array();
            $types = explode('|', $type);
            foreach($types AS $t){
                $ret = array_merge($ret, static::get_all_images( $module_id, $item_id, $t, $language  ) );
            }

            return $ret;
        }

        static::$item_id = $item_id;

        $query = static::base_query_image($module_id, $item_id, $type, $language);
        $query->order_by( 'sort' );

        $images = $query->get();

        $settings = static::getSettingsArray($module_id, $type);

        return static::to_array( $images, $settings );
    }


    /**
     * The base Image query.
     *
     * @param $module_id
     * @param int $item_id
     * @param null $type
     * @param null $language
     *
     * @author Sebastiano Bellinzis
     *
     * @return mixed
     */
    protected static function base_query_image($module_id, $item_id = 0, $type = null, $language = null)
    {
        $language = ('' == $language ? Scotty::get_language() : $language );

        $query = Medium::with(array('meta'=> function($query) use($language){
            $query->where_language_id($language)->first();
        }))->where_module( $module_id )
            ->where_type( 'p' );

        if( 0 != $item_id )
        {
            $query->where_item_id( $item_id );
        }

        if(null != $type){
            $query->where_type_module( $type );
        }

        return $query;
    }


    /**
     * The base Document query.
     *
     * @param $module_id
     * @param int $item_id
     * @param null $type
     * @param null $language
     *
     * @author Sebastiano Bellinzis
     *
     * @return mixed
     */
    protected static function base_query_documents($module_id, $item_id = 0, $type = null, $language = null)
    {
        $language = ('' == $language ? Scotty::get_language() : $language );

        $query = Medium::with(array('meta'=> function($query) use($language){
            $query->where_language_id($language)->first();
        }))->where_module( $module_id )
            ->where_type( 'd' );

        if( 0 != $item_id )
        {
            $query->where_item_id( $item_id );
        }

        if(null != $type){
            $query->where_type_module( $type );
        }

        return $query;
    }


    /**
     * Convert the object to an array for easier frontend handling
     *
     * @param array $images
     * @param array $settings
     * @param string $includePath
     *
     * @author Sebastiano Bellinzis
     *
     * @return array
     */
    protected static function to_array( $images, $settings = array(), $includePath = '/uploads/images/' )
    {
        $data = array();

        if( !empty($images) )
        {
            foreach($images AS $key => $image )
            {
                if( $image instanceof Medium)
                {
                    if( '' == Config::get('application.media_url') )
                    {
                        $path = rtrim( Config::get('application.url'),'/' );
                    }
                    else{
                        $path = rtrim( Config::get('application.media_url'),'/' );
                    }

                    $path.= $includePath;

                    //check if  x + y + w + h are set.
                    $cropped = '';
                    if( '' !=  $image->x && '' !=  $image->y && '' !=  $image->w && '' !=  $image->h ){
                        $width   = ( 0 < $settings['width']  ? $settings['width']  : $image->w );
                        $height  = ( 0 < $settings['height'] ? $settings['height'] : $image->h );

                        $image->w = ( $width  > $image->w  ? $width  : $image->w );
                        $image->h = ( $height > $image->h  ? $height : $image->h );

                        $cropped = floor($width).'x'.floor($height).'-trim('.floor($image->x).','.floor($image->y).','.floor($image->w).','.floor($image->h).')-';
                    }

                    $src = $image->src;

                    $data[$key]['cropped']  = $path.$cropped.$src;
                    $data[$key]['full']     = $path.$src;
                    $data[$key]['original'] = $src;
                    $data[$key]['path']     = $path;
                    $data[$key]['item_id']  = $image->item_id;
                    $data[$key]['object']   = $image;
                    $data[$key]['class']    = $image->css_class;
                    $data[$key]['title']    = '';
                    $data[$key]['content']  = '';

                    if( !empty( $image->relationships['meta'] ) )
                    {
                        $data[$key]['title']    = $image->relationships['meta'][0]->title;
                        $data[$key]['content']  = $image->relationships['meta'][0]->content;
                    }
                }
            }
        }

        return $data;
    }


    /**
     * Try to retrieve the aspect-ratio in which the image should be loaded.
     *
     * @author Sebastiano Bellinzis
     *
     * @param $module
     * @param null $type
     * @return array
     */
    protected static function getSettingsArray( $module, $type = null )
    {
        if( 'microitems' == $module ){
            // get micro_item parent
            $parent = Microitem::find(static::$item_id);
            $module = 'micros_'.$parent->micro_id;
        }

        $part = Modulesettings::select('value')
            ->where_module( $module )
            ->where_label('aspectratio');

        if( $module == 'pages' ){
            $template = Template::where_slug( Beam::$template )->first();

            if( !is_null( $template ) )
            {
                $part->where_template_id( $template->id );
            }
        }

        if( !is_null( $type ) )
        {
            $part->where_type( $type );
        }

        $ret = $part->first();

        if( !is_null( $ret ) )
        {
            $value = $ret->value;

            if( str_contains($value, '/') )
            {
                list($width, $height) = explode('/', $value );

                return array(
                    'width'  => intval($width),
                    'height' => intval($height),
                );
            }
        }

        return array(
            'width'  => 0,
            'height' => 0,
        );
    }

    /**
     * Render the view for YouTube videos
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public static function youtube( $params )
    {
        if( !empty($params) )
        {
            # Check if the 'href' is inserted
            if( array_key_exists('href', $params) )
            {
                $parts = explode('=', $params['href']);
                $srcId = end( $parts );

                return Beam::make('libraries.media.youtube')
                    ->with('srcId', $srcId)
                    ->render();
            }
        }

        return '';
    }

    /**
     * Render the view for Vimeo videos
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public static function vimeo( $params )
    {
        if( !empty($params) )
        {
            # Check if the 'href' is inserted
            if( array_key_exists('href', $params) )
            {
                $parts = explode('/', $params['href']);
                $srcId = end( $parts );

                return Beam::make('libraries.media.vimeo')
                    ->with('srcId', $srcId)
                    ->render();
            }
        }

        return '';
    }

}