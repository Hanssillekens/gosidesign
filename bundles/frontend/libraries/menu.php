<?php

class Frontend_Menu_Library{

    public static $crumbs       = array();
    public static $home_page_id = '';


    public static function navigation( $page_id = 0, $tree_id = 1, $language = '', $path = '' ){
        $language = ('' == $language ? Scotty::get_language() : $language );
        $path     = ('' == $path ? Scotty::url($language).'/' : $path);

        $sql = 'SELECT p.*, pm.slug, pm.name, pm.page_title, pm.link_type, pm.redirect_url, pm.hashtag
                FROM pages      AS p
                JOIN pages_meta AS pm ON ( pm.page_id = p.id AND pm.language_id = ? AND '.( Scotty::get_preview() ? ' pm.public in (0,1) ' : ' pm.public = 1 ' ).' )
                WHERE p.page_id = ?
                AND   p.tree_id = ?
                AND   p.deleted = 0
                ORDER BY p.`sort` ASC';

        $pages = DB::query($sql, array($language, $page_id, $tree_id) );            

        $out = '';
        $tot = count($pages);

        if( 0 < $tot ) {
            $out.= "\n";

            if(0 != $page_id){
                $ul_css = ( 0 == $page_id ? 'drop' : 'dir' );

                $out.= '<ul class="'.$ul_css.'" >'.PHP_EOL;
            }

            $i = 1;

            foreach( $pages AS $page ) 
            {                   
                $class = '';
                $url   = $child_url = $path.$page->slug.'/';                    

                if( $page_id == 0 ){
                    $class.= ( 1 == $i ? ' first' : ( $i == $tot ? ' last' : '' ) );
                }else{
                    $class.= ( 1 == $i ? ' first-sub' : ( $i == $tot ? ' last-sub' : '' ) );
                }

                // the homepage does NOT need the slug, however we need to pass it to the child for reference!
                if( $page_id == 0 && 1 == $i ){
                    static::$home_page_id = $page->id;
                    $url = $path;
                }

                if( in_array($page->id, Scotty::$page_array) ){
                    self::$crumbs[] = array( 'url'  => $url
                    ,'name' => $page->name );
                    $class.= ' active';
                }



                if( '0' != $page->module_id )
                {
                    $module    = $page->module_id ;
                    $module_id = 0;
                    if( stristr( $page->module_id , '|' ) )
                    {
                        list($module, $module_id) = explode( '|', $page->module_id );
                    }

                    $children = static::navigation_module( $module, $module_id, $language, $child_url );
                }else{
                    $children = static::navigation( $page->id, $tree_id, $language, $child_url );
                }

                $out.= '<li class="'.$class.'">'.PHP_EOL;
                if( is_object( $page ) )
                {
                    # Check for internal or external link
                    if( isset($page->link_type) && $page->link_type != '' )
                    {                            
                        switch ($page->link_type) 
                        {
                            case 'external':
                                $url = ( strstr($page->redirect_url, 'http') ) ? $page->redirect_url : 'http://' .$page->redirect_url;
                                break;

                            case 'internal':
                                $url = \Tools::path_to_page_by_id($page->redirect_url);
                                if( isset($page->hashtag) && $page->hashtag != '' )
                                {
                                    $url .= '#' .$page->hashtag;
                                }
                                break;
                        }
                    };                        

                    $out.= '<a href="'.$url.'" title="'.$page->page_title.'">';
                    $out.= $page->name;
                    $out.= '</a>';
                }

                $out.= $children;

                $out.= "</li>\n";

                $i++;
            }

            if(0 != $page_id){
                $out.="</ul>\n";
            }
        }

        return $out;
    }


    public static function navigation_module( $module, $module_id = '0', $language = '', $path = '' )
    {
        $out   = '';
        $controller = ucfirst($module);

        if( class_exists($controller) )
        {
            if( method_exists($controller, 'navigation') )
            {
                $items = $controller::navigation( $module_id, $language );

                $tot   = count($items);

                if( null != $items && 0 < $tot )
                {
                    $i = 1;
                    $out = '<ul class="drop">'.PHP_EOL;

                    foreach( $items AS $item )
                    {
                        $class = '';
                        $url  = $child_url = $path.$item['slug'].'/';

                        $class.= ( 1 == $i ? ' first-sub' : ( $i == $tot ? ' last-sub' : '' ) );

                        if( URL::full() == $url )
                        {
                            self::$crumbs[] = array( 'url'  => $url
                                                    ,'name' => $item['title'] );
                            $class.= ' active';
                        }

                        $out.= '<li class="'.$class.'">'.PHP_EOL;

                        # Check for internal or external link
                        if( isset($item['link_type']) && $item['link_type'] )
                        {                            
                            switch ($item['link_type']) 
                            {
                                case 'external':
                                    $url = ( strstr($item['redirect_url'], 'http') ) ? $item['redirect_url'] : 'http://' .$item['redirect_url'];
                                    break;

                                case 'internal':
                                    $url = \Tools::path_to_page_by_id($item['redirect_url']);
                                    if( isset($item['hashtag']) && $item['hashtag'] != '' )
                                    {
                                        $url .= '#' .$item['hashtag'];
                                    }
                                    break;
                            }
                        };

                        $out.= '<a href="'.$url.'" title="'.$item['title'].'">';
                        $out.= $item['title'];
                        $out.= '</a>';

                        $out.= "</li>\n";

                        $i++;
                    }

                    $out.="</ul>\n";
                }
            }
        }

        return $out;
    }



    public static function sub( $page_id = 0, $path = '', $language ){

        $language = ('' == $language ? Scotty::get_language() : $language );
        $path     = ('' == $path ? Tools::path_to_page_by_id($page_id) : $path );

        $sql = "SELECT  pm.*
                FROM    pages      AS p
                JOIN    pages_meta AS pm ON ( p.id = pm.page_id AND pm.language_id = ? AND ".( Scotty::get_preview() ? ' pm.public in (0,1) ' : ' pm.public = 1 ' )." )
                WHERE   p.page_id = ? AND p.deleted = 0 ORDER BY p.sort ASC";

        $pages = DB::query( $sql, array($language, $page_id) );

        $out = '';
        $tot = count($pages);

        if( 0 < $tot ) {
            $out.= "\n";

            $ul_css = ( 0 == $page_id ? 'drop' : 'dir' );

            $out.= '<ul class="'.$ul_css.'" >'.PHP_EOL;
            $i = 1;
            foreach( $pages AS $page ) {
                $class = '';

                $url = $path.$page->slug.'/';

                $class.= ( 1 == $i ? ' first' : ( $i == $tot ? ' last' : '' ) );

                // the homepage does NOT need the slug, however we need to pass it to the child for reference!
                if( $page_id == 0 && 1 == $i ){
                    $url = $path;
                }

                $class.= ( in_array( $page->page_id, Scotty::$page_array ) ? ' active' : '' );

                $out.= '<li class="'.$class.'">'.PHP_EOL;
                if( is_object( $page ) )
                {
                    # Check for internal or external link
                    if( isset($page->link_type) && $page->link_type != '' )
                    {                            
                        switch ($page->link_type) 
                        {
                            case 'external':
                                $url = ( strstr($page->redirect_url, 'http') ) ? $page->redirect_url : 'http://' .$page->redirect_url;
                                break;

                            case 'internal':
                                $url = \Tools::path_to_page_by_id($page->redirect_url);
                                if( isset($page->hashtag) && $page->hashtag != '' )
                                {
                                    $url .= '#' .$page->hashtag;
                                }
                                break;
                        }
                    };

                    $out.= '<a href="'.$url.'" title="'.$page->page_title.'">';
                    $out.= $page->name;
                    $out.= '</a>';
                }

                $out.= "</li>\n";

                $i++;
            }
            $out.="</ul>\n";
        }

        return $out;
    }


    public static function crumbs(){
        $out = '';

        $out.= '<ul class="crumbs">'.PHP_EOL;

        $tot = count(self::$crumbs);

        if( 0 == $tot ){
            $out.= '<li>home</li>'.PHP_EOL;
        }else{
            $out.= '<li><a href="/">home</a></li>'.PHP_EOL;
        }

        $i = 1;
        foreach(self::$crumbs AS $crumb ){
            if( $tot == $i ){
                $out.= '<li>'.$crumb['name'].'</li>'.PHP_EOL;
            }else{
                $out.= '<li><a href="'.$crumb['url'].'" title="'.$crumb['name'].'">'.$crumb['name'].'</a></li>'.PHP_EOL;
            }

            $i++;
        }

        $out.= '</ul>'.PHP_EOL;

        return $out;
    }
}