<?php
/**
 * @package     jquery.Formbuilder
 * @author      Michael Botsko
 * @copyright   2009, 2012 Trellis Development, LLC
 *
 * This PHP object is the server-side component of the jquery formbuilder
 * plugin. The Formbuilder allows you to provide users with a way of
 * creating a formand saving that structure to the database.
 *
 * Using this class you can easily prepare the structure for storage,
 * rendering the xml file needed for the builder, or render the html of the form.
 *
 * This package is licensed using the Mozilla Public License 1.1
 *
 * We encourage comments and suggestion to be sent to mbotsko@trellisdev.com.
 * Please feel free to file issues at http://github.com/botskonet/jquery.formbuilder/issues
 * Please feel free to fork the project and provide patches back.
 */


/**
 * @abstract This class is the server-side component that handles interaction with
 * the jquery formbuilder plugin.
 * @package jquery.Formbuilder
 */
class Frontend_Formbuilder_Library {

    /**
     * @var array Holds the form id and array form structure
     * @access protected
     */
    protected $_form_array;
    protected $_form_css;

    public static $db = null;


     /**
      * Constructor, loads either a pre-serialized form structure or an incoming POST form
      * @param array $containing_form_array
      * @access public
      */
    public function __construct($form = false, $form_css = array('form_id' => '', 'form_class' => '')){

        $form = is_array($form) ? $form : array();

        $this->_form_css = $form_css;

        // Set the serialized structure if it's provided
        // otherwise, store the source
        if(array_key_exists('form_structure', $form)){
            $form['form_structure'] = json_decode($form['form_structure'], true);
            $this->_form_array = $form;
        }
        else if(array_key_exists('frmb', $form)){
            $_form = array();
            $_form['form_id'] = ($form['form_id'] == "undefined" ? false : $form['form_id']);
            $_form['form_structure'] = $form['frmb']; // since the form is from POST, set it as the raw array
            $this->_form_array = $_form;
        }
        return true;
    }


    /**
     * Returns the form array with the structure encoded, for saving to a database or other store
     *
     * @access public
     * @return array
     */
    public function get_encoded_form_array(){
        return array('form_id'=>$this->_form_array['form_id'],'form_structure'=>json_encode($this->_form_array['form_structure']));
    }


    /**
     * Prints out the generated json file with a content-type of application/json
     *
     * @access public
     */
    public function render_json(){
        header("Content-Type: application/json");
        print json_encode( $this->_form_array );
    }


    /**
     * Renders the generated html of the form.
     *
     * @param string $form_action Action attribute of the form element.
     * @access public
     * @uses generate_html
     */
    public function render_html($form_action = false, $errors = null){
        print $this->generate_html($form_action, $errors = null);
    }


    /**
     * Generates the form structure in html.
     *
     * @param bool $form_action
     * @param null $errors
     *
     * @return string
     */
    public function generate_html($form_action = false, $errors = null){

        $html = '';

        $form_action = $form_action ? $form_action : $_SERVER['PHP_SELF'];

        $id    = ( $this->_form_css['form_id'] != '') ? $this->_form_css['form_id'] : '' ;
        $class = ( $this->_form_css['form_class'] != '') ? $this->_form_css['form_class'] : 'form form-horizontal';

        if(is_array($this->_form_array['form_structure'])){
            if( $errors ){
                $html .= '<div class="well error">';
                    foreach( $errors->all('<p>:message</p>') AS $err )
                    {
                        $html .= $err;
                    }
                $html .= '</div>';
            }

            $html .= '<form class="' .$class .'" id="' .$id .'" method="post"  action="'.$form_action.'" enctype="multipart/form-data">' . "\n";
            $html .= '<div class="well">';

            foreach($this->_form_array['form_structure'] as $field)
            {
                $html .= $this->loadField((array)$field, $errors);
            }                

            $submit = end($this->_form_array['form_structure']);

            $sumbit_text = ($submit['cssClass'] == 'submit') ? $submit['values'] : 'Verzenden';                                             
            $sumbit_name = ($submit['cssClass'] == 'submit' && $submit['name_tag'] != '') ? $submit['name_tag'] : 'submit';                                                
            
            $html .= '<div style="margin:12px 0 0;"><input type="submit" name="' .$sumbit_name .'" class="btn" id="submitBtn" value="' .$sumbit_text .'" /></div>';
            $html .= '</div>';
            $html .=  '</form>' . "\n";
            
        }

        return $html;

    }


    /**
     * Parses the POST data for the results of the specific form values. Checks
     * for required fields and returns an array of any errors.
     *
     * @access public
     * @returns array
     */
    public function process(){

        $error      = array();
        $results    = array();

        // Put together an array of all expected indices
        if(is_array($this->_form_array['form_structure'])){
            foreach($this->_form_array['form_structure'] as $field){
                
                $field = (array)$field;

                $field['required'] = ($field['required'] == 'checked');

                if($field['cssClass'] == 'input_text' || $field['cssClass'] == 'textarea'){

                    $val = $this->getPostValue( $this->elemId($field['values']));

                    if($field['required'] && empty($val)){
                        $error[] = 'Please complete the ' . $field['values'] . ' field.';
                    } else {
                        $results[ $this->elemId($field['values']) ] = $val;
                    }
                }
                elseif($field['cssClass'] == 'radio' || $field['cssClass'] == 'select'){

                    $val = $this->getPostValue( $this->elemId($field['title']));

                    if($field['required'] && empty($val)){
                        $error[] = 'Please complete the ' . $field['title'] . ' field.';
                    } else {
                        $results[ $this->elemId($field['title']) ] = $val;
                    }
                }
                elseif($field['cssClass'] == 'checkbox'){
                    $field['values'] = (array)$field['values'];
                    if(is_array($field['values']) && !empty($field['values'])){

                        $at_least_one_checked = false;

                        foreach($field['values'] as $item){
                            $item = (array)$item;
                            $elem_id = $this->elemId($item['value'], $field['title']);

                            $val = $this->getPostValue( $elem_id );

                            if(!empty($val)){
                                $at_least_one_checked = true;
                            }

                            $results[ $this->elemId($item['value']) ] = $this->getPostValue( $elem_id );
                        }

                        if(!$at_least_one_checked && $field['required']){
                            $error[] = 'Please check at least one ' . $field['title'] . ' choice.';
                        }
                    }
                } else { }
            }
        }

        $success = empty($error);

        return array('success'=>$success,'results'=>$results,'errors'=>$error);
        
    }


    //+++++++++++++++++++++++++++++++++++++++++++++++++
    // NON-PUBLIC FUNCTIONS
    //+++++++++++++++++++++++++++++++++++++++++++++++++


    /**
     * Loads a new field based on its type
     *
     * @param array $field
     * @access protected
     * @return string
     */
    protected function loadField($field, $errors = false)
    {

        if(is_array($field) && isset($field['cssClass'])){

            switch($field['cssClass']){

                case 'input_text':
                    return '<div>' .$this->loadInputText($field, $errors) .'</div>';
                    break;
                case 'textarea':
                    return '<div>' .$this->loadTextarea($field, $errors) .'</div>';
                    break;
                case 'checkbox':
                    return '<div>' .$this->loadCheckboxGroup($field, $errors) .'</div>';
                    break;
                case 'radio':
                    return '<div>' .$this->loadRadioGroup($field, $errors) .'</div>';
                    break;
                case 'select':
                    return '<div>' .$this->loadSelectBox($field, $errors) .'</div>';
                    break;
                case 'upload':
                    return '<div>' .$this->loadFileUpload($field) .'</div>';
                    break;
                // case 'submit':
                //     return '<div class="form-submit">' .$this->loadSubmit($field) .'</div>';
                //     break;
            }
        }

        return false;

    }


    /**
     * Returns html for an input type="text"
     * 
     * @param array $field Field values from database
     * @access protected
     * @return string
     */
    protected function loadInputText($field){

        # $field['required'] = $field['required'] == 'checked' ? ' required' : false;
        $required_label = ($field['required'] == 'true') ? ' *' : '';
        $required = ($field['required'] == 'true') ? 'required' : '';
        $placeholder = $field['placeholder'];

        $html = '';
        $html .= sprintf('<label for="%s">%s %s</label>' . "\n", $this->elemId($field['values']), $field['values'], $required_label);
        $html .= sprintf('<input class="%s" type="text" id="%s" name="%s" value="%s" placeholder="%s"  %s />' . "\n",
                                $this->elemId( ( array_key_exists( 'class_tag', $field ) ? $field['class_tag'] : '' ) ),
                                $this->elemId($field['values']),
                                $this->elemId($field['name_tag']),
                                $this->getPostValue($this->elemId($field['name_tag'])),
                                $placeholder,
                                $required);
        //$html .= '</li>' . "\n";

        return $html;

    }


    /**
     * Returns html for a <textarea>
     *
     * @param array $field Field values from database
     * @access protected
     * @return string
     */
    protected function loadTextarea($field){

        # $field['required'] = $field['required'] == 'checked' ? ' required' : false;

        $required_label = ($field['required'] == 'true') ? ' *' : '';
        $required = ($field['required'] == 'true') ? 'required' : '';
        $placeholder = $field['placeholder'];

        $html = '';
        //$html .= sprintf('<li class="%s%s" id="fld-%s">' . "\n", $this->elemId($field['cssClass']), $field['required'], $this->elemId($field['values']));
        $html .= sprintf('<label for="%s">%s %s</label>' . "\n", $this->elemId($field['values']), $field['values'], $required_label);
        $html .= sprintf('<textarea class="%s" id="%s" name="%s" rows="5" cols="50" placeholder="%s"  %s >%s</textarea>' . "\n",
                                $this->elemId($field['class_tag']),
                                $this->elemId($field['values']),
                                $this->elemId($field['name_tag']),
                                $placeholder,
                                $required,
                                $this->getPostValue($this->elemId($field['name_tag'])));
        //$html .= '</li>' . "\n";

        return $html;

    }


    /**
     * Returns html for an <input type="checkbox"
     *
     * @param array $field Field values from database
     * @access protected
     * @return string
     */
    protected function loadCheckboxGroup($field){

        $field['required'] = $field['required'] == 'checked' ? ' required' : false;

        $html = '';
        //$html .= sprintf('<li class="%s%s" id="fld-%s">' . "\n", $this->elemId($field['cssClass']), $field['required'], $this->elemId($field['title']));

        if(isset($field['title']) && !empty($field['title'])){
            $html .= sprintf('<label>%s</label>' . "\n", $field['title']);
        }
        $field['values'] = (array)$field['values'];
        if(isset($field['values']) && is_array($field['values'])){
            $html .= sprintf('<span class="multi-row clearfix">') . "\n";
            foreach($field['values'] as $item){
                
                $item = (array)$item;

                // set the default checked value
                $checked = ($item['baseline'] == 'true') ? true : false;

                // load post value
                $val = $this->getPostValue($this->elemId($item['value']));
                // $checked = !empty($val);

                // if checked, set html
                $checked = $checked ? ' checked="checked"' : '';

                $checkbox   = '<span class="clearfix"><input type="checkbox" id="%s-%s" name="%s-%s" value="%s"%s /><label for="%s-%s">%s</label></span>' . "\n";
                $html .= sprintf($checkbox, $this->elemId($field['title']), $this->elemId($item['value']), $this->elemId($field['name_tag']), $this->elemId($item['value']), $item['value'], $checked, $this->elemId($field['title']), $this->elemId($item['value']), $item['value']);
            }
            $html .= sprintf('</span>') . "\n";
        }

        //$html .= '</li>' . "\n";

        return $html;

    }


    /**
     * Returns html for an <input type="radio"
     * @param array $field Field values from database
     * @access protected
     * @return string
     */
    protected function loadRadioGroup($field){

        $field['required'] = $field['required'] == 'checked' ? ' required' : false;

        $html = '';

        //$html .= sprintf('<li class="%s%s" id="fld-%s">' . "\n", $this->elemId($field['cssClass']), $field['required'], $this->elemId($field['title']));

        if(isset($field['title']) && !empty($field['title'])){
            $html .= sprintf('<label>%s</label>' . "\n", $field['title']);
        }
        $field['values'] = (array)$field['values'];
        if(isset($field['values']) && is_array($field['values'])){
            $html .= sprintf('<span class="multi-row">') . "\n";
            foreach($field['values'] as $item){
                
                $item = (array)$item;                                   

                // set the default checked value
                $checked = ($item['baseline'] == 'true') ? true : false;

                // load post value
                $val = $this->getPostValue($this->elemId($field['title']));
                //$checked = !empty($val); // WHY?

                // if checked, set html
                $checked = ($checked == true) ? ' checked="checked"' : '';

                $radio      = '<span class="clearfix"><input type="radio" id="%s-%s" name="%1$s" value="%s"%s /><label for="%1$s-%2$s">%3$s</label></span>' . "\n";
                $html .= sprintf($radio,
                                        $this->elemId($field['title']),
                                        $this->elemId($item['value']),
                                        $item['value'],
                                        $checked);
            }
            $html .= sprintf('</span>') . "\n";
        }

        //$html .= '</li>' . "\n";

        return $html;

    }


    /**
     * Returns html for a <select>
     * 
     * @param array $field Field values from database
     * @access protected
     * @return string
     */
    protected function loadSelectBox($field){

        $field['required'] = $field['required'] == 'checked' ? ' required' : false;

        $html = '';

        //$html .= sprintf('<li class="%s%s" id="fld-%s">' . "\n", $this->elemId($field['cssClass']), $field['required'], $this->elemId($field['title']));

        if(isset($field['title']) && !empty($field['title'])){
            $html .= sprintf( '<label>%s</label>' . "\n", $field['title'] );
        }
        $field['values'] = (array)$field['values'];
        if(isset($field['values']) && is_array($field['values'])){
            $multiple = $field['multiple'] == "true" || $field['multiple'] == "checked" ? ' multiple="multiple"' : '';
            $html .= sprintf('<select name="%s" id="%s"%s>' . "\n", $this->elemId($field['name_tag']), $this->elemId($field['title']), $multiple);
            if($field['required']){ $html .= '<option value="">Selection Required</label>'; }
            
            foreach($field['values'] as $item){
                
                $item = (array)$item;               

                // set the default checked value
                $checked = (isset($item['baseline']) && $item['baseline'] == 'true') ? true : false;

                // load post value
                $val = $this->getPostValue($this->elemId($field['title']));
                //$checked = !empty($val); // WHY?

                // if checked, set html
                $checked = $checked ? ' selected="selected"' : '';

                $option     = '<option value="%s"%s>%s</option>' . "\n";
                $html .= sprintf($option, $item['value'], $checked, $item['value']);
            }

            $html .= '</select>' . "\n";
            //$html .= '</li>' . "\n";

        }

        return $html;

    }

    /**
     * Returns html for an input type="file"
     * 
     * @param array $field Field values from database
     * @access protected
     * @return string
     */
    protected function loadFileUpload($field){

        # $field['required'] = $field['required'] == 'checked' ? ' required' : false;
        $required_label = ($field['required'] == 'true') ? ' *' : '';
        $required = ($field['required'] == 'true') ? 'required' : '';

        $html = '';
        //$html .= sprintf('<li class="%s%s" id="fld-%s">' . "\n", $this->elemId($field['cssClass']), $field['required'], $this->elemId($field['values']));
        $html .= sprintf('<label for="%s">%s %s</label>' . "\n", $this->elemId($field['values']), $field['values'], $required_label);
        $html .= sprintf('<input type="file" id="%s" name="%s" value="%s" %s />' . "\n",
                                $this->elemId($field['values']),
                                $this->elemId($field['name_tag']),
                                $this->getPostValue($this->elemId($field['name_tag'])),
                                $required);
        //$html .= '</li>' . "\n";

        return $html;

    }


    /**
     * Generates an html-safe element id using it's label
     * 
     * @param string $label
     * @return string
     * @access protected
     */
    protected function elemId($label, $prepend = false){
        if(is_string($label)){
            $prepend = is_string($prepend) ? $this->elemId($prepend).'-' : false;
            return $prepend.strtolower( preg_replace("/[^A-Za-z0-9_]/", "", str_replace(" ", "_", $label) ) );
        }
        return false;
    }
    

    /**
     * Attempts to load the POST value into the field if it's set (errors)
     *
     * @param string $key
     * @return mixed
     */
    protected function getPostValue($key)
    {
        return ( Input::has($key) ? Input::get($key) : false );
    }
}