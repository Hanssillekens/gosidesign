<?php
/**
 * Class Scotty
 *
 * Scotty::get_languages()
 * Scotty::get_language_default()
 * Scotty::get_language()
 *
 * Scotty::get_module_params()
 *
 */
class Frontend_Scotty_Library{

    public static $settings      = array();
    public static $module_params = array();
    public static $module_url    = null;
    public static $module_id     = null;
    public static $module        = null;

    public static $page_array    = array();

    public static $level         = 0;
    public static $detect        = null;

    public static $base_rendered = false;

    public static $restfull      = false;

    public static function initialize()
    {
        static::$detect = new Mobile_Detect;

        // Get the site parameters.
        $parameters = URI::$segments;
        reset($parameters);

        // Let scotty know what languages we have.
        static::set_languages(        Config::get('frontend::runtime.languages' ) );
        static::set_language_default( Config::get('frontend::runtime.language.default') );

        // NEWSLETTER MODE !             
        if( !empty($parameters) && ( head($parameters) == 'newsletter' || in_array('unsubscribe', $parameters) ) )
        {
            array_shift($parameters);
            // GOTO NEWSLETTER CONTROLLER!
            # Retrieve the newsletter
            if( Input::get('unsubscribe') && Input::get('unsubscribe') == 1 )
            {
                return call_user_func_array( array('Newsletter', 'get_unsubscribe_view'), array());
            }
            return call_user_func_array( array('Newsletter', 'get_view_newsletter'), array());
            exit;
        }

        // PREVIEW MODE !
        static::set_preview( false );
        if( !empty($parameters) && head($parameters) == 'preview' )
        {
            static::set_languages( Config::get('frontend::runtime.languages_preview' ) );
            static::set_preview( true );
            array_shift($parameters);
        }


        // first check if the first array key is an language variable
        // if so set it as the default language and remove it from the array.
        // if no language was present. redirect to the url with the language parament
        if( !empty($parameters) && array_key_exists( head($parameters), static::get_languages() ) )
        {
            static::set_language( head($parameters) );
            array_shift($parameters);
        }else
        {
            ini_set('memory_limit', '128M');
            // Pass Croppa the current URL
            Croppa::config(array(

                // Directories to search for source files
                'src_dirs' => array(
                    path('public'),
                ),

                // Maximum number of sizes to allow for a particular
                // source file.  This is to limit scripts from filling
                // up your hard drive with images.  Set to false or comment
                // out to have no limit.
                'max_crops' => 12,
            ));

            Croppa::handle_404(Request::uri());

            $redirect_to = Config::get('frontend::runtime.language.default').'/'.implode('/',$parameters);
            return Redirect::to( $redirect_to );
        }

        // Based on the parameters left get the page. starting from the last parameter used. so reverse the array
        $parts    = array_filter(array_reverse($parameters, true));
        $language = static::get_language();
        $page     = false;

        static::$module_params  = array();

        // based on the first parameter that matches the page.
        if(!empty($parts))
        {
            foreach( $parts AS $key => $part )
            {
                $parent_slug = ( isset($parts[$key-1]) ? $parts[$key-1] : '' );

                if('' != $parent_slug )
                {
                    static::$level = $key;

                    $sql = "SELECT pm.*
                            FROM   pages_meta AS pm
                            JOIN   pages      AS p   ON ( p.id = pm.page_id AND p.tree_id = 1 )
                            JOIN   pages_meta AS pmp ON ( pmp.slug = ?      AND pmp.language_id = pm.language_id AND pmp.page_id = p.page_id )
                            WHERE  pm.language_id = ?
                            AND    pm.slug = ? ";

                    $ret = DB::query($sql, array($parent_slug, $language, $part) );

                    if(!empty($ret))
                    {
                        $page                  = Pagemeta::with('page')->where_id( $ret[0]->id )->first();
                        static::$module_params = array_filter(array_slice($parameters,$key+1));
                        break;
                    }
                }
                else
                {
                    $ret = Pagemeta::with('page')->where_slug( $part )->where_language_id( $language )->first();

                    if(null != $ret && $ret->relationships['page']->page_id == 0 )
                    {
                        $page                  = $ret;
                        static::$module_params = array_filter(array_slice($parameters,$key+1));
                        break;
                    }
                }
            }
        }
        else
        {
            // get the HOME page!
            $page = Page::order_by('sort')->where_page_id('0')->where_tree_id('1')->first();

            if(null != $page)
            {
                $page = Pagemeta::with('page')->where_page_id($page->id)->where_language_id( $language )->first();
                static::$module_params = array_filter(array_slice($parameters,1));
            }
        }

        if(false == $page)
        {
            return Response::error('404');
        }

        // okay we came this far! A page was been found now we can p00p it to the screen.
        // First things first. Get all the required items.
        Beam::$page = $page;

        //check if page has redirect
        if( 0 != Beam::$page->relationships['page']->redirect )
        {
            $path = Tools::get_path_to_page( Beam::$page->relationships['page']->redirect );

            return Redirect::to( Scotty::url().'/'.$path );
        }

        Beam::$template    = $page->relationships['page']->page_template();

        Beam::$backgrounds = Media::get_all_images('pages', $page->page_id, 'pagebackground|page_background');
        Beam::$headers     = Media::get_all_images('pages', $page->page_id, 'pageheader|page_header');

        Beam::$blocks      = static::build_blocks();

        // gather site settings
        static::settings();

        // build all menus
        static::menus();

        $mc = static::module_content();

        if($mc instanceof Laravel\Redirect){
            return $mc;
        }

        Beam::page_title( Beam::$page->page_title );


        $template = Beam::use_template();

        if( false == static::$base_rendered )
        {
            Base::before_render();
        }

        // NOW p00p!
        return Beam::make( $template )
            ->nest('head',          'base/head')
            ->nest('content',       'base/content')
            ->nest('header',        'base/header')
            ->nest('footer',        'base/footer')
            ->nest('base_js',       'base/js');
    }


    public static function get( $key )
    {
        return Session::get( 'scotty_'.$key );
    }


    public static function set( $key, $val )
    {
        return Session::put( 'scotty_'.$key, $val );
    }


    public static function __callStatic( $method, $params )
    {
        $methods = explode( '_', $method, 2 );
        $mode    = array_shift( $methods );
        $value   = array_shift( $methods );

        if( 'set' == $mode )
        {
            return call_user_func_array( 'self::set', array( $value, array_shift($params) )  );
        }

        return call_user_func_array( 'self::get', array( $value ) );
    }


    protected static function build_blocks()
    {
        $blocks = array( 'top' => array(), 'left' => array(), 'right' => array(), 'bottom' => array() );

        // template blocks available?
        if( 0 < Beam::$page->page->template_id )
        {
            $blocks_ret = Block::select( array( 'blocks.id', 'template_blocks.position' ) )->where_deleted(0)
                ->join('template_blocks', 'template_blocks.block_id', '=', 'blocks.id')
                ->raw_where(    ' ( template_blocks.refer_to = "template" AND template_blocks.refer_id = '.Beam::$page->page->template_id.') ' )
                ->raw_or_where( ' ( template_blocks.refer_to = "page"     AND template_blocks.refer_id = '.Beam::$page->page_id.') '           )
                ->order_by('template_blocks.position')
                ->order_by('template_blocks.sort')
                ->get();
        }

        if( !isset($blocks_ret) || is_null($blocks_ret) || 0 == count($blocks_ret) )
        {
            // Page Blocks
            $blocks_ret = Block::where_page_id( Beam::$page->page_id )->where_deleted(0)->or_where( 'access_all_pages', '=', '1' )->order_by('position')->order_by('sort')->get();
        }

        if( is_array($blocks_ret) )
        {
            foreach( $blocks_ret AS $block )
            {
                if( !array_key_exists($block->position, $blocks) )
                {
                    $blocks[$block->position] = array();
                }

                if( static::get_preview() )
                {
                    $meta = Blockmeta::with('block')->where_block_id($block->id)->where_language_id( static::get_language() )->first();
                }else{
                    $meta = Blockmeta::with('block')->where_public(1)->where_block_id($block->id)->where_language_id( static::get_language() )->first();
                }

                if( null != $meta )
                {
                    $blocks[$block->position][] = $meta;
                }
            }
        }

        return $blocks;
    }


    protected static function settings()
    {
        // Get page settings
        $settings_ret   = Setting::all();
        static::$settings = Config::get('frontend::sitesettings');

        if( is_array($settings_ret) )
        {
            foreach( $settings_ret AS $set )
            {
                static::$settings[$set->name] = $set->value;
            }
        }
    }


    public static function url( $language = '' )
    {
        $language = ( '' == $language ? static::get_language() : $language );

        $url = URL::base();

        if(  true == static::get_preview() )
        {
            $url.='/preview';
        }

        $url.= '/'.$language;

        return $url;
    }



    protected static function menus()
    {
        //page_array
        static::$page_array = Page::full_page_array( Beam::$page->page_id );

        // Build the menu based on the tree.
        Beam::$menu   = Menu::navigation( 0, 1, static::get_language(), '', static::$page_array );

        $sub_menu_key = ( 0 == Beam::$page->relationships['page']->page_id ? Beam::$page->page_id : Beam::$page->relationships['page']->page_id );

        if( '0' == Beam::$page->relationships['page']->module_id ){
            Beam::$menu_sub = Menu::sub( $sub_menu_key, '', static::get_language() );
        }else{
            $module    = Beam::$page->relationships['page']->module_id;
            $module_id = 0;

            if( stristr( Beam::$page->relationships['page']->module_id, '|' ) )
            {
                list($module, $module_id) = explode( '|', Beam::$page->relationships['page']->module_id  );
            }

            $module_menu = Menu::navigation_module( $module, $module_id, static::get_language() );
            if( !empty($module_menu) )
            {
                Beam::$menu_sub = $module_menu;
            }else
            {
                Beam::$menu_sub = Menu::sub( $sub_menu_key, '', static::get_language() );
            }
        }

        // Breadcrumbs ?! Will make em, first need to talk with Hanzel & Gretel!
        Beam::$crumbs = Menu::crumbs();
    }


    protected static function module_content()
    {
        // if we have a module. get its content!
        if( '' !== Beam::$page->relationships['page']->module_id && 0 !== Beam::$page->relationships['page']->module_id && 'none' != Beam::$page->relationships['page']->module_id )
        {
            $module = Beam::$page->relationships['page']->module_id;

            if(stristr($module, '|' ) ){
                list($module, static::$module_id) = explode('|',$module);
            }

            static::$module_url = Tools::get_page_url_by_module( Beam::$page->relationships['page']->module_id );

            if('micros' === $module)
            {
                $slug       = Microsmeta::where_micro_id(static::$module_id)->where_language_id( Scotty::get('language') )->first()->slug;
                $controller = 'Micros_'.ucfirst($slug);

                // get slug for the module
                return Beam::$module_content = static::handle_restful_controller( $controller );
            }

            $controller = ''.ucfirst($module);

            static::$module = Beam::$page->relationships['page']->module_id;

            // check controller
            if( class_exists($controller) )
            {
                if( property_exists($controller, 'restfulController') && true == $controller::$restfulController )
                {
                    return Beam::$module_content = static::handle_restful_controller( $controller );
                }

                // check dynamic method. if it exists inject the module into the main layout
                if (method_exists($controller,'dynamic'))
                {
                    // The module exists, the method exists. the client is expecting loads of content.
                    // shablam! there it is.
                    return Beam::$module_content = call_user_func_array( array( $controller, 'dynamic' ) , self::$module_params );
                }

                // not dynamic, so module with decide layout and such.
                $module = new $controller( static::$module_params );
                return $module->build();
            }
        }
    }


    protected static function handle_restful_controller( $controller )
    {
        $rest   = Request::method();
        $method = 'index';

        if( str_contains( $controller, '-' ) )
        {
            $class  = '' . ucfirst( str_replace('-', '', $controller ) );
        }else
        {
            $class  = '' . ucfirst( $controller );
        }


        # Variable with all current params
        $allParams = static::$module_params;

        // method
        if (0 < count(static::$module_params))
        {
            $method = array_shift(static::$module_params);
        }            

        $method = $rest . '_' . $method;
        $c = new $class();                                               

        if (method_exists($class, $method))
        {
            return call_user_func_array( array( $c, $method ) , static::$module_params );
        }

        $c = new $class();
        return call_user_func_array( array( $c, '__call' ) , array($method, $allParams) );
    }
}