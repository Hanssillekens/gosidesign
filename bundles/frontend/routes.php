<?php

Route::get('/serve-media/(:num)', function($id){
    return Media::serve($id);
});

Route::controller('portal');
Route::controller('micros');


/**
 * Clean and simple. Scotty will fix it.
 */
Route::any( '(.*)', function(){
    return Scotty::initialize();
});